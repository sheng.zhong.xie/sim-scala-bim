package it.unibo.ssb.controller

import it.unibo.ssb.controller.game.{GameController, GameControllerSimple}
import it.unibo.ssb.model.{Card, Wizard}
import org.scalatest.FunSuite

class controllerTest extends FunSuite {
  private final val MaxPlayersNum: Int = GameControllerSimple.MaximumPlayer
  private final val player = List("Andrea", "Mauro", "Giorgia", "Alessia", "Mario")
  private final val unset = "unset"
  private final val spell = 8

  test("I can create board with 5 player not 6 or greater") {
    assert(GameController() != null)
    assertThrows[IllegalArgumentException](GameController().setPlayer(List("a", "b", "c", "d", "e", "f", "g", "h")))
    assert(GameController() != null)
  }

  test("I can't launch next round or nextTurn  or nextWizard moment before start game") {
    val gc = GameController()
    gc.setPlayer(player)
    gc.initializeGame()
    assertThrows[IllegalStateException](gc.nextWizardMoment())
  }

  test("Wizard must be created correctly") {
    val gc = GameController()
    gc.setPlayer(player)
    gc.initializeGame()
    assert(gc.wizardsPlayer.size == MaxPlayersNum)
  }

  test("Wizard can throw a spell just if it is his round") {
    val wizards = (1 to MaxPlayersNum).map(p => Wizard(unset, p))
    val gc = GameController()
    gc.setPlayer(player)
    gc.initializeGame()
    gc.startGame()
    gc.wizardsCastSpell(wizards(0), 1)
  }

  test("All wizards can throw spell one after one") {
    val wizards = (1 to MaxPlayersNum).map(p => Wizard(unset, p))

    val gc = GameController()
    gc.setPlayer(player)
    gc.initializeGame()
    gc.startGame()
    wizards.foreach(w => {
      gc.wizardsCastSpell(w, spell)
      gc.nextWizardMoment()
    })
    wizards.foreach(w => {
      gc.wizardsCastSpell(w, spell)
      gc.nextWizardMoment()
    })
  }


  test("Controller must refill hands of wizard did wrong cast") {
    val wizards: Seq[Wizard] = (1 to MaxPlayersNum).map(p => Wizard(unset, p))
    val gc = GameController()
    gc.setPlayer(player)
    gc.initializeGame()
    gc.startGame()
    assert(gc.getGameBoard.wizardGameBoard(wizards.head).cardsList.size == MaxPlayersNum)
    gc.getGameBoard.wizardGameBoard(wizards.head)
      .discard(gc.getGameBoard.wizardGameBoard(wizards.head).cardsList.head)
    gc.getGameBoard.wizardGameBoard(wizards.head).addCard(Card(2))

    assert(gc.getGameBoard.wizardGameBoard(wizards.head).cardsList.size == MaxPlayersNum)
    gc.wizardsCastSpell(wizards.head, 2)
    assert(gc.getGameBoard.wizardGameBoard(wizards.head).cardsList.size == 4)
    gc.nextWizardMoment()

    assert(gc.getGameBoard.wizardGameBoard(wizards.head).cardsList.size == MaxPlayersNum)
  }

}
