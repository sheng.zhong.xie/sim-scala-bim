package it.unibo.ssb.model

import java.util.NoSuchElementException

import it.unibo.ssb.model.Card.Card_1
import it.unibo.ssb.model.WizardGameBoardTest.{id, name}
import org.scalatest.FunSuite

import scala.collection.mutable.ListBuffer

class WizardGameBoardTest extends FunSuite {

  test("A wizard board must have correct id") {
    val wizardGameBoard: WizardGameBoard = WizardGameBoard(Wizard(name, id))
    assert(wizardGameBoard.idWizard == 1234)
  }

  test("A wizard board can have more cards") {
    val wizardGameBoard: WizardGameBoard = WizardGameBoard(Wizard(name, id))
    var list: ListBuffer[Card] = ListBuffer[Card]()
    for (_: Int <- 1 to 10) {
      var card: Card = new Card_1
      list += card
      wizardGameBoard addCard card
      assert(wizardGameBoard containCard card)
    }
    assert(wizardGameBoard.cardsList.size == 10)
    list foreach (card => wizardGameBoard discard card)
    assert(wizardGameBoard.cardsList.isEmpty)
  }

  test("A wizard board can have inc and dec cards") {
    var wizardGameBoard: WizardGameBoard = WizardGameBoard(Wizard(name, id))
    wizardGameBoard = WizardGameBoard(Wizard(name, id))
    val card: Card = new Card_1
    wizardGameBoard addCard card
    assert(wizardGameBoard.cardsList.size == 1)
    wizardGameBoard discard card
    assert(wizardGameBoard.cardsList.isEmpty)
    assertThrows[NoSuchElementException](wizardGameBoard discard card)
  }

  test("A wizard board must contains cards not used") {
    val wizardGameBoard: WizardGameBoard = WizardGameBoard(Wizard(name, id))
    val card1 = Card(1)
    val card2 = Card(2)
    val card7 = Card(7)
    wizardGameBoard.addCard(card1)
    wizardGameBoard.addCard(card2)
    wizardGameBoard.addCard(card7)
    assert(wizardGameBoard.containCard(card1))
    assert(wizardGameBoard.containCard(card2))
    assert(wizardGameBoard.containCard(card7))
  }
}

object WizardGameBoardTest {
  private final val id: Int = 1234
  private final val name: String = "Wiz"
}

