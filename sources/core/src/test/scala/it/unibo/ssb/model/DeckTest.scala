package it.unibo.ssb.model

import org.scalatest.FunSuite

class DeckTest extends FunSuite {

  private final val decksize = 36

  test("Deck have 36 cards") {
    val deck: Deck = Deck()
    deck.card(decksize)
    assert(deck.isEmpty)
  }
  test("Deck must give a know set of cards") {
    val deck1: Deck = Deck()
    val deck2 = deck1.card(decksize)
    for (a <- 1 to 8) {
      assert(a == deck2.count(_.id == a))
    }
  }
  test("Deck shuffle must be random") {
    var deck1: Deck = Deck()
    var deck2: Deck = Deck()

    for (_ <- 1 to 100) {
      deck1.card(decksize) == deck2.shuffle().card(decksize)
      deck2 = Deck()
      deck1 = Deck()
    }
  }
  test("Size method must work") {
    val deck: Deck = Deck()
    assert(deck.size == decksize)
    for (a <- 1 to decksize) {
      deck.card()
      assert(deck.size == (decksize - a))
    }
  }


}
