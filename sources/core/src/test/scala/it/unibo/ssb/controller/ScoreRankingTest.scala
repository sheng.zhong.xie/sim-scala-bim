package it.unibo.ssb.controller

import it.unibo.ssb.controller.game.ScoreRanking
import org.scalatest.FunSuite

class ScoreRankingTest extends FunSuite{

  test("ScoreRanking must compute new offset score"){
    val score: ScoreRanking = ScoreRanking()

    val offset = score.computeScore(List(1200), List(1300, 1200, 1000))

    assert(offset._1 > 0)
    assert(offset._2 < 0)
  }

  test("Algorithm must gives more point to weaker player then string player"){
    val score: ScoreRanking =  ScoreRanking()

    val offset = score.computeScore(List(1200), List(1300, 1200, 1000))
    val offset2 = score.computeScore(List(2000), List(1300, 1200, 1000))

    assert(offset._1 > offset2._1)
    assert(offset._2 < offset2._2)
  }

}
