package it.unibo.ssb.model

import org.scalatest.FunSuite

import scala.collection.mutable.ListBuffer

class sharedBoardTest extends FunSuite {

  var deck: Deck = Deck()
  var sgb: SharedGameBoard = SharedGameBoard()

  test("ShredBoard must be initialized properly") {
    assert(sgb.secretCardsSize().equals(4))
    assert(sgb.usedCards.isEmpty)
  }

  test("I can put a card in used cards list") {
    sgb = SharedGameBoard()
    sgb.putCardInUsedCards(sgb.drawCard())
    assert(sgb.usedCards.size == 1)
    val card = sgb.drawCard()
    sgb.putCardInUsedCards(card)
    assert(sgb.usedCards.contains(card))
  }
  test("Secret must be only 4") {
    sgb = SharedGameBoard()
    for (_ <- 1 to 4) {
      sgb.drawSecretCard()
    }
    assertThrows[IndexOutOfBoundsException](sgb.drawSecretCard())
  }


}
