package it.unibo.ssb.model

import it.unibo.ssb.controller.game.Dice

/**
  * The spell trait models a strategy for a [[Card]], that from a [[GameBoard]] context and a spell caster,
  * applies the magic.
  */
sealed trait Spell extends ((GameBoard, Wizard) => Unit) with Serializable {
  override def apply(game: GameBoard, spellCaster: Wizard): Unit

  def apply(dice: Dice)(game: GameBoard, spellCaster: Wizard): Unit = apply(game, spellCaster)
}

object Spell {

  /**
    * Reduce life of a player in a position relative to the spell caster.
    *
    * @param wizards     the ordered list of wizards
    * @param spellCaster the wizard that casts the spell that hurts someone
    * @param direction   the direction to cast the spell to
    * @param amount      the amount of damage to do
    */
  private def hurt(wizards: Seq[Wizard], spellCaster: Wizard)(direction: Direction, amount: Int = 1): Unit = {
    val pos = wizards.indexOf(spellCaster)

    direction match {
      case Direction.LEFT if pos >= 0 => (if (pos > 0) wizards(pos - 1) else wizards.last).decLife(amount)
      case Direction.RIGHT if pos <= wizards.size - 1 =>
        (if (pos < wizards.size - 1) wizards(pos + 1) else wizards.head).decLife(amount)
      case Direction.BOTH =>
        def nestedHurt(direction: Direction, amount: Int = 1): Unit = hurt(wizards, spellCaster)(direction, amount)

        nestedHurt(Direction.LEFT, amount)
        nestedHurt(Direction.RIGHT, amount)
      case Direction.ALL => wizards.filter(!spellCaster.equals(_)).foreach(_.decLife(amount))
      case _ => throw new IllegalArgumentException
    }
  }

  /** The direction to cast a spell to. */
  private sealed trait Direction

  /** Take a secret spellstone, look at it and put it in front of you. */
  case class Owl() extends Spell {
    override def apply(game: GameBoard, spellCaster: Wizard): Unit = {
      game.wizardGameBoard(spellCaster).addCard(game.sharedGameBoard.drawSecretCard())
    }
  }

  /** Roll a dice: every other player lose a number of life points equal to the result. */
  case class Dragon() extends Spell {
    override def apply(game: GameBoard, spellCaster: Wizard): Unit = apply()(game, spellCaster)

    override def apply(dice: Dice = Dice.three())(game: GameBoard, spellCaster: Wizard): Unit = {
      hurt(game.livingWizard, spellCaster)(Direction.ALL, dice.throwOnce())
    }
  }

  /** All other players lose 1 life point; you gain 1 life point. */
  case class Ghost() extends Spell {
    override def apply(game: GameBoard, spellCaster: Wizard): Unit = {
      hurt(game.livingWizard, spellCaster)(Direction.ALL)
      spellCaster.incLife()
    }
  }

  /** Roll a dice: you gain a number of life points equal to the result. */
  case class Forest() extends Spell {
    override def apply(game: GameBoard, spellCaster: Wizard): Unit = apply()(game, spellCaster)

    override def apply(dice: Dice = Dice.three())(game: GameBoard, spellCaster: Wizard): Unit = {
      spellCaster.incLife(dice.throwOnce())
    }
  }

  /** The players at your left and your right lose 1 life point. */
  case class Storm() extends Spell {
    override def apply(game: GameBoard, spellCaster: Wizard): Unit = hurt(game.livingWizard, spellCaster)(Direction.BOTH)
  }

  /** The player at your left lose 1 life point. */
  case class Wave() extends Spell {
    override def apply(game: GameBoard, spellCaster: Wizard): Unit = hurt(game.livingWizard, spellCaster)(Direction.LEFT)
  }

  /** The player at your right lose 1 life point. */
  case class Fire() extends Spell {
    override def apply(game: GameBoard, spellCaster: Wizard): Unit = hurt(game.livingWizard, spellCaster)(Direction.RIGHT)
  }

  /** You gain 1 life point. */
  case class Potion() extends Spell {
    override def apply(game: GameBoard, spellCaster: Wizard): Unit = spellCaster.incLife()
  }

  private object Direction {

    case object LEFT extends Direction

    case object RIGHT extends Direction

    case object BOTH extends Direction

    case object ALL extends Direction

  }

}