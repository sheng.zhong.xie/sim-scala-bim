package it.unibo.ssb.controller.game

import it.unibo.ssb.model.Wizard

trait DisconnectionHandling {

  /**
    * Method for a distributed game to handle a disconnection
    * @param wizard the wizard
    */
  def wizardDisconnected(wizard: Wizard): Unit

}
