package it.unibo.ssb.model.lobby

import akka.actor.ActorRef
import it.unibo.ssb.controller.GameTypes

/**
  * Interface of a class use to master lobby to send settings to Lobby
  * Lobby need to know
  * <ul>
  *   <li> Owner
  *   <li> Game Type
  */
trait Settings extends Serializable {

  /**
    * The owner of that game (the name of player and it's client actor ref
    * @return
    */
  def owner: (ActorRef, String)

  /**
    * The max number of player in this game
    * @return
    */
  def nPlayer : Int

  /**
    * if game is ranked or not
    * @return
    */
  def ranked : Boolean

  /**
    * Number of invited player
    * @return
    */
  def nInvitedPlayer: Int

}

object Settings{
  def apply(owner : (ActorRef, String), ranked: Boolean, nPlayers: Int, nInvitedPlayer: Int): Settings =
    SettingSimple(owner, ranked, nPlayers, nInvitedPlayer)
}