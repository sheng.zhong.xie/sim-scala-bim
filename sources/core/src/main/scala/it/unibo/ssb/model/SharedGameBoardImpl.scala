package it.unibo.ssb.model

import scala.collection.mutable.ListBuffer

case class SharedGameBoardImpl(deck: Deck) extends SharedGameBoard {

  private val secretCards: ListBuffer[Card] = ListBuffer[Card]() ++= deck.card(SharedGameBoard.SecretCardMaxNumber)
  private var usedCard: List[Card] = List.empty[Card]

  override def drawCard(): Card = deck.card()

  override def deckSize: Int = deck.size

  override def drawSecretCard(): Card = secretCards.remove(0)

  override def putCardInUsedCards(card: Card): Unit = {
    usedCard = usedCard :+ card
  }

  override def secretCardsSize(): Int = secretCards.size

  override def usedCards: List[Card] = usedCard
}

object SharedGameBoardImpl {
  def apply(deck: Option[Deck]): SharedGameBoardImpl = new SharedGameBoardImpl(deck.getOrElse(Deck().shuffle()))
}
