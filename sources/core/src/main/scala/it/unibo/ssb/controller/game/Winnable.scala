package it.unibo.ssb.controller.game

import it.unibo.ssb.model.Wizard

/** The trait defines method to handle the win of a game. */
trait Winnable {

  /**
    * Method to call to know if someone won.
    *
    * @return the list of winners, if any
    */
  def getWinners: Option[List[Wizard]]

  /**
    * This method is called to start next round.
    * Each game is made up of many rounds
    * Each round is composed by turns.
    *
    * <br>Rounds is piece of game composed by more turns. At the end of each round
    * each wizard gains or not points for the game ranking.
    * <ul>
    * <li> Winners: 3 points
    * <li> living wizards: 1 point
    * <li> dead wizards: 0 points
    * </ul>
    *
    * @see GameController.nextTurn
    * @throws IllegalStateException if game not started
    */
  @throws[IllegalStateException]
  def nextRound(): Unit

  /**
    * Method the returns the current raking. The ranking is handled by tower where
    * each floor represents a score. This tower has a prefixed number of floor (Default 8)
    * Once a wizard reach the last floor he won
    *
    * @return the current ranking
    */
  def getRanking: List[(Wizard, Int)]

  /**
    * Provides the current round
    * @return integers the means the current round
    */
  def currentRound: Int

  /**
    * Provides the raking of the game with a more flag that indicates if a wizard is winner or not
    * @return List of wizard with their score and a flag to indicate if he is winner or not
    */
  def gameWinners: List[(Wizard, Boolean, Int)]



}
