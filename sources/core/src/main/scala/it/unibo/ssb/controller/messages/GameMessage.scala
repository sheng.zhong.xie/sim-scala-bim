package it.unibo.ssb.controller.messages

import akka.actor.ActorRef
import it.unibo.ssb.model.{GameBoard, Wizard}

/**
  * In this class will be defined all messages that the actors responsible of connecting the GUI
  * with the Controller will use.
  */
sealed trait GameMessage extends Message

object GameMessage {

  /**
    * Message sent by MatchMaking when a game is ready to start to tell
    * to each players theirs gameController
    * @param gameController the game controller of the game
    */
  final case class IsTimeToPlay(gameController: ActorRef) extends GameMessage

  final case class GuiReadyToPlay(gameController: ActorRef, gameGuiActor: ActorRef) extends GameMessage

  final case class InitializeGameMessage() extends GameMessage

  final case class StartGameMessage() extends GameMessage

  final case class NextRoundMessage() extends GameMessage

  final case class NextTurnMessage() extends GameMessage

  final case class SetPlayerMessage(numberOfHumanPlayers: List[String]) extends GameMessage

  final case class WizardsCastSpellMessage(wizard: Wizard, spellToCast: Int, canCast: Option[Boolean] = None) extends GameMessage

  final case class NextWizardMomentMessage() extends GameMessage

  final case class WizardsPlayerMessage(players: Seq[Wizard]) extends GameMessage

  final case class GetGameBoardMessage(gameBoard: Option[GameBoard] = None) extends GameMessage

  final case class WizardTurnMessage(wizardInTurn: Option[Wizard] = None) extends GameMessage

  final case class PlayerDisconnection(wizard: Wizard) extends GameMessage

  final case class TimerTimeOut() extends GameMessage

  final case class ContinueTheGame() extends GameMessage

  final case class SecretCard(idCard :Int) extends GameMessage

  /**
    * The message is used to communicate GUI actor reference to the actor that controls the client the GUI runs on.
    * @param actRef the GUI actor reference
    * @param name the name of the Wizard
    */
  final case class ClientPresentation(actRef: ActorRef, name: String) extends GameMessage

  final case class WizardHasWonTheRound(wizard: List[Wizard], round: Int, ranking: List[(Wizard, Int)]) extends GameMessage

  final case class WizardHasWonTheGame(winners: List[Wizard], ranking: List[(Wizard, Int)]) extends GameMessage

  final case class StartRound(round: Int) extends GameMessage

}
