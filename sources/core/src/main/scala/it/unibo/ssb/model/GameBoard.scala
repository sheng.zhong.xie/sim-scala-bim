package it.unibo.ssb.model

trait GameBoard extends Serializable {

  /**
    *
    * @return the Shared Game Board
    */
  var sharedGameBoard: SharedGameBoard

  /**
    *
    * @return a Seq with the wizards that are alive
    */
  def livingWizard: Seq[Wizard]

  /**
    *
    * @return a Seq with the wizards what are dead
    */
  def deadWizard: Seq[Wizard]

  /**
    *
    * @return a Seq with the wiards that have left
    */
  def leftWizard: Seq[Wizard]

  /**
    *
    * @return a Seq with the WizardGameBoard of all the wizard
    */

  def wizardGameBoardList: Seq[WizardGameBoard]

  /**
    *
    * @param wizardGameBoard of the actual wizard
    * @param jumps to do
    *
    * @return the wizard gameboard at the right after n jumps
    */
  def leftWizardGameBoard(wizardGameBoard: WizardGameBoard, jumps: Int): WizardGameBoard

  /**
    *
    * @param wizardGameBoard of the actual wizard
    * @param jumps to do
    *
    * @return the wizard gameboard at the left after n jumps
    */
  def rightWizardGameBoard(wizardGameBoard: WizardGameBoard, jumps: Int): WizardGameBoard

  /**
    *
    * @param wizard of the board
    *
    * @return the WizardGAmeBoard of the wizard
    */
  def wizardGameBoard(wizard: Wizard): WizardGameBoard

  /**
    *
    * @param name name of wizard
    *
    * @return wizard game board
    */
  def wizardGameBoard(name: String): WizardGameBoard

  /**
    *
    * @param wizard to remove from the game
    */
  def removeWizard(wizard: Wizard):Unit
}

object GameBoard {
  final val MaxPlayers: Int = 5
  final val MinPlayers: Int = 2
  final val MaxCardsPerPlayer: Int = 5

  def apply(wizards: Seq[Wizard]): GameBoard = GameBoardImpl(wizards)
}
