package it.unibo.ssb.model.lobby

import akka.actor.ActorRef
import it.unibo.ssb.controller.GameTypes

case class SettingSimple(owner : (ActorRef, String), ranked: Boolean, nPlayer: Int, nInvitedPlayer: Int) extends Settings
