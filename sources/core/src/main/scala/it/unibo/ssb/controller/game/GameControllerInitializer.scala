package it.unibo.ssb.controller.game

import it.unibo.ssb.model.Wizard

trait GameControllerInitializer {
  /*
    NB: According to scala style shown by official scala site function with 0-arity
     and NO Side-Effect must be declared without parentheses
   */

  /**
    * this method must be called to initialize all game stuff as:
    * <ul>
    * <li> Board
    * <li> Cards
    * <li> Wizard
    * <li> Ranking
    * </ul>
    * It must be called as first method
    */
  def initializeGame(): Unit

  /**
    * This method is called to start a game. It must be called after initializeGame
    * This method has to be launched after everything is ready to play
    *
    * @throws java.lang.IllegalStateException if not initialized game
    */
  @throws(classOf[IllegalStateException])
  def startGame(): Unit


  /**
    * This method has to be used to set the player in game
    *
    * @param numberOfHumanPlayers number of players
    *
    * @throws IllegalArgumentException if sum of players is higher than 5
    * @throws IllegalStateException    if method is launched before initialized
    *
    */
  @throws(classOf[IllegalArgumentException])
  @throws(classOf[IllegalStateException])
  def setPlayer(numberOfHumanPlayers: List[String]): Unit

  /**
    * The list of wizards
    *
    * @return
    */
  def wizardsPlayer: Seq[Wizard]

}
