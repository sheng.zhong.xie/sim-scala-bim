package it.unibo.ssb.model.remote

import java.util.concurrent.TimeUnit

import it.unibo.ssb.controller.SHA256
import it.unibo.ssb.model.remote.Auth.UserPassAuth.DefaultDuration
import javax.security.auth.login.CredentialNotFoundException

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/** The trait models an authentication object, like username-password couple, or a token. */
trait Auth extends Serializable {

  /**
    * Check if the authentication object is valid.
    *
    * @return true if valid, false otherwise
    */
  def check(): Boolean

  /**
    * Check if the authentication object is valid.
    *
    * @return a future with true if valid, false otherwise
    */
  def checkFuture(): Future[Boolean]

  /**
    * Get an authentication object equivalent to this one, if possible.
    *
    * @return the authentication object, if any
    */
  def renew(): Option[Auth]

  /**
    * Get an authentication object equivalent to this one, if possible.
    *
    * @return a future with the authentication object, if any
    */
  def renewFuture(): Future[Option[Auth]]
}

object Auth {

  trait UserAuth extends Auth {
    def username: String

    override def renew(): Option[UserAuth]

    override def renewFuture(): Future[Option[UserAuth]]
  }

  abstract class UserPassAuth(override val username: String, private val password: String) extends UserAuth {
    def passwordHash: String = SHA256.crypt(password)

    override def check(): Boolean = Await.result(checkFuture(), DefaultDuration)

    override def renew(): Option[UserAuth] = Await.result(renewFuture(), DefaultDuration)
  }

  final case class Anonymous(private val anonymousIdentifier: String = "")
    extends UserPassAuth(s"Anonymous$anonymousIdentifier", "") {

    override def checkFuture(): Future[Boolean] =
      Future.failed(new CredentialNotFoundException("Can't log an anonymous user"))

    /**
      * Get an authentication object equivalent to this one, if possible.
      *
      * @return a future with the authentication object, if any
      */
    override def renewFuture(): Future[Option[UserAuth]] =
      Future.failed(new CredentialNotFoundException("Can't register an anonymous user"))
  }

  object UserPassAuth {
    final val DefaultDuration = Duration(1, TimeUnit.MINUTES)

    def unapply(auth: UserPassAuth): (String, String) = (auth.username, auth.passwordHash)
  }

}