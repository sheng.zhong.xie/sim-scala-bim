package it.unibo.ssb.model

package object remote {
  final val DefaultHostname = "localhost"
  final val DefaultIpHostname = "192.168.5.230"
  final val AkkaSystemName = "SSBSystem"
  final val DefaultHostNameServer = "213.209.215.68"
  final val DefaultApiPort = 80
  final val DefaultAkkaPort = 4000
  final val DefaultServerAkkaPort = 443
  final val AuthRoute = "/auth"
  final val RegistrationRoute = "/register"
  final val FriendsRoute = "/friends"
  final val ScoreRoute = "/score"
  final val PlayersListRoute = "/players"
  final val HallOfFameRoute = "/hallOfFame"
  final val FriendsRequestRoute = "/friendsRequest"
  final val RequestFriendshipRoute = "/requestFriendship"
  final val AcceptFriendshipRoute = "/acceptFriendship"
  final val RankingRoute = "/rank"
  final val DiscoveryRoute = "/discovery"
  final val FriendsCandidateRoute = "/candidateFriends"
  final val ApiScore = "score"
  final val ApiRank = "rank"
  final val ApiUsername = "username"
  final val ApiPwHash = "pwhash"
  final val ApiFriend = "friend"
  final val ApiDiscovery = "discovery"
  final val ActorOfDiscoveryIndex = 0
  final val ActorOfMasterLobbyIndex = 1
  final val ActorOfMatchMakingIndex = 2
}
