package it.unibo.ssb.model

import java.util.NoSuchElementException

import scala.collection.mutable.ListBuffer

case class WizardGameBoardImpl(wizard: Wizard) extends WizardGameBoard {

  private var cards: ListBuffer[Card] = ListBuffer[Card]()

  /**
    *
    * @return a safe copy of the carts of the wizard
    */
  override def cardsList: Seq[Card] = {
    cards.seq
  }

  /**
    *
    * @param card to discard
    */
  override def discard(card: Card): Unit = {
    if (cards contains card) cards -= card
    else throw new NoSuchElementException("Card not found")
  }

  /**
    *
    * @param card to add to the wizard board
    */
  override def addCard(card: Card): Unit = cards += card

  /**
    *
    * @param card to search
    *
    * @return true if the card is present
    */
  override def containCard(card: Card): Boolean = cards contains card

  override def idWizard: Int = wizard.id
}

