package it.unibo.ssb

import com.typesafe.scalalogging.LazyLogging
import it.unibo.ssb.model.{GameBoard, Wizard}

package object controller extends LazyLogging {

  /** Time a player has to do a move. */
  val MaximumTimeStepToResponse = 60

  /** Extra time given to player to do his move. */
  val TimeTolerance = 10

  val scoreToWin: Int = 8

  def createBoard(wizards: List[Wizard]): GameBoard = {
    logger.debug("Board Created")
    GameBoard(wizards)
  }

  def createWizards(players: List[String]): Seq[Wizard] = {
    (1 to players.size).map(p => Wizard(players(p - 1), p))
  }

  def notifyWizardIsHisTurn(wizard: Wizard): Unit = {
    logger.debug("ACTUAL TURN: " + wizard.id)
  }

  def notifyVictory(wizard: Wizard): Unit = {
    logger.debug("WINNER: " + wizard.id)
  }

  def notifyMultipleVictory(winners: List[Wizard]): Unit = {
    logger.debug("WINNERS: " + winners.map(w => "" + w.id).mkString(" "))
  }

  def notifyWizardIsCastingSpell(wizard: Wizard, spellID: Int): Unit = {
    logger.debug("SPELL CASTED: WID" + wizard.id + " SID: " + spellID)
  }

  def notifyWizardCastedWorgSpell(wizard: Wizard, spellID: Int): Unit = {
    logger.debug("WRONG CAST: WID" + wizard.id + " SID: " + spellID)
  }
}
