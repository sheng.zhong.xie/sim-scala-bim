package it.unibo.ssb.controller.game

import it.unibo.ssb.model.{GameBoard, Wizard}

/** The interface defines methods for a Game Controller. */
trait GameControllerForPlay {

  /**
    * Method to call to tell to controller that a wizard wish to launch a spell
    *
    * @param wizard      the wizards who want cast spell
    * @param spellToCast the spell casted
    *
    * @throws IllegalArgumentException if wizard is not in list
    * @throws IllegalStateException    is state differ from started
    */
  @throws(classOf[IllegalArgumentException])
  @throws(classOf[IllegalStateException])
  def wizardsCastSpell(wizard: Wizard, spellToCast: Int): Boolean

  /**
    * Method used to make next wizard in list active to play.
    * For examples when player selects to stop his turn and let the next one go has to call this method.
    *
    * Wizard has done his moment so it time for next wizard in list.
    * <p>
    * First af all this method checks:
    * <ul>
    * <li>living wizard
    * <li>if someone won
    * <li>if turn is over (last wizard has done); in that case, next turn starts
    * </ul>
    * After that he set the current wizard the next living wizard the list of all wizards and start moment
    */
  def nextWizardMoment(): Unit


  /**
    * Method for get board game.
    *
    * @return the bard game
    */
  def getGameBoard: GameBoard

  /**
    * Return the current wizard turn.
    *
    * @return the wizard that should play in current turn
    */
  def wizardTurn: Wizard

}
