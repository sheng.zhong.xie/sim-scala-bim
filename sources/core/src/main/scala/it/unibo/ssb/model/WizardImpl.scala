package it.unibo.ssb.model

import it.unibo.ssb.model.Wizard.MaxLifePoints

protected case class WizardImpl(name: String, id: Int) extends Wizard {

  private var lifePoint: Int = MaxLifePoints

  /**
    *
    * @return true if the lifePoint of the wizard are = 0, else false
    */
  override def isDead: Boolean = !isAlive

  /**
    *
    *
    * @return true if the lifePoint of the wizard are > 0, else false
    */
  override def isAlive: Boolean = lifePoint > 0

  /**
    *
    * @param value of point to add to the wizard life
    */
  override def incLife(value: Int = 1): Unit = {
    lifePoint += value
    if (lifePoint > MaxLifePoints) lifePoint = MaxLifePoints
  }

  /**
    *
    * @param value of point to dec to the wizard life
    */
  override def decLife(value: Int = 1): Unit = {
    lifePoint -= value
    if (lifePoint < 0) lifePoint = 0
  }

  /**
    *
    * @return the spell launched
    */
  override def spellToLaunch: Int = 1

  /**
    *
    * @return the life points
    */
  override def lifePoints: Int = lifePoint

  /**
    * reset the life points
    */
  override def resetLifePoints(): Unit = {
    lifePoint = MaxLifePoints
  }

  override def equals(that: Any): Boolean = {
    that match {
      case thatWizard: Wizard => thatWizard.id == this.id
      case _ => super.equals(that)
    }
  }

  // Need to override for ScalaStyle best practice (when overriding equals() method
  override def hashCode(): Int = super.hashCode()
}
