package it.unibo.ssb.controller.messages

import it.unibo.ssb.controller.GameTypes
import it.unibo.ssb.model.lobby.Settings

sealed trait LobbyMessage extends Message

object LobbyMessage {

  sealed trait ACKMessage extends LobbyMessage

  /**
    * To send when Lobby is created to LobbyMaster
    */
  final case class IAmAlive() extends ACKMessage

  /**
    * To send when lobby has received setting and he is ready to work
    */
  final case class IAmReady() extends ACKMessage

  /**
    * From lobby to player just added.
    */
  final case class Hello(players: List[String], owner: String) extends ACKMessage

  /**
    * From Lobby Master to lobby so send game setting:
    * <li>Number of player
    * <li>Owner
    * <li>Type of Game (rank or unrank)
    * @param setting info about game
    */
  final case class Setting(setting: Settings) extends LobbyMessage

  /**
    * From clients to lobby to join it
    * @param clientOfPlayer ActorRef of clients
    */
  final case class Subscribe(clientOfPlayer: String) extends LobbyMessage

  /**
    * From Lobby to Joined players to notify another player joined the Lobby
    * @param player player joined
    */
  final case class HasJoined(player: String) extends LobbyMessage

  /**
    * From owner to lobby. To destroy the game
    */
  final case class DestroyLobby() extends LobbyMessage

  /**
    * From lobby to players when owner decide to destroy the game
    */
  final case class LobbyDestroyed()  extends LobbyMessage

  /**
    * From lobby to matchmaking to remove player
    */
  final case class PlayerLefted() extends LobbyMessage

  /**
    * From lobby to master lobby when all players have joined the lobby or timer is reached
    * Lobby must be added in an waiting list according his setting.si
    * @param gameType info about game
    */
  final case class SubscribeLobby(gameType: GameTypes) extends LobbyMessage

  /**
    * From Lobby to players to tell them game is stared and now they are in a
    * queue waiting other player to create match
    */
  final case class WaitingState() extends LobbyMessage
  /**
    * From owner of lobby to lobby when he decide to start
    */
  final case class ForceStart() extends LobbyMessage

}