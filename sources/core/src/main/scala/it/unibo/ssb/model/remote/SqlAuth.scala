package it.unibo.ssb.model.remote

import akka.http.scaladsl.model.StatusCodes
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.ssb.controller.SHA256
import it.unibo.ssb.model.remote.Auth.UserPassAuth

import scala.concurrent.{Await, ExecutionContextExecutor, Future}

/**
  * Checks if the credentials are valid on a remote database.
  *
  * @param username the username
  * @param password the password
  */
case class SqlAuth(
    override val username: String,
    private val password: String)(
    implicit private val webClient: WebClient,
    implicit private val executor: ExecutionContextExecutor
) extends UserPassAuth(username, password) {

  override def checkFuture(): Future[Boolean] = webClient
    .post(DefaultApiPort, DefaultHostNameServer, AuthRoute)
    .addQueryParam(ApiUsername, username)
    .addQueryParam(ApiPwHash, SHA256.crypt(password))
    .sendFuture()
    .map(response => response.statusCode() == StatusCodes.OK.intValue)

  override def renewFuture(): Future[Option[Auth.UserAuth]] = {
    checkFuture().map(valid => if (valid) {Future(Option(this))} else {SqlAuth.registerFuture(this)}).flatten
  }
}

object SqlAuth {

  def registerFuture(auth: SqlAuth)
    (implicit webClient: WebClient, executor: ExecutionContextExecutor): Future[Option[SqlAuth]] =
    registerFuture(auth.username, auth.password)

  private def register(auth: SqlAuth)(implicit webClient: WebClient, executor: ExecutionContextExecutor): Option[SqlAuth] =
    register(auth.username, auth.password)

  private def register(username: String, password: String)
    (implicit webClient: WebClient, executor: ExecutionContextExecutor): Option[SqlAuth] =
    Await.result(registerFuture(username, password), UserPassAuth.DefaultDuration)

  def registerFuture(username: String, password: String)
    (implicit webClient: WebClient, executor: ExecutionContextExecutor): Future[Option[SqlAuth]] = webClient
    .put(DefaultApiPort, DefaultHostNameServer, RegistrationRoute)
    .addQueryParam(ApiUsername, username)
    .addQueryParam(ApiPwHash, SHA256.crypt(password))
    .sendFuture()
    .map(response =>
      if (response.statusCode() == StatusCodes.OK.intValue) Option(SqlAuth(username, password))
      else None)
}
