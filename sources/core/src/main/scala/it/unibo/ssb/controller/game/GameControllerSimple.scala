package it.unibo.ssb.controller.game

import it.unibo.ssb.controller.game.GameControllerSimple.MaximumPlayer
import it.unibo.ssb.controller.{createBoard, createWizards, notifyWizardCastedWorgSpell, notifyWizardIsCastingSpell, notifyWizardIsHisTurn, scoreToWin}
import it.unibo.ssb.model.{Card, GameBoard, Wizard}

import scala.collection.mutable.ListBuffer

case class GameControllerSimple(ranked: Boolean) extends GameController {

  private var round = 1
  private var currentWizardMoment: Wizard = _
  private var gameBoard: GameBoard = _
  private var wizards = ListBuffer.empty[Wizard]
  private var leftWizard = ListBuffer.empty[Wizard]
  private var state: Int = internalState.notInitialized
  private var winner: Option[List[Wizard]] = None
  private var tower: Ranking = _
  private var gWinner = List.empty[(Wizard, Boolean, Int)]


  override def currentRound: Int = round

  override def initializeGame(): Unit = {

    checkState(internalState.notInitialized, "State must be notInitialized to call this method")
    gameBoard = createBoard(wizards.toList)
    tower = Ranking(wizards.toList, scoreToWin)
    // initialazed gui
    state = internalState.notStarted
  }

  override def startGame(): Unit = {
    checkState(internalState.notStarted, "State must be notStarted to call this method")
    state = internalState.started
    nextTurn()
  }


  def nextRound(): Unit = {
    checkState(internalState.started, "State has to be started to call this method")
    round += 1

    val gameWinner = tower.checkForWinner()
    if (gameWinner.nonEmpty) {
      gWinner = tower.ranking.map(w => (w._1, gameWinner.map(_._1).contains(w._1), w._2))
        .sortWith(_._3 > _._3).toList
    } else {
      if((gameBoard.livingWizard ++ gameBoard.deadWizard).size == 1){
        gWinner = (gameBoard.livingWizard ++ gameBoard.deadWizard).map(w => (w, true, 8)).toList
      }else {
        gameBoard = createBoard(wizards.toList.map(w => Wizard(w.name, w.id)))
        state = internalState.started
        winner = None
        nextTurn()
      }
    }
  }

  override def setPlayer(numberOfHumanPlayers: List[String]): Unit = {
    checkState(internalState.notInitialized, "State must be notInitialized to call this method")

    if (numberOfHumanPlayers.size > MaximumPlayer) {
      throw new IllegalArgumentException("Maximum players: " + MaximumPlayer)
    }
    var allWizards = createWizards(numberOfHumanPlayers)
    wizards ++= allWizards
  }

  override def wizardsCastSpell(wizard: Wizard, spellToCast: Int): Boolean = {

    var canCast = true

    // check everything is fine
    checkState(internalState.started, "State must be started to call this method")
    checkWizardTurn(wizard)

    // cast spell.. if he can the spell will happen otherwise he will lose 1 point or more
    canCast = checkIfWizardCanCastSpell(wizard, spellToCast)
    if (canCast) {
      executeSpell(wizard, Card(spellToCast))
      notifyWizardIsCastingSpell(wizard, spellToCast)
    } else {
      notifyWizardCastedWorgSpell(wizard, spellToCast)
      if (spellToCast == 1) {
        gameBoard.livingWizard(gameBoard.livingWizard.indexOf(currentWizardMoment)).decLife(Dice.three().throws(1))
      } else {
        gameBoard.livingWizard(gameBoard.livingWizard.indexOf(currentWizardMoment)).decLife()
      }
    }
    // check victory
    winner = checkSomeOneHasWon()

    // I must update ranking
    if (winner.nonEmpty) {
      if (winner.get.size == 1) tower.updateRanking(winner.get.head, 2)
      winner.get.foreach(w => tower.updateRanking(w, 1))
    }

    canCast
  }

  /**
    * This method check if some wizard won:
    * <ul>
    * <li> he has life points greater tha zero and he finished cards
    * <li> someone is dead cause him
    * <li> he is dead so all the other are the winner
    * </ul>
    *
    * @return
    */
  private def checkSomeOneHasWon(): Option[List[Wizard]] = {

    val playingWizards = gameBoard.livingWizard.toList
    // check option 1
    val winners = gameBoard.wizardGameBoardList.filter(h => h.cardsList.isEmpty &&
      playingWizards.filter(_.id == h.idWizard).head
        .isAlive)
      .map(p => playingWizards.filter(_.id == p.idWizard).head).toList
    if (winners.size == 1) {
      Some(List[Wizard]() :+ currentWizardMoment)
    }else {
      var ret : Option[List[Wizard]] = None
      //check option 2 and 3
      if (gameBoard.deadWizard.nonEmpty) {
        // check option 3
        if (gameBoard.deadWizard.contains(currentWizardMoment)) {
          ret = Some(playingWizards)
        }
        // option 2
        else ret =  Some(List[Wizard]() :+ currentWizardMoment)
      }
      ret
    }
  }

  /**
    * check if wizard that is casting spells is a player and it's his turn
    *
    * @param wizard wizard to check
    *
    * @throws java.lang.IllegalArgumentException if not in list or not his turn
    */
  @throws(classOf[IllegalArgumentException])
  private def checkWizardTurn(wizard: Wizard): Unit = {
    if (!wizards.contains(wizard)) throw new IllegalArgumentException("Wizard not in list")
    if (!wizard.equals(currentWizardMoment)) throw new IllegalArgumentException(s"Not ${wizard.name} turn." +
      s" It's $wizardTurn turn")
  }

  /**
    * Method used to check if wizard can cast that spell
    *
    * @param wizard      wizard to check
    * @param spellToCast spell to check
    *
    * @return true he can cast
    *         false he can not
    */
  private def checkIfWizardCanCastSpell(wizard: Wizard, spellToCast: Int): Boolean = {
    gameBoard.wizardGameBoard(wizard).containCard(Card(spellToCast))
  }

  /**
    * this method takes care to launch spell and handle cards
    * Move cards from wizard's hand to used cards
    *
    * @param wizard - wizard who casts spell
    * @param card   - card that represents spell
    */
  private def executeSpell(wizard: Wizard, card: Card): Unit = {

    // effect takes place
    card.spell(gameBoard, gameBoard.livingWizard.filter(_ == wizard).head)

    // cards is removed from wizards hand and placed in used cards
    gameBoard.wizardGameBoard(wizard).discard(card)
    gameBoard.sharedGameBoard.putCardInUsedCards(card)
  }

  override def nextWizardMoment(): Unit = {
    // update living wizard
    // playingWizards = gameBoard.methodToGetAllWizard
    checkState(internalState.started, "State must be started")

    // refill the hand of the current wizard
    while (gameBoard.wizardGameBoard(currentWizardMoment).cardsList.size < 5 &&
      gameBoard.sharedGameBoard.deckSize > 0) {
      val card = gameBoard.sharedGameBoard.drawCard()
      gameBoard.wizardGameBoard(currentWizardMoment).addCard(card)
    }

    // check if current wizard is the last in list
    if (currentWizardMoment != wizards.last) {
      currentWizardMoment = wizards(wizards.indexOf(currentWizardMoment) + 1)
      updateWizards()
      wizardMoment()
    } else {
      updateWizards()
      nextTurn()
    }
  }

  /**
    * Method used to start next turn.
    * Turn is a piece of game where each wizards according to predefined order
    * make their decision. One after one, during round, they cast their spells
    * and gain or lose life point, cards etc etc..
    *
    * @throws java.lang.IllegalStateException if round not started
    */
  private def nextTurn(): Unit = {
    checkState(internalState.started, "State must be started to call this method")

    // set current wizard as the first of the list
    currentWizardMoment = wizards.head

    // start the turn
    wizardMoment()

  }

  /**
    * WizardMoment is a pice of turn where current wizard makes his choise
    * Cast his spell ect etc ...
    * This method notifies current wizard is his moment to play
    */
  private def wizardMoment(): Unit = {
    //tell next wizard is his time to play
    notifyWizardIsHisTurn(currentWizardMoment)

  }

  /**
    * Check if current state is state otherwise throws exception
    * Msg will be used as msg of exception
    *
    * @param stateToCheck state desired
    * @param msg          message for exception
    */
  private def checkState(stateToCheck: Int, msg: String): Unit = {
    if (state != stateToCheck) {
      throw new IllegalStateException(msg)
    }
  }

  private def updateWizards(): Unit = {
    wizards = ListBuffer() ++= getGameBoard.livingWizard
    leftWizard = ListBuffer() ++= getGameBoard.leftWizard
  }

  override def getGameBoard: GameBoard = gameBoard

  override def wizardDisconnected(wizard: Wizard): Unit = {
    gameBoard.removeWizard(wizard)
  }

  override def wizardsPlayer: Seq[Wizard] = wizards

  override def wizardTurn: Wizard = currentWizardMoment

  override def getWinners: Option[List[Wizard]] = winner

  override def getRanking: List[(Wizard, Int)] = tower.ranking.toList

  override def gameWinners: List[(Wizard, Boolean, Int)] = gWinner


  /**
    * This object collects all different values a state can be
    */
  case object internalState {

    def notInitialized: Int = 0

    def notStarted: Int = 1

    def started: Int = 2
  }

}

object GameControllerSimple {
  final val MaximumPlayer: Int = 5

  /**
    * Creates a GameControllerSimple and sets players
    *
    * @param players players
    * @param ranked  game mode
    *
    * @return
    */
  def apply(players: List[String], ranked: Boolean): GameControllerSimple = {
    val g = apply(ranked)
    g.setPlayer(players)
    g
  }

  /**
    * Creates a GameControllerSimple
    *
    * @param ranked game mode
    *
    * @return
    */
  def apply(ranked: Boolean): GameControllerSimple = {
    new GameControllerSimple(ranked)
  }

}
