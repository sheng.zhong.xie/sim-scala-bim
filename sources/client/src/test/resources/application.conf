akka {
  loggers = ["akka.event.slf4j.Slf4jLogger"]

  # Log level used by the configured loggers (see "loggers") as soon
  # as they have been started; before that, see "stdout-loglevel"
  # Options: OFF, ERROR, WARNING, INFO, DEBUG
  loglevel = "WARNING"

  # Log level for the very basic logger activated during ActorSystem startup.
  # This logger prints the log messages to stdout (System.out).
  # Options: OFF, ERROR, WARNING, INFO, DEBUG
  stdout-loglevel = "WARNING"

  # Filter of log events that is used by the LoggingAdapter before
  # publishing log events to the eventStream. It can perform
  # fine grained filtering based on the log source.
  #
  # logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"

  # Log the complete configuration at INFO level when the actor system is started.
  # This is useful when you are uncertain of what configuration is used.
  log-config-on-start = on

  # Log at info level when messages are sent to dead letters.
  # Possible values:
  # on: all dead letters are logged
  # off: no logging of dead letters
  # n: positive integer, number of dead letters that will be logged
  log-dead-letters = on

  # Possibility to turn off logging of dead letters while the actor system
  # is shutting down. Logging is only done when enabled by 'log-dead-letters'
  # setting.
  log-dead-letters-during-shutdown = on

  # List FQCN of extensions which shall be loaded at actor system startup.
  # Should be on the format: 'extensions = ["foo", "bar"]' etc.
  # See the Akka Documentation for more info about Extensions
  #
  # extensions = []

  # JVM shutdown, System.exit(-1), in case of a fatal error,
  # such as OutOfMemoryError
  jvm-exit-on-fatal-error = on

  # Akka installs JVM shutdown hooks by default, e.g. in CoordinatedShutdown and Artery. This property will
  # not disable user-provided hooks registered using `CoordinatedShutdown#addCancellableJvmShutdownHook`.
  # This property is related to `akka.coordinated-shutdown.run-by-jvm-shutdown-hook` below.
  # This property makes it possible to disable all such hooks if the application itself
  # or a higher level framework such as Play prefers to install the JVM shutdown hook and
  # terminate the ActorSystem itself, with or without using CoordinatedShutdown.
  jvm-shutdown-hooks = on

  actor {

    # Either one of "local", "remote" or "cluster" or the
    # FQCN of the ActorRefProvider to be used; the below is the built-in default,
    # note that "remote" and "cluster" requires the akka-remote and akka-cluster
    # artifacts to be on the classpath.
    provider = "local"

    # The guardian "/user" will use this class to obtain its supervisorStrategy.
    # It needs to be a subclass of akka.actor.SupervisorStrategyConfigurator.
    # In addition to the default there is akka.actor.StoppingSupervisorStrategy.
    #
    # guardian-supervisor-strategy = "akka.actor.DefaultSupervisorStrategy"

    # Timeout for ActorSystem.actorOf
    creation-timeout = 20s

    # Serializes and deserializes (non-primitive) messages to ensure immutability,
    # this is only intended for testing.
    # serialize-messages = off

    # Serializes and deserializes creators (in Props) to ensure that they can be
    # sent over the network, this is only intended for testing. Purely local deployments
    # as marked with deploy.scope == LocalScope are exempt from verification.
    #
    # serialize-creators = off

#    debug {
#      # enable function of actor.loggable(), which is to log any received message
#      # at DEBUG level, see the “Testing actor Systems” section of the Akka
#      # Documentation at http://akka.io/docs
#      receive = off
#
#      # enable DEBUG logging of all AutoReceiveMessages (Kill, PoisonPill etc.)
#      autoreceive = off
#
#      # enable DEBUG logging of actor lifecycle changes
#      lifecycle = off
#
#      # enable DEBUG logging of all LoggingFSMs for events, transitions and timers
#      fsm = off
#
#      # enable DEBUG logging of subscription changes on the eventStream
#      event-stream = off
#
#      # enable DEBUG logging of unhandled messages
#      unhandled = off
#
#      # enable WARN logging of misconfigured routers
#      router-misconfiguration = off
#    }

    # SECURITY BEST-PRACTICE is to disable java serialization for its multiple
    # known attack surfaces.
    #
    # This setting is a short-cut to
    # - using DisabledJavaSerializer instead of JavaSerializer
    # - enable-additional-serialization-bindings = on
    #
    # Completely disable the use of `akka.serialization.JavaSerialization` by the
    # Akka Serialization extension, instead DisabledJavaSerializer will
    # be inserted which will fail explicitly if attempts to use java serialization are made.
    #
    # The log messages emitted by such serializer SHOULD be treated as potential
    # attacks which the serializer prevented, as they MAY indicate an external operator
    # attempting to send malicious messages intending to use java serialization as attack vector.
    # The attempts are logged with the SECURITY marker.
    #
    # Please note that this option does not stop you from manually invoking java serialization
    #
    # The default value for this might be changed to off in future versions of Akka.
    #
    # allow-java-serialization = on

    # Entries for pluggable serializers and their bindings.
    #
    # serializers {
    #   java = "akka.serialization.JavaSerializer"
    #   bytes = "akka.serialization.ByteArraySerializer"
    # }

    # Class to Serializer binding. You only need to specify the name of an
    # interface or abstract base class of the messages. In case of ambiguity it
    # is using the most specific configured class, or giving a warning and
    # choosing the “first” one.
    #
    # To disable one of the default serializers, assign its class to "none", like
    # "java.io.Serializable" = none
    #
    # serialization-bindings {
    #   "[B" = bytes
    #   "java.io.Serializable" = java
    # }

    # Additional serialization-bindings that are replacing Java serialization are
    # defined in this section for backwards compatibility reasons. They are included
    # by default but can be excluded for backwards compatibility with Akka 2.4.x.
    # They can be disabled with enable-additional-serialization-bindings=off.
    #
    # This should only be needed for backwards compatibility reasons.
    #
    # enable-additional-serialization-bindings = on

    # Additional serialization-bindings that are replacing Java serialization are
    # defined in this section for backwards compatibility reasons. They are included
    # by default but can be excluded for backwards compatibility with Akka 2.4.x.
    # They can be disabled with enable-additional-serialization-bindings=off.
    #
    # additional-serialization-bindings {
    # }

    # Log warnings when the default Java serialization is used to serialize messages.
    # The default serializer uses Java serialization which is not very performant and should not
    # be used in production environments unless you don't care about performance. In that case
    # you can turn this off.
    warn-about-java-serializer-usage = on

    # To be used with the above warn-about-java-serializer-usage
    # When warn-about-java-serializer-usage = on, and this warn-on-no-serialization-verification = off,
    # warnings are suppressed for classes extending NoSerializationVerificationNeeded
    # to reduce noize.
    warn-on-no-serialization-verification = on

    # Configuration namespace of serialization identifiers.
    # Each serializer implementation must have an entry in the following format:
    # `akka.actor.serialization-identifiers."FQCN" = ID`
    # where `FQCN` is fully qualified class name of the serializer implementation
    # and `ID` is globally unique serializer identifier number.
    # Identifier values from 0 to 40 are reserved for Akka internal usage.
    #
    # serialization-identifiers {
    #   "akka.serialization.JavaSerializer" = 1
    #  "akka.serialization.ByteArraySerializer" = 4
    # }
  }

}