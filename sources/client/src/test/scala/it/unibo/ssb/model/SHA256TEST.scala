package it.unibo.ssb.model

import it.unibo.ssb.controller.SHA256
import org.scalatest.FunSuite

class SHA256TEST extends FunSuite {
  val test1: String = "stringa di prova"
  val test2: String = "prova di stringa"
  val test3: String = "70575E2F0D6125F80D84F5EFD802D72F0DF06EB491BA9F727A93ACE066208E25"
  assert(SHA256.crypt(test1).equals("70575E2F0D6125F80D84F5EFD802D72F0DF06EB491BA9F727A93ACE066208E25"))
  assert(SHA256.crypt(test2).equals("8EF84CC699DC5EE839EF5E5B4FC725CE2A6F3DA1298C7B46E8B6F8212D6D674A"))
  assert(SHA256.crypt(test3).equals("870B7576EB173FD6494EB5D133648F517231F8AA7B68AEB8CACCCB3949439ACD"))
}
