package it.unibo.ssb.tmptest.actor

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import com.typesafe.scalalogging.LazyLogging
import it.unibo.ssb.controller.messages.DiscoveryMessage.GameInvitation
import it.unibo.ssb.controller.messages.GameMessage.IsTimeToPlay
import it.unibo.ssb.controller.messages.LobbyMessage.{DestroyLobby, ForceStart, HasJoined, Hello, LobbyDestroyed, Setting, Subscribe, WaitingState}
import it.unibo.ssb.model.lobby.Settings

import scala.collection.mutable.ListBuffer

class LobbyForTest(actorRef: ActorRef) extends Actor with LazyLogging {

  private val clients = ListBuffer[(ActorRef, String)]()
  private var owner: (ActorRef, String) = _
  private var gameType: Int = 1
  private var invitedPlayer = 1
  private var isRanked = false
  private var isDone = false


  def destroyLobby(sender: ActorRef): Unit = {
    if (sender == owner._1) {
      clients.foreach(_._1 ! LobbyDestroyed())
      self ! PoisonPill
    }
  }

  def receive: Receive = {
    case Setting(setting) => handleSettings(setting)
    case ForceStart() => forceStart(sender())
    case Subscribe(player) => handleSubscription(player, sender())
    case DestroyLobby() => destroyLobby(sender())
  }

  private def fakePlayerJoining(nOfPlyer : Int): Unit ={
    for(id <- 1 to nOfPlyer){
      context.actorOf(Props(new FakePlayer(id))) ! GameInvitation(self, owner._2)
    }
  }

   def handleSettings(settings: Settings): Unit = {
     owner = settings.owner
     gameType = settings.nPlayer
     invitedPlayer = settings.nInvitedPlayer
     isRanked = settings.ranked
     owner._1 ! Hello(clients.map(_._2).toList, owner._2)
     clients += owner
     isDone = isRanked
    fakePlayerJoining(settings.nInvitedPlayer)
  }


   def startGame(): Unit = {
    clients.foreach(_._1 ! WaitingState())
     //Thread.sleep(4000)
      val gca = context.actorOf( Props(new GameControllerActorForTest(isRanked, clients.map(_._2).toList)))
      clients.foreach(c => {
        c._1 ! IsTimeToPlay(gca)
      })
  }

   def forceStart(sender: ActorRef): Unit = {
    if (sender == owner._1) {
      startGame()
    }
  }

   def handleSubscription(playerName: String, client: ActorRef): Unit = {
    if (!clients.map(_._1).contains(client) && !isDone) {
      client ! Hello(clients.map(_._2).toList, owner._2)
      clients.foreach(_._1 ! HasJoined(playerName))
      clients += ((client, playerName))
      isDone = invitedPlayer == clients.size - 1
    }
    if (isDone) {
      startGame()
    }
  }

}
