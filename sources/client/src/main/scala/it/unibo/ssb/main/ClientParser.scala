package it.unibo.ssb.main

import it.unibo.ssb.main.ClientParser.{clientActorPathCommand => client, tornadoCommands}
import scopt.OptionParser

/**
  * Immutable Scopt command line options parser.
  *
  * @param name the name of the software the parser should show
  *
  * @see [[it.unibo.ssb.main.ClientConfig]]
  * @see [[https://github.com/scopt/scopt#immutable-parsing]]
  */
case class ClientParser(name: String) extends OptionParser[ClientConfig](name) {
  private val version: Option[String] = Option(getClass.getPackage.getImplementationVersion)

  head(name, s"${version.getOrElse("DEV")} version")

  opt[String]("host").abbr("lh").action((h, c) =>
    c.copy(akkaHostname = h)).text("Specify the hostname of the local actorSystem")

  opt[Int]("port").abbr("lp").action((p, c) =>
    c.copy(akkaPort = p)).text("Specify the port of the local actorSystem")

  opt[String]("remote-host").abbr("rh").action((h, c) =>
    c.copy(remoteApiHostname = h)).text("Specify the hostname of the remote server")

  opt[Int]("remote-api-port").abbr("rp").action((p, c) =>
    c.copy(remoteApiPort = p)).text("Specify the port of the remote HTTP server")

  opt[String]("remote-akka-host").abbr("ah").action((h, c) =>
    c.copy(remoteAkkaHostname = h)).text("Specify the hostname of the remote akka actor")

  opt[Int]("remote-akka-port").abbr("ap").action((p, c) =>
    c.copy(remoteAkkaPort = p)).text("Specify the port of the remote akka actor")

  if (version.isEmpty) { // if in DEV mode, show extra params in help
    tornadoCommands.foreach(triple => opt[Unit](name = triple._1).abbr(triple._2).text(s"DEV Option: ${triple._3}"))
  }

  help("help").abbr("h").text("prints this usage text")

  override def showUsageOnError: Boolean = true

  /**
    * List all accepted CLI parameters.
    *
    * @return the sequence containing each param (both named and abbreviated) as strings
    */
  final def acceptedCommands: Seq[String] = commands
    .map(o => (o.name, o.shortOpt))
    .flatMap(pair => Seq(pair._1, pair._2.getOrElse("")))
    .filter(_.nonEmpty)
}

object ClientParser {

  /**
    * Command line options natively supported by [[tornadofx.App Application]].
    *
    * They are passed down via [[javafx.application.Application JavaFX Application launcher]]
    *
    * @see [[https://github.com/edvin/tornadofx/wiki/Application-Startup#built-in-startup-parameters]]
    */
  final val tornadoCommands: Seq[(String, String, String)] = Seq[(String, String, String)](
    ("live-stylesheets", "ls", "Stylesheets will be reloaded whenever the Stage gains focus"),
    ("dump-stylesheets", "ds", "Stylesheets will be printed to stdout when they are loaded or reloaded"),
    ("live-views", "lv", "Views will be reloaded whenever the Stages gains focus"),
    ("dev-mode", "d", "Enable all the developer functions with a single parameter"))

  /**
    * CLI param pair for the local Client actor path, in serialization form.
    *
    * Usually, it should be a local [[akka.actor.Actor]].
    */
  final val clientActorPathCommand: (String, String, String) = (
    TornadoApp.ClientActorRefParam,
    "cp",
    "Path of the Akka actor that handle the client")
}
