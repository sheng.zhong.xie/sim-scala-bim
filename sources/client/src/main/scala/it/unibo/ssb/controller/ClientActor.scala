package it.unibo.ssb.controller

import java.lang.{Boolean => JBoolean}
import java.util.NoSuchElementException
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, Props}
import akka.http.scaladsl.model.StatusCodes
import com.typesafe.scalalogging.LazyLogging
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonObject
import io.vertx.lang.scala.json.JsonArray
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.{HttpResponse, WebClient}
import it.unibo.ssb.controller.ClientActor.ScalaJsonArray
import it.unibo.ssb.controller.messages.AuthenticatedSocialMessage.{AcceptFriendRequest, AllFriendshipResponse,
  AllPlayers, Friends, GetAllPlayers, GetFriends, GetFriendshipRequests, GetHallOfFame, GetRankingPos, GetScore,
  HallOfFame, RankingPos, RequestFriendship, Score}
import it.unibo.ssb.controller.messages.ClientMessage._
import it.unibo.ssb.controller.messages.DiscoveryMessage.{ACK, ClientPresentationDiscovery, GameInvitation, MasterLobbyIs, WhoIsMasterLobby}
import it.unibo.ssb.controller.messages.GameMessage.{GuiReadyToPlay, IsTimeToPlay}
import it.unibo.ssb.controller.messages.LobbyMessage.{DestroyLobby, ForceStart, HasJoined, Hello, LobbyDestroyed, Subscribe, WaitingState}
import it.unibo.ssb.controller.messages.MasterLobbyMessage.CreateThisLobby
import it.unibo.ssb.controller.messages.{AuthenticatedSocialMessage, ClientMessage, DiscoveryMessage, GameMessage, LobbyMessage}
import it.unibo.ssb.main.ClientRunner
import it.unibo.ssb.model.remote.Auth.UserAuth
import it.unibo.ssb.model.remote._

import scala.collection.JavaConverters
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.Try

//noinspection ScalaStyle,ScalaStyle
class ClientActor extends Actor with LazyLogging {
  private final val webClient: WebClient = WebClient.create(Vertx.vertx())
  private implicit val ec: ExecutionContextExecutor = context.dispatcher

  private final var lobbyActor: Option[ActorRef] = None
  private final var guiActor: Option[ActorRef] = None
  private final var discoveryActor: Option[ActorRef] = None
  private final var masterLobby: Option[ActorRef] = None
  private final var game: Option[PlayerWantsToStartGame] = None

  override def receive: Receive = {
    case m: ClientMessage =>
      logger.debug("Received ClientMessage of type: " + m.getClass.getSimpleName)
      receiveClientMsg(m)
    case m: DiscoveryMessage =>
      logger.debug("Received DiscoveryMessage of type: " + m.getClass.getSimpleName)
      receiveDiscoveryMsg(m)
    case m: LobbyMessage =>
      logger.debug("Received LobbyMessage of type: " + m.getClass.getSimpleName)
      receiveLobbyMsg(m)
    case m: GameMessage =>
      receiveGameMessage(m)
    case m: AuthenticatedSocialMessage[_, _] =>
      receiveSocialMsg(m)
    case m: Any => logger.warn("Unexpected message: " + m.toString)
  }

  private def receiveLobbyMsg(m: LobbyMessage): Unit = m match {
    case Hello(owner, friendsList) => {
      guiActor.getOrElse(throw new IllegalStateException()) ! Hello(owner, friendsList)
    }
      if (lobbyActor.isEmpty) lobbyActor = Option(sender())
    case HasJoined(client) =>
      guiActor.getOrElse(throw new IllegalStateException()) ! HasJoined(client)
    case WaitingState() =>
      guiActor.getOrElse(throw new IllegalStateException()) ! WaitingState()
    case ForceStart() =>
      lobbyActor.getOrElse(throw new IllegalStateException()) ! ForceStart()
    case DestroyLobby() =>
      lobbyActor.getOrElse(throw new IllegalStateException()) ! DestroyLobby()
    case LobbyDestroyed() =>
      lobbyActor = Option.empty
      guiActor.getOrElse(throw new IllegalStateException()) ! LobbyDestroyed()
    case _ => logger.warn("Unexpected lobby message: " + m.toString)
  }

  private def receiveDiscoveryMsg(m: DiscoveryMessage): Unit = m match {
    case GameInvitation(lobby, client) =>
      lobbyActor = Option(lobby)
      guiActor.getOrElse(throw new IllegalStateException()) ! GameInvitation(lobby, client)
    case m: MasterLobbyIs if this.game.isDefined =>
      this.masterLobby = Some(m.actorRef)
      m.actorRef ! this.game.get
    case m: MasterLobbyIs =>
      this.masterLobby = Some(m.actorRef)
    case _: ACK =>
      sender() ! WhoIsMasterLobby()

    case _ => logger.warn("Unexpected discovery message: " + m.toString)
  }

  protected def receiveClientMsg(m: ClientMessage): Unit = m match {
    case GuiToManagerPresentation(actorRef) =>
      this.guiActor = Option(actorRef)
    case m: PlayerWantsToStartGame if masterLobby.isEmpty =>
      this.game = Some(m)
    case PlayerWantsToStartGame(auth, isRanked, maxPlayers, friendsInvited) =>
      if (masterLobby.isDefined) masterLobby.get ! CreateThisLobby(maxPlayers, isRanked, auth.username,
        if(friendsInvited.isEmpty) None else Option(friendsInvited) )
      else throw new Exception("MASTERLOBBY NON PRESENTE")
    case PlayerAcceptsRequest(myPlayerName, lobbyActorRef) =>
      lobbyActorRef ! Subscribe(myPlayerName)
    case ram: RequestAuthenticationMessage =>
      logger.debug("Received RequestAuthenticationMessage of type: " + ram.getClass.getSimpleName)
      receiveAuthMsg(ram)
    case _ => logger.warn("Unexpected client message: " + m.toString)
  }

  private def receiveAuthMsg(m: RequestAuthenticationMessage): Unit = m match {
    case RequestAuthenticationMessage(auth, registration) =>
      val sender: ActorRef = this.sender()
      (if (registration) {
        logger.debug("Registration requested")
        auth.renewFuture()
      } else {
        logger.debug("Authentication requested")
        auth.checkFuture()
      })
        .onComplete(tryAuth => sender ! ResponseAuthenticationMessage(tryAuth.toEither match {
          case Right(optAuth) => optAuth match {
            case Some(_) =>
              logger.debug("Registration completed successfully")
              presentClient(auth)
              StatusCodes.OK
            case None =>
              logger.warn("Registration failed: no valid auth returned")
              StatusCodes.Unauthorized
            case b: Boolean if b =>
              logger.debug("Authentication completed successfully")
              presentClient(auth)
              StatusCodes.OK
            case b: JBoolean if b =>
              logger.debug("Authentication completed successfully")
              presentClient(auth)
              StatusCodes.OK
            case _: Boolean | _: JBoolean =>
              logger.warn("Authentication failed: invalid credentials")
              StatusCodes.Unauthorized
            case _ =>
              logger.error("UNEXPECTED WRONG MATCH: " + optAuth.getClass.getName)
              StatusCodes.Unauthorized
          }
          case Left(t) =>
            logger.warn("Registration failed for exception: " + t.getLocalizedMessage, t)
            StatusCodes.BadRequest
        }))
  }

  private def presentClient(auth: UserAuth): Unit = {
    if (discoveryActor.isEmpty) getDiscoveryActor.onComplete(tryActorRef => {
      discoveryActor = tryActorRef.toOption
      if (discoveryActor.isDefined) {
        discoveryActor.get ! ClientPresentationDiscovery(auth.username, self)
      } else throw new Exception("API FALLITA")
    })
    else discoveryActor.get ! ClientPresentationDiscovery(auth.username, self)
  }

  private def getDiscoveryActor: Future[ActorRef] = {
    webClient
      .get(DefaultApiPort, DefaultHostNameServer, DiscoveryRoute)
      .sendFuture()
      .map(r => if (r.statusCode() == StatusCodes.OK.intValue) Option(r.bodyAsJsonObject()) else None)
      .flatMap(optDiscovery => optDiscovery.flatten match {
        case Some(json) => this.context
          .system
          .actorSelection(json.getString(ApiDiscovery))
          .resolveOne(FiniteDuration.apply(10, TimeUnit.SECONDS))
        case None => Future.failed(new NoSuchElementException())
      })
  }

  private def receiveGameMessage(m: GameMessage): Unit = m match {
    case IsTimeToPlay(_) =>
      guiActor.getOrElse(throw new IllegalStateException()) ! m
    case GuiReadyToPlay(gameController, gameGuiActor) =>
      val gameActor = context.actorOf(Props(new GameActor(gameController, gameGuiActor)))
      gameGuiActor ! IsTimeToPlay(gameActor)
    case _ => logger.warn("Unexpected game message: " + m.toString)
  }

  private def receiveSocialMsg(m: AuthenticatedSocialMessage[_, _]): Unit = m match {
    case RequestFriendship(auth, friend) =>
      doRest(HttpMethod.PUT, RequestFriendshipRoute,
        resp => resp.statusCode(),
        (ApiUsername, auth.username), (ApiPwHash, auth.passwordHash), (ApiFriend, friend))
        .onComplete(tryInt => if (tryInt.toOption.contains(StatusCodes.OK.intValue)) {
          logger.debug(s"Friend acceptance to $friend sent correctly")
        } else {
          logger.warn("Can't send friend request")
        })
    case AcceptFriendRequest(auth, friend) =>
      doRest(HttpMethod.PUT, AcceptFriendshipRoute,
        resp => resp.statusCode(),
        (ApiUsername, auth.username), (ApiPwHash, auth.passwordHash), (ApiFriend, friend))
        .onComplete(tryInt => if (tryInt.toOption.contains(StatusCodes.OK.intValue)) {
          logger.debug(s"Friend request to $friend sent correctly")
        } else {
          logger.warn("Can't send friend request")
        })
    case GetFriendshipRequests(auth) =>
      val sender = this.sender()
      doRest(HttpMethod.GET, FriendsRequestRoute,
        resp => if (resp.statusCode() == StatusCodes.OK.intValue) resp.bodyAsJsonArray() else None,
        (ApiUsername, auth.username), (ApiPwHash, auth.passwordHash))
        .onComplete(tryJson => consumeTry[JsonArray](tryJson, jsonArray =>
          sender ! AllFriendshipResponse(jsonArray.toScalaListOfString),
          "Request list of friendship requests failed"))
    case GetFriends(auth) =>
      val sender = this.sender()
      doRest(HttpMethod.GET, FriendsRoute,
        resp => if (resp.statusCode() == StatusCodes.OK.intValue) resp.bodyAsJsonArray() else None,
        (ApiUsername, auth.username), (ApiPwHash, auth.passwordHash))
        .onComplete(tryJson => consumeTry[JsonArray](tryJson, jsonArray => {
          val l = jsonArray.toScalaListOfString
          println(s"PIPPO: $l")
          sender ! Friends(l)
        }, "Request list of Friends failed"))
    case GetAllPlayers(auth) =>
      val sender = this.sender()
      doRest(HttpMethod.GET, FriendsCandidateRoute,
        resp => if (resp.statusCode() == StatusCodes.OK.intValue) resp.bodyAsJsonArray() else None,
        (ApiUsername, auth.username), (ApiPwHash, auth.passwordHash))
        .onComplete(tryJson => consumeTry[JsonArray](tryJson, jsonArray =>
          sender ! AllPlayers(jsonArray.toScalaListOfJson.map(json =>
            (json.getString(ApiUsername), json.getInteger(ApiScore).toInt))),
          "Request list of players failed"))
    case GetHallOfFame(auth) =>
      val sender = this.sender()
      doRest(HttpMethod.GET, HallOfFameRoute,
        resp => if (resp.statusCode() == StatusCodes.OK.intValue) resp.bodyAsJsonArray() else None,
        (ApiUsername, auth.username), (ApiPwHash, auth.passwordHash))
        .onComplete(tryJson => consumeTry[JsonArray](tryJson,
          jsonArray => sender ! HallOfFame(jsonArray.toScalaListOfJson.map(json =>
            (json.getString(ApiUsername), Integer.parseInt(json.getInteger(ApiScore).toString)))),
          "Request list of hallOfFame failed"))
    case GetRankingPos(auth) =>
      val sender = this.sender()
      doRest(HttpMethod.GET, RankingRoute,
        resp => if (resp.statusCode() == StatusCodes.OK.intValue)
          Some(resp.bodyAsJsonObject().get.getInteger(ApiScore).toString)
        else None,
        (ApiUsername, auth.username), (ApiPwHash, auth.passwordHash))
        .onComplete(tryInt => consumeTry[String](tryInt,
          integer => sender ! RankingPos(integer),
          "Request ranking position failed"))
    case GetScore(auth) =>
      val sender = this.sender()
      doRest(HttpMethod.GET, ScoreRoute,
        resp => if (resp.statusCode() == StatusCodes.OK.intValue) {
          Some(resp.bodyAsJsonObject().get.getInteger(ApiScore).toString)
        }
        else None,
        (ApiUsername, auth.username), (ApiPwHash, auth.passwordHash))
        .onComplete(tryInt => consumeTry[String](tryInt,
          integer => sender ! Score(integer),
          "Request score failed"))
    case _ => logger.warn("Unexpected social message: " + m.toString)
  }

  private def doRest[T](method: HttpMethod, route: String,
      mapper: HttpResponse[Buffer] => T, params: (String, String)*): Future[T] = {
    val req = webClient.request(method, DefaultApiPort, ClientRunner.configuration().remoteApiHostname, route)
    params.foreach(tuple => req.addQueryParam(tuple._1, tuple._2))
    req.sendFuture().map(mapper)
  }

  private def consumeTry[T](tryOpt: Try[Option[T]], consumer: T => Unit, errorMsg: String): Unit =
    tryOpt.toEither match {
      case Right(opt) => opt match {
        case Some(t) => consumer(t)
        case None => logger.warn(errorMsg)
      }
      case Left(throwable) => logger.warn(errorMsg + throwable.getLocalizedMessage, throwable)
    }

  override def preStart(): Unit = {
    super.preStart()
    presentClient(SingleActorSystemWrapper.INSTANCE.getAnonymousAuth)
  }

}

object ClientActor {

  /**
    * Implicit class that implements Pimp my library pattern to JSON array class.
    *
    * @param jsonArray the JSON array to wrap
    */
  implicit class ScalaJsonArray(private val jsonArray: JsonArray) extends AnyVal {

    /**
      * Pimp my library function that converts a JSON array to a list of strings.
      *
      * @return the JSON array as a list of strings
      */
    def toScalaListOfString: List[String] = toScalaList(o => o.toString)

    /**
      * Pimp my library function that converts a JSON array to a list of JSON objects.
      *
      * @return the JSON array as a list of JSON objects
      */
    def toScalaListOfJson: List[JsonObject] = toScalaList(o => JsonObject.mapFrom(o))

    /**
      * Pimp my library function that converts a JSON array to a list of something.
      *
      * @param mapper the function that defines how to map the content of the JSON array
      * @tparam T the type of the data contained into the JSON array
      *
      * @return the JSON array as a list of something
      */
    def toScalaList[T](mapper: Any => T): List[T] = JavaConverters.asScalaBuffer(jsonArray.getList).toList.map(mapper)
  }

}
