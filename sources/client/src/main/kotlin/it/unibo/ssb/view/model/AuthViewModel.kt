package it.unibo.ssb.view.model

import it.unibo.ssb.model.remote.Auth
import javafx.beans.property.StringProperty
import tornadofx.ItemViewModel

/**
 * TornadoFX viewmodel for [Auth] instance.
 *
 * @see AuthModel
 *
 * @param authFactory   a function that supplies Auth objects given username and password;
 *                      could be null if injected [by param][tornadofx.Component.param]
 */
class AuthViewModel(authFactory: ((String, String) -> Auth.UserAuth)? = null) : ItemViewModel<AuthModel>() {

    private val authFactory: (String, String) -> Auth.UserAuth by param(authFactory)

    /**
     * ViewModel binding to the username property.
     *
     * @see [AuthModel.usernameProperty]
     */
    val username: StringProperty = bind { item?.usernameProperty() } as StringProperty

    /**
     * ViewModel binding to the password property.
     *
     * @see [AuthModel.passwordProperty]
     */
    val password: StringProperty = bind { item?.passwordProperty() } as StringProperty

    override fun onCommit() {
        if (item == null) {
            item = AuthModel(username.get(), password.get(), authFactory)
        }
    }
}
