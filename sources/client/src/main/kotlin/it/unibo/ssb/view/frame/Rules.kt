package it.unibo.ssb.view.frame

import it.unibo.ssb.view.style.LeaderBoardStyle.Companion.backIcon
import it.unibo.ssb.view.style.RulesStyle
import it.unibo.ssb.view.style.RulesStyle.Companion.basicPadding
import it.unibo.ssb.view.style.RulesStyle.Companion.mainPane
import it.unibo.ssb.view.style.RulesStyle.Companion.rulesLabel
import it.unibo.ssb.view.style.RulesStyle.Companion.rulesPane
import javafx.geometry.Pos
import javafx.scene.image.Image
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundImage
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import tornadofx.FX.Companion.messages
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.borderpane
import tornadofx.button
import tornadofx.get
import tornadofx.hbox
import tornadofx.importStylesheet
import tornadofx.label
import tornadofx.paddingAll
import tornadofx.scrollpane
import tornadofx.vbox
import java.util.ResourceBundle

class Rules : View(messages["game_rules"]) {

    private val rulesBundle = ResourceBundle.getBundle("it/unibo/ssb/strings/rules")

    init {
        importStylesheet<RulesStyle>()
    }

    override val root = borderpane {
        addClass(mainPane)
        background = gameBackgroundImage
        top = hbox {
            alignment = Pos.CENTER
            paddingAll = basicPadding
            button(messages["back"]) {
                graphic = backIcon()
                action {
                    replaceWith(InitialFrame::class)
                }
            }

        }
        center = scrollpane {
            addClass(rulesPane)
            vbox {
                spacing = basicPadding
                label(rulesBundle.getString("game_plot")) {
                    addClass(rulesLabel)
                    isWrapText = true
                }
                label(rulesBundle.getString("game_overview")) {
                    addClass(rulesLabel)
                    isWrapText = true
                }
                label(rulesBundle.getString("game_preparation")) {
                    addClass(rulesLabel)
                    isWrapText = true
                }
                label(rulesBundle.getString("turn_overview")) {
                    addClass(rulesLabel)
                    isWrapText = true
                }
                label(rulesBundle.getString("round_end")) {
                    addClass(rulesLabel)
                    isWrapText = true
                }
                label(rulesBundle.getString("game_end")) {
                    addClass(rulesLabel)
                    isWrapText = true
                }
            }
        }
    }

    override fun onDock() {
        super.onDock()
        currentWindow?.sizeToScene()
        currentWindow?.centerOnScreen()
    }

    companion object {
        private const val backgroundPath = "it/unibo/ssb/backgrounds/"
        private const val rulesBackground = "Background4.png"
        private val gameBackgroundImage = Background(
                BackgroundImage(Image(backgroundPath + rulesBackground),
                        BackgroundRepeat.REPEAT,
                        BackgroundRepeat.REPEAT,
                        BackgroundPosition.DEFAULT,
                        BackgroundSize(BackgroundSize.AUTO,
                                BackgroundSize.AUTO,
                                false,
                                false,
                                false, true)))
    }
}
