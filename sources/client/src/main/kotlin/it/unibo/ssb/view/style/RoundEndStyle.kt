package it.unibo.ssb.view.style

import javafx.geometry.Pos
import javafx.scene.text.FontWeight
import javafx.scene.text.TextAlignment
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.em
import tornadofx.px

class RoundEndStyle : Stylesheet() {

    companion object {
        private val basicPadding = 20.0.px

        val rootPane by cssclass()
        val topPane by cssclass()
        val messageLabel by cssclass()
        val listOfWinnersLabel by cssclass()
        val okButton by cssclass()
        val rankingElement by cssclass()
        val pointsLabel by cssclass()
    }

    init {
        rootPane {
            alignment = Pos.CENTER
            padding = box(basicPadding)
        }

        topPane {
            alignment = Pos.CENTER
            spacing = basicPadding
        }

        messageLabel {
            fontSize = 1.5.em
            fontWeight = FontWeight.BOLD
        }

        listOfWinnersLabel {
            fontSize = 1.3.em
            fontWeight = FontWeight.BOLD
        }

        okButton {
            fontSize = 1.3.em
            prefWidth = basicPadding * 3
        }

        rankingElement {
            padding = box(basicPadding / 5)
            alignment = Pos.CENTER_LEFT
        }

        pointsLabel {
            alignment = Pos.CENTER_RIGHT
            textAlignment = TextAlignment.RIGHT
        }
    }
}