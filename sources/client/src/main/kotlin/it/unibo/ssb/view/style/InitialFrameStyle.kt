package it.unibo.ssb.view.style

import javafx.geometry.Pos
import javafx.scene.paint.Color.BLUE
import javafx.scene.text.FontWeight
import jiconfont.icons.FontAwesome
import jiconfont.javafx.IconFontFX
import jiconfont.javafx.IconNode
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.em
import tornadofx.mixin
import tornadofx.px

class InitialFrameStyle : Stylesheet() {
    companion object {

        val rulesVBox by cssclass()
        val mainVBox by cssclass()
        val personalInfoVBox by cssclass()
        val scoreHBox by cssclass()
        val loginButton by cssclass()
        val newGameButton by cssclass()
        val leaderBoardButton by cssclass()
        val rulesButton by cssclass()
        val listOfFriendsStyle by cssclass()
        val friendRequest by cssclass()
        val friendRequestList by cssclass()
        val allFriendsList by cssclass()
        val friendRequestPresentation by cssclass()

        const val vBoxPaddingAll = 20.0
        const val paddingDiv = 4
        private const val textSize = 1.3
        private const val rulesRadius = 6
        private val spacingAndWidth = 100.px
        private val newGameHeight = 80.0.px

        val refreshIcon = {
            IconFontFX.register(FontAwesome.getIconFont())
            val icon = IconNode(FontAwesome.REFRESH)
            icon.fill = BLUE
            icon
        }

        private val presentationStringFormat = mixin {
            fontSize = textSize.em
            fontWeight = FontWeight.BOLD
        }
        private val vBoxSpacings = mixin {
            spacing = spacingAndWidth
            padding = box(vBoxPaddingAll.px)
        }

        private val personalInfo = mixin {
            alignment = Pos.CENTER
            padding = box((vBoxPaddingAll / 2).px)
            fontSize = textSize.em
            fontWeight = FontWeight.BOLD
        }

        private val listViewHeight = mixin {
            prefHeight = spacingAndWidth * 3
        }
    }

    init {
        mainVBox {
            +vBoxSpacings
            alignment = Pos.CENTER
        }

        rulesVBox {
            +vBoxSpacings
            alignment = Pos.TOP_CENTER
        }

        personalInfoVBox {
            +personalInfo
            spacing = vBoxPaddingAll.px
        }

        scoreHBox {
            +personalInfo
            spacing = (vBoxPaddingAll / 2).px
        }

        loginButton {
            +presentationStringFormat
            padding = box(vBoxPaddingAll.px / 3, vBoxPaddingAll.px * 1.5)
            alignment = Pos.CENTER
        }

        newGameButton {
            +presentationStringFormat
            alignment = Pos.CENTER
            padding = box(vBoxPaddingAll.px * 2)
        }

        leaderBoardButton {
            +presentationStringFormat
            alignment = Pos.CENTER
            padding = box(vBoxPaddingAll.px / 3, vBoxPaddingAll.px * 3)
        }

        rulesButton {
            +presentationStringFormat
            backgroundRadius += box(rulesRadius.em)
            minWidth = newGameHeight
            maxWidth = newGameHeight
            minHeight = newGameHeight
            minHeight = newGameHeight
        }

        listOfFriendsStyle {
            maxHeight = spacingAndWidth * 2
            padding = box((vBoxPaddingAll / 2).px)
        }

        friendRequest {
            fontSize = 1.2.em
            fontWeight = FontWeight.BOLD
        }

        friendRequestList {
            +listViewHeight
            prefWidth = spacingAndWidth * 1.5
        }

        allFriendsList {
            +listViewHeight
            prefWidth = spacingAndWidth * 3.5
        }

        friendRequestPresentation {
            +presentationStringFormat
        }
    }
}
