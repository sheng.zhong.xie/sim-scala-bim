package it.unibo.ssb.view.fragments

import it.unibo.ssb.view.controller.GuiController
import it.unibo.ssb.view.style.InvitationStyle
import it.unibo.ssb.view.style.InvitationStyle.Companion.buttonAccept
import it.unibo.ssb.view.style.InvitationStyle.Companion.buttonDecline
import it.unibo.ssb.view.style.InvitationStyle.Companion.mainBox
import javafx.geometry.Pos
import tornadofx.DefaultScope
import tornadofx.Fragment
import tornadofx.action
import tornadofx.addClass
import tornadofx.borderpane
import tornadofx.button
import tornadofx.get
import tornadofx.hbox
import tornadofx.importStylesheet
import tornadofx.label
import tornadofx.vbox

class InvitationFragment : Fragment() {

    val controller: GuiController by inject(DefaultScope)
    val inviting: String by param()

    init {
        importStylesheet<InvitationStyle>()
    }

    override val root = borderpane {
        addClass(mainBox)
        top = vbox {
            label("${messages["invite"]} $inviting")
            label(messages["want_accept"])
        }
        bottom = hbox {
            alignment = Pos.CENTER
            button(messages["accept"]) {
                addClass(buttonAccept)
                action {
                    controller.acceptRequest()
                    close()
                }
            }
            button(messages["decline"]) {
                addClass(buttonDecline)
                action {
                    controller.declineRequest()
                    close()
                }
            }
        }
    }

}
