package it.unibo.ssb.main

import it.unibo.ssb.controller.SingleActorSystemWrapper.getSystem
import it.unibo.ssb.view.frame.InitialFrame
import javafx.application.Platform
import javafx.stage.Stage
import mu.KLogging
import tornadofx.App

/**
 * The class models a TornadoFX app.
 *
 * It loads JavaFX framework thread and the TornadoFX framework facilities.
 */
class TornadoApp : App(InitialFrame::class) {

    override fun start(stage: Stage) {
        if (getSystem() == null) {
            throw IllegalStateException("Underlying ActorSystem not found")
        } else {
            super.start(stage)
            stage.setOnCloseRequest {
                it.consume()
                logger.debug { it.toString() }
                close(0)
            }
        }
    }

    companion object : KLogging() {
        const val ClientActorRefParam: String = "client-path"

        @JvmStatic
        fun close(code: Int) {
            Platform.exit()
            System.exit(code)
        }
    }
}
