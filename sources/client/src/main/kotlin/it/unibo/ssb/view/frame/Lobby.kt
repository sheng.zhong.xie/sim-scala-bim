package it.unibo.ssb.view.frame

import it.unibo.ssb.view.controller.GuiController
import it.unibo.ssb.view.style.LobbyStyle
import it.unibo.ssb.view.style.LobbyStyle.Companion.abortIcon
import it.unibo.ssb.view.style.LobbyStyle.Companion.acceptBoard
import it.unibo.ssb.view.style.LobbyStyle.Companion.acceptIcon
import it.unibo.ssb.view.style.LobbyStyle.Companion.boardLabel
import it.unibo.ssb.view.style.LobbyStyle.Companion.centerBox
import it.unibo.ssb.view.style.LobbyStyle.Companion.playIcon
import it.unibo.ssb.view.style.LobbyStyle.Companion.presentationLabel
import it.unibo.ssb.view.style.LobbyStyle.Companion.requestBoard
import it.unibo.ssb.view.style.LobbyStyle.Companion.rootPane
import it.unibo.ssb.view.style.LobbyStyle.Companion.startButton
import javafx.beans.property.SimpleBooleanProperty
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import tornadofx.DefaultScope
import tornadofx.FX
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.borderpane
import tornadofx.button
import tornadofx.disableWhen
import tornadofx.enableWhen
import tornadofx.get
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.importStylesheet
import tornadofx.label
import tornadofx.listview
import tornadofx.vbox
import tornadofx.vgrow

class Lobby : View(title = FX.messages["title"]) {

    private val controller: GuiController by inject(DefaultScope)
    val iAmOwner: Boolean by param()
    val owner: String by param()

    private val friendsRequested = controller.friendsSelectedObservable
    private val friendsAccepted = controller.friendsAcceptedObservable
    private val buttonsDisabled = SimpleBooleanProperty(controller.forceLobbyButtonsProperty.get())

    init {
        importStylesheet<LobbyStyle>()
        controller.forceLobbyButtonsProperty.addListener { _, _, newValue ->
            buttonsDisabled.set(newValue)
            println("Lobby Frame: property = " + buttonsDisabled.get())
        }
    }

    override val root = borderpane {
        addClass(rootPane)
        top = vbox {
            hgrow = Priority.ALWAYS
            vgrow = Priority.ALWAYS
            addClass(presentationLabel)
            label(if (iAmOwner) messages["game_requested_by_you"] else "$owner ${messages["game_requested"]}")
            label("${messages["waiting_opponents"]}...")
        }

        center = hbox {
            hgrow = Priority.ALWAYS
            vgrow = Priority.ALWAYS
            addClass(centerBox)
            vbox {
                label("${messages["friends_requested"]}:").addClass(boardLabel)
                listview(friendsRequested) {
                    placeholder = label(messages["no_invited"])
                    addClass(requestBoard)
                    isEditable = false
                    isMouseTransparent = true
                    isFocusTraversable = false
                }
            }
            vbox {
                label("${messages["friends_accepted"]}:").addClass(boardLabel)
                listview(friendsAccepted) {
                    placeholder = label(messages["no_accepted"])
                    addClass(acceptBoard)
                    cellFormat {
                        graphic = borderpane {
                            left = acceptIcon()
                            right = label(it)
                        }
                    }
                    isEditable = false
                    isMouseTransparent = true
                    isFocusTraversable = false
                }
            }
        }

        bottom = borderpane {
            left = hbox {
                addClass(startButton)
                alignment = Pos.CENTER_LEFT
                button(messages["abort"].toUpperCase()) {
                    graphic = abortIcon()
                    enableWhen { SimpleBooleanProperty(iAmOwner).and(!buttonsDisabled) }
                    action {
                        controller.forceDestroyLobby()
                    }
                }
            }

            center = hbox {
                addClass(startButton)
                button("${messages["start_game"]}!".toUpperCase()) {
                    graphic = playIcon()
                    enableWhen { SimpleBooleanProperty(iAmOwner).and(!buttonsDisabled) }
                    action {
                        controller.forceStartGame()
                    }
                }
            }
        }
    }
}
