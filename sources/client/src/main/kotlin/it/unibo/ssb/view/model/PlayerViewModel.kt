package it.unibo.ssb.view.model

import it.unibo.ssb.model.Player
import it.unibo.ssb.model.remote.Auth
import javafx.beans.property.IntegerProperty
import javafx.beans.property.ListProperty
import javafx.beans.property.Property
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.StringProperty
import javafx.collections.FXCollections
import tornadofx.ItemViewModel

/**
 * TornadoFX viewmodel for [Player] instance shared between client views.
 *
 * @see PlayerModel
 */
class PlayerViewModel : ItemViewModel<PlayerModel>() {
    val username: StringProperty = bind { item?.usernameProperty() } as StringProperty
    val auth: Property<Auth.UserPassAuth> = bind { item?.authProperty() }
    val score: IntegerProperty = bind { item?.scoreProperty() } as IntegerProperty
    val rankingPos: IntegerProperty = bind { item?.rankingPosProperty() } as IntegerProperty
    val friends: ListProperty<Player> = bind {
        SimpleListProperty<Player>(FXCollections.observableArrayList(item?.friends ?: emptyList()))
    }
}