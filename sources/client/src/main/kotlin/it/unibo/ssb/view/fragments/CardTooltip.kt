package it.unibo.ssb.view.fragments

import it.unibo.ssb.view.style.GameStyle
import javafx.scene.control.Tooltip
import tornadofx.Fragment
import tornadofx.addClass
import tornadofx.hbox
import tornadofx.imageview
import tornadofx.label
import tornadofx.vbox
import java.util.ResourceBundle

/**
 * A Fragment to model the structure of a [[Tooltip]] to be added on a visible Card
 */
class CardTooltip : Fragment() {

    val currentPair: Pair<Int, String> by param()

    override val root = hbox {
        spacing = GameStyle.basicPadding / 2
        /*
        imageview(currentPair.second) {
            fitHeight = GameStyle.cardHeight
            fitWidth = GameStyle.cardWidth
        }
        */
        vbox {
            addClass(GameStyle.tooltipStyle)
            label(addLabelsToTooltips(currentPair.first).first) {
                addClass(GameStyle.spellTitleLabel)
                isWrapText = true
            }
            label(addLabelsToTooltips(currentPair.first).second) {
                isWrapText = true
            }
        }
    }

    companion object {
        private val spellsBundle = ResourceBundle.getBundle("it/unibo/ssb/strings/spells")

        private fun addLabelsToTooltips(number: Int): Pair<String, String> = when (number) {
            1 -> Pair(spellsBundle.getString("dragon_title"), spellsBundle.getString("dragon_effect"))
            2 -> Pair(spellsBundle.getString("ghost_title"), spellsBundle.getString("ghost_effect"))
            3 -> Pair(spellsBundle.getString("heal_title"), spellsBundle.getString("heal_effect"))
            4 -> Pair(spellsBundle.getString("owl_title"), spellsBundle.getString("owl_effect"))
            5 -> Pair(spellsBundle.getString("storm_title"), spellsBundle.getString("storm_effect"))
            6 -> Pair(spellsBundle.getString("wave_title"), spellsBundle.getString("wave_effect"))
            7 -> Pair(spellsBundle.getString("fire_title"), spellsBundle.getString("fire_effect"))
            8 -> Pair(spellsBundle.getString("potion_title"), spellsBundle.getString("potion_effect"))
            else -> Pair("Not Found", "Not Found")
        }
    }
}