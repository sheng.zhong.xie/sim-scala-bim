package it.unibo.ssb.extension

import scala.compat.java8.FutureConverters
import scala.concurrent.Await
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.Promise
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration.`FiniteDuration$`.`MODULE$` as FiniteDuration
import scala.util.Try
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit
import scala.concurrent.ExecutionContext.`Implicits$`.`MODULE$` as ExecutionContext
import scala.concurrent.Future as SFuture
import scala.concurrent.`Future$`.`MODULE$` as SFutureCompanion
import java.util.concurrent.Future as JFuture

/*
 * This file contains module-wide utility Kotlin extension functions that apply Scala's "Pimp My Library" pattern to
 * Scala library itself.
 */
private val DefaultDuration: FiniteDuration = FiniteDuration.apply(1, TimeUnit.MINUTES)

/**
 * Get the Scala execution context needed by Vertx Scala facilities to work.
 *
 * @return  the [global][scala.concurrent.ExecutionContext.Implicits.global]
 *          [execution context][scala.concurrent.ExecutionContext] as an [executor][ExecutionContextExecutor]
 * @throws [IllegalStateException] if system does not define a valid global Scala Execution Context
 */
@Throws(IllegalStateException::class)
fun getScalaExecutor(): ExecutionContextExecutor = ExecutionContext.global() as? ExecutionContextExecutor
    ?: throw IllegalStateException("System does not define a valid global Scala Execution Context")

/**
 * Invoke Kotlin function on success using the [global][scala.concurrent.ExecutionContext.Implicits.global]
 * [Scala execution context][scala.concurrent.ExecutionContext].
 *
 * @param T the type wrapped by the future
 * @param U the return type of the function to apply on the result of the future
 * @param f the function to invoke
 *
 * @see [SFuture.onSuccess]
 * @see [onCompleteK]
 */
fun <T, U> SFuture<T>.onSuccessK(f: Function1<T, U>): Unit = this.onComplete({ t: Try<T> ->
    if (t.isSuccess) f.invoke(t.get())
}, getScalaExecutor())

/**
 * Invoke Kotlin function on failure using the [global][scala.concurrent.ExecutionContext.Implicits.global]
 * [Scala execution context][scala.concurrent.ExecutionContext].
 *
 * @param T the type wrapped by the future
 * @param U the return type of the function to apply on the throwable that failed the future
 * @param f the function to invoke
 *
 * @see [SFuture.onFailure]
 * @see [onCompleteK]
 */
fun <T, U> SFuture<T>.onFailureK(f: Function1<Throwable, U>): Unit = this.onComplete({ t: Try<T> ->
    if (t.isFailure) f.invoke(t.failed().get())
}, getScalaExecutor())


/**
 * Invoke Kotlin function on complete using the [global][scala.concurrent.ExecutionContext.Implicits.global]
 * [Scala execution context][scala.concurrent.ExecutionContext].
 *
 * @param T the type wrapped by the future
 * @param U the return type of the function to apply on the result of the future
 * @param f the function to invoke
 *
 * @see [SFuture.onComplete]
 */
fun <T, U> SFuture<T>.onCompleteK(f: Function1<Try<T>, U>): Unit = this.onComplete({ t: Try<T> ->
    f.invoke(t)
}, getScalaExecutor())

/**
 * Invoke Kotlin function on complete using the [global][scala.concurrent.ExecutionContext.Implicits.global]
 * [Scala execution context][scala.concurrent.ExecutionContext].
 *
 * @param T the type wrapped by the future
 * @param U the return type of the function to apply on the result of the future
 * @param V the return type of the function to apply on the throwable that failed the future
 * @param onSuccess the function to invoke on success
 * @param onFailure the function to invoke on failure
 *
 * @see [onSuccessK]
 * @see [onFailureK]
 * @see [SFuture.onComplete]
 */
fun <T, U, V> SFuture<T>.onCompleteK(onSuccess: Function1<T, U>, onFailure: Function1<Throwable, V>) {
    this.onSuccessK(onSuccess)
    this.onFailureK(onFailure)
}

/**
 * Convert this Java Future to a Scala one.
 *
 * Pay attention that the generated [SFuture] wraps the [get method][JFuture.get], so it blocks one of the worker
 * threads of the [global][scala.concurrent.ExecutionContext.Implicits.global]
 * [Scala executor][scala.concurrent.ExecutionContext].
 *
 * @return this Java Future as a Scala one
 *
 * @see [JFuture.get]
 */
fun <T> JFuture<T>.toScala(): SFuture<T> = SFutureCompanion.apply({ this.get() }, getScalaExecutor())

/**
 * Convert this Java CompletableFuture to a Scala one.
 *
 * @return this Java Future as a Scala one
 */
fun <T> CompletableFuture<T>.toScala(): SFuture<T> = FutureConverters.toScala(this)

/**
 * Convert this Scala Future to a Java one.
 *
 * @return this Scala Future as a Java one
 */
fun <T> SFuture<T>.toJava(): JFuture<T> = FutureConverters.toJava(this).toCompletableFuture()

fun <T> SFuture<T>.getK(): T = Await.result(this, DefaultDuration)

fun <T> Promise<T>.getK(): T = this.future().getK()
