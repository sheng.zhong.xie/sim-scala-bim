package it.unibo.ssb.view.style

import javafx.geometry.Pos
import javafx.scene.paint.Color
import javafx.scene.paint.Color.DARKBLUE
import javafx.scene.paint.Color.GRAY
import javafx.scene.paint.Color.WHITE
import javafx.scene.text.FontWeight
import javafx.scene.text.TextAlignment
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.em
import tornadofx.mixin
import tornadofx.px
import java.awt.Font

class ChooseModalityStyle : Stylesheet() {
    companion object {

        const val basicPadding = 10.0
        const val circleSize = 120.0
        const val circleOpacity = 0.5
        val circleColor: Color = GRAY

        val vBoxBasic = mixin {
            spacing = basicPadding.px
            alignment = Pos.CENTER
            fontWeight = FontWeight.BOLD
            textFill = WHITE
        }
        val presentationLabel by cssclass()
        val listOfPlayers by cssclass()
        val gameKindLabel by cssclass()
        val startGameButton by cssclass()
        val rankedExplanation by cssclass()
        val rightPanel by cssclass()
        val leftPanel by cssclass()
        val howManyPlayers by cssclass()
        val howManyPlayersLabel by cssclass()
    }

    init {

        rightPanel {
            +vBoxBasic
            padding = box(basicPadding.px, basicPadding.px, basicPadding.px, (basicPadding * 5).px)
        }

        leftPanel {
            +vBoxBasic
            alignment = Pos.CENTER
            padding = box(basicPadding.px, (basicPadding * 5).px, basicPadding.px, basicPadding.px)
        }

        howManyPlayers {
            textFill = WHITE
        }

        howManyPlayersLabel {
            fontSize = 1.3.em
            textFill = WHITE
            fontWeight = FontWeight.BOLD
        }

        presentationLabel {
            fontSize = 2.5.em
            fontScale = Font.BOLD
            fontWeight = FontWeight.BOLD
            textFill = DARKBLUE
        }
        gameKindLabel {
            fontSize = 2.em
            fontScale = Font.BOLD
            textFill = WHITE
        }
        listOfPlayers {
            fontSize = 1.3.em
            fontWeight = FontWeight.BOLD
            opacity = 0.7
            maxHeight = 200.0.px
        }

        startGameButton {
            prefHeight = 60.px
            fontSize = 1.5.em
        }

        rankedExplanation {
            textAlignment = TextAlignment.CENTER
            textFill = WHITE
            maxWidth = 200.0.px
        }
    }
}