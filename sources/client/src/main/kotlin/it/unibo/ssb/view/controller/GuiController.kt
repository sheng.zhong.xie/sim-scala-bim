package it.unibo.ssb.view.controller

import akka.actor.ActorRef
import akka.http.javadsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCode
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.ssb.controller.SingleActorSystemWrapper
import it.unibo.ssb.controller.messages.AuthenticatedSocialMessage
import it.unibo.ssb.controller.messages.ClientMessage
import it.unibo.ssb.controller.messages.LobbyMessage
import it.unibo.ssb.extension.getK
import it.unibo.ssb.extension.getScalaExecutor
import it.unibo.ssb.extension.toListK
import it.unibo.ssb.extension.toSeqS
import it.unibo.ssb.model.remote.Auth
import it.unibo.ssb.model.remote.SqlAuth
import it.unibo.ssb.view.fragments.InvitationFragment
import it.unibo.ssb.view.frame.Game
import it.unibo.ssb.view.frame.InitialFrame
import it.unibo.ssb.view.frame.LeaderBoard
import it.unibo.ssb.view.frame.Lobby
import it.unibo.ssb.view.model.AuthViewModel
import it.unibo.ssb.view.model.PlayerViewModel
import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.Alert
import javafx.stage.Modality
import javafx.stage.StageStyle
import mu.KLogging
import scala.concurrent.Promise
import tornadofx.Controller
import tornadofx.DefaultScope
import tornadofx.Scope
import tornadofx.alert
import tornadofx.find
import tornadofx.get
import tornadofx.replaceWith
import scala.collection.immutable.List as ListS

class GuiController : Controller() {

    private var promiseLogin: Promise<StatusCode>? = null
    private var promiseFriends: Promise<ListS<String>>? = null
    private var promiseFriendshipRequests: Promise<ListS<String>>? = null

    private var guiActor: ActorRef? = null
    private var lobbyActor: ActorRef? = null
    private var inviteWindow: InvitationFragment? = null

    val forceLobbyButtonsProperty = SimpleBooleanProperty(true)

    val player: PlayerViewModel by inject(DefaultScope)
    val name: StringProperty = SimpleStringProperty(getPlayerName())

    init {
        player.itemProperty.addListener { _, _, _ -> name.set(getPlayerName()) }
    }

    var friendshipRequests: ObservableList<String> = FXCollections.observableArrayList()
    var listOfFriends: ObservableList<String> = FXCollections.observableArrayList()
    var allPlayers: ObservableList<Pair<String, Int>> = FXCollections.observableArrayList()
    var hallOfFame: ObservableList<Pair<String, Int>> = FXCollections.observableArrayList()
    var friendsSelectedObservable: ObservableList<String> = FXCollections.observableArrayList()
    var friendsAcceptedObservable: ObservableList<String> = FXCollections.observableArrayList()

    /* New scope specific of the Auth object, for runtime security purpose. */
    private val authScope: Scope = Scope()

    val auth: AuthViewModel by inject(authScope, mapOf(
            "authFactory" to { username: String, password: String ->
                SqlAuth(username, password, WebClient.create(Vertx.vertx()), getScalaExecutor())
            }))

    /**
     * Function that GuiActor will call in its preStart method to set its actor ref.
     */
    fun setGuiActor(actor: ActorRef) {
        guiActor = actor
        name.set(getPlayerName())
    }

    /**
     * Function that allows to avoid Null Pointer Exceptions over the player name
     *
     * @return  the player name if it exists, else an automatically generated string
     */
    private fun getPlayerName(): String =
            if (player.item?.username == "") getAnonymous().username()
            else player.item?.username ?: getAnonymous().username()

    /**
     * Function that will return an anonymous Auth object
     *
     * @return an [Auth.Anonymous]
     */
    private fun getAnonymous(): Auth.Anonymous = SingleActorSystemWrapper.getAnonymousAuth() ?: throw Exception()

    /**
     * Function that will be called from Choose Modality Frame, when the user decides to start a game.
     * This function will send a message to GuiActor to tell that the user wants to start a game,
     *
     * @param isRanked          true if the game modality is ranked, false otherwise
     * @param maxPlayers        max number of players chosen for the game
     * @param playersSelected   the list of friends selected for the game
     */
    fun askLobby(isRanked: Boolean, maxPlayers: Int, playersSelected: List<String>) {
        Platform.runLater {
            friendsSelectedObservable.addAll(playersSelected)
            forceLobbyButtonsProperty.set(playersSelected.isEmpty())
        }

        println("Ranked mode: ${isRanked}, maxp: ${maxPlayers}, pl: ${playersSelected}")
        guiActor?.tell(ClientMessage.PlayerWantsToStartGame(player.item?.auth ?: getAnonymous(), isRanked,
                maxPlayers, playersSelected.toSeqS()), ActorRef.noSender())
    }

    /**
     * Function that will be triggered when the guiActor receives a Hello message.
     * It will change the current window with the one representing the Lobby.
     *
     * @param myName            my player name
     * @param owner             the owner of the lobby
     * @param friendsAccepted   the list of friends that have currently accepted the invite
     */
    fun helloReceivedFromLobby(myName: String, owner: String, friendsAccepted: List<String>) {
        Platform.runLater {
            if (friendsAccepted.isNotEmpty()) {
                friendsAcceptedObservable = FXCollections.observableArrayList(friendsAccepted)
            }
            primaryStage.scene.root.replaceWith(find<Lobby>(mapOf(
                    Lobby::iAmOwner to (owner == myName),
                    Lobby::owner to owner)).root)
        }
    }

    /**
     * Function to be called when an invitation is received. It sets the actor ref of the
     * lobby actor and opens a dialog for accepting or refusing the invitation.
     *
     * @param inviting      the player who is inviting me
     * @param lobbyActorRef the actor ref of the lobby
     */
    fun invitationReceived(inviting: String, lobbyActorRef: ActorRef?) {
        lobbyActor = lobbyActorRef
        Platform.runLater {
            inviteWindow = find(mapOf(InvitationFragment::inviting to inviting))
            inviteWindow?.openModal(
                    stageStyle = StageStyle.UNDECORATED, modality = Modality.APPLICATION_MODAL,
                    escapeClosesWindow = false, block = false)
        }
    }

    /**
     * Function to be called from InvitationFragment when the player clicks Accept.
     */
    fun acceptRequest() {
        guiActor?.tell(ClientMessage.PlayerAcceptsRequest((player.item?.auth
            ?: getAnonymous()).username(), lobbyActor), ActorRef.noSender())
    }

    /**
     * Function to be called from InvitationFragment when the player clicks Decline.
     */
    fun declineRequest() {
        inviteWindow?.close()
    }

    /**
     * Function to be called from GuiActor when a new player accepts the request.
     *
     * @param player    the new player who has accepted the request
     */
    fun newPlayerHasJoined(player: String) {
        Platform.runLater { friendsAcceptedObservable.add(player) }
    }

    /**
     * Function to be called from InvitationFragment when the lobby owner clicks Start Game.
     */
    fun forceStartGame() {
        guiActor?.tell(LobbyMessage.ForceStart(), ActorRef.noSender())
    }

    /**
     * Function to be called from InvitationFragment when the lobby owner clicks Abort.
     */
    fun forceDestroyLobby() {
        guiActor?.tell((LobbyMessage.DestroyLobby()), ActorRef.noSender())
    }

    /**
     * Function that notifies the actual destruction of the lobby.
     */
    fun lobbyHasBeenDestroyed() {
        Platform.runLater {
            friendsSelectedObservable.clear()
            friendsAcceptedObservable.clear()
            primaryStage.scene.root.replaceWith(find<InitialFrame>(DefaultScope).root)
        }
        lobbyActor = null
    }

    /**
     * Function that disables lobby frame's buttons.
     */
    fun gameIsStarting() {
        forceLobbyButtonsProperty.set(true)
    }

    /**
     * Notify the actual starting of a game.
     */
    fun lobbyFinished() {
        Platform.runLater { primaryStage.scene.root.replaceWith(find<Game>().root) }
    }

    /**
     * Function that will refresh the list of all the players.
     */
    fun refreshAllPlayers() {
        guiActor?.tell(AuthenticatedSocialMessage.GetAllPlayers(player.item?.auth
            ?: getAnonymous()), ActorRef.noSender())
    }

    /**
     * Function that will update the observable list of all players.
     *
     * @param players   the list returned by the rest API, it contains Pairs of
     *                  player name - player score
     */
    fun updateAllPlayers(players: List<Pair<String, Int>>) {
        logger.debug { "received new List of players: " + players.toString() }
        Platform.runLater {
            allPlayers.clear()
            allPlayers.addAll(players)
        }
    }

    /**
     * Function called for requesting friendship to a certain Player, and then
     * removes it from the list of all players.
     *
     * @param friend    the friend to ask friendship to
     */
    fun requestFriendship(friend: Pair<String, Int>) {
        guiActor?.tell(AuthenticatedSocialMessage.RequestFriendship(player.item?.auth
            ?: getAnonymous(), friend.first), ActorRef.noSender())
        Platform.runLater {
            allPlayers.remove(friend)
        }
    }

    /**
     * Function to be called when I want to accept a friendship request.
     *
     * @param friend    the friend that I want to accept
     */
    fun acceptFriendshipRequest(friend: String) {
        guiActor?.tell(AuthenticatedSocialMessage.AcceptFriendRequest(player.item?.auth
            ?: getAnonymous(), friend), ActorRef.noSender())
        Platform.runLater { if (friendshipRequests.contains(friend)) friendshipRequests.remove(friend) }
    }

    /**
     * Function to be called when I want to refuse a friendship request.
     *
     * @param friend    the friend that I want to refuse
     */
    fun refuseFriendshipRequest(friend: String) {
        Platform.runLater { if (friendshipRequests.contains(friend)) friendshipRequests.remove(friend) }
    }

    /**
     * Function to be called to know the ranking position of my player.
     */
    fun getRankingPos() {
        guiActor?.tell(AuthenticatedSocialMessage.GetRankingPos(
                player.item?.auth ?: getAnonymous()), ActorRef.noSender())
    }

    /**
     * Function that returns the position of my player.
     *
     * @param pos   the position returned by the rest API
     */
    fun setRankingPos(pos: String) {
        Platform.runLater { player.rankingPos.set(Integer.parseInt(pos)) }
    }

    /**
     * Function to be called to know the score of my player.
     */
    fun getScore() {
        guiActor?.tell(AuthenticatedSocialMessage.GetScore(
                player.item?.auth ?: getAnonymous()), ActorRef.noSender())
    }

    /**
     * Function that returns the score of my player.
     *
     * @param score   the position returned by the rest API
     */
    fun setScore(score: String) {
        Platform.runLater { player.score.set(Integer.parseInt(score)) }
    }

    /**
     * Function to be called when I want to know the Hall Of Fame.
     */
    fun getLeaderBoard() {
        guiActor?.tell(AuthenticatedSocialMessage.GetHallOfFame(
                player.item?.auth ?: getAnonymous()), ActorRef.noSender())
    }

    /**
     * Function that returns the Hall Of Fame.
     *
     * @param leaderBoard   the leader board returned by the rest API
     */
    fun setLeaderBoard(leaderBoard: List<Pair<String, Int>>) {
        Platform.runLater {
            hallOfFame.clear()
            hallOfFame.addAll(leaderBoard)
            primaryStage.scene.root.replaceWith(find<LeaderBoard>(DefaultScope).root)
        }
    }

    /**
     * Function to be called when I want to know the list of friends of my player.
     *
     * @return true if the [Promise] is set in a correct way, false otherwise
     */
    fun getFriends(): Boolean =
            if (promiseFriends == null) {
                if (player.item?.auth == null || SingleActorSystemWrapper.getAnonymousAuth() == player.item?.auth) {
                    println("You are ${player.item?.auth}, you can't have friends")
                    Platform.runLater { listOfFriends.clear() }
                    true
                } else {
                    val msg = AuthenticatedSocialMessage.GetFriends(player.item?.auth ?: getAnonymous())
                    promiseFriends = msg.toPromise()
                    guiActor?.tell(msg, ActorRef.noSender())
                    val l = promiseFriends?.getK()?.toListK() ?: emptyList()
                    Platform.runLater {
                        listOfFriends.clear()
                        listOfFriends.addAll(l)
                    }
                    promiseFriends = null
                    l.isEmpty().not()
                }
            } else {
                Platform.runLater {
                    alert(Alert.AlertType.WARNING, messages["friends_pending"])
                }
                logger.warn { messages["friends_pending"] }
                false
            }

    /**
     * Function that will give me a function to solve the [Promise] of friends.
     *
     * @param function  the function that will resolve my [Promise], given by the message
     */
    fun setFriends(function: Function1<Promise<ListS<String>>, Unit>) {
        if (promiseFriends != null) {
            function(promiseFriends ?: throw IllegalStateException())

        } else {
            Platform.runLater {
                alert(Alert.AlertType.WARNING, messages["friends_pending"])
            }
            logger.warn { messages["friends_pending"] }
        }
    }

    /**
     * Ask to get all friendship requests.
     */
    fun refreshAllFriendshipRequests(): Boolean =
            if (promiseFriendshipRequests == null) {
                val msg = AuthenticatedSocialMessage.GetFriendshipRequests(player.item?.auth ?: getAnonymous())
                promiseFriendshipRequests = msg.toPromise()
                guiActor?.tell(msg, ActorRef.noSender())
                val l = promiseFriendshipRequests?.getK()?.toListK() ?: kotlin.collections.emptyList()
                Platform.runLater {
                    friendshipRequests.clear()
                    friendshipRequests.addAll(l)
                }
                promiseFriendshipRequests = null
                l.isEmpty().not()
            } else {
                Platform.runLater {
                    alert(Alert.AlertType.WARNING, messages["friends_pending"])
                }
                logger.warn { messages["friends_pending"] }
                false
            }

    /**
     * Function that will give me a function to solve the [Promise] of friendship requests.
     *
     * @param function  the function that will resolve my [Promise], given by the message
     */
    fun updateFriendshipRequests(function: Function1<Promise<ListS<String>>, Unit>) {
        if (promiseFriendshipRequests != null) {
            function(promiseFriendshipRequests ?: throw java.lang.IllegalStateException())

        } else {
            Platform.runLater {
                alert(Alert.AlertType.WARNING, messages["friends_pending"])
            }
            logger.warn { messages["friends_pending"] }
        }
    }

    fun logout() {
        player.item = null
        auth.item = null
    }

    fun login(): Boolean =
            if (promiseLogin == null) {
                val msg = ClientMessage.RequestAuthenticationMessage(auth.item, false)
                promiseLogin = msg.toPromise()
                guiActor?.tell(msg, ActorRef.noSender())
                promiseLogin?.future()?.getK() == StatusCodes.OK
            } else {
                Platform.runLater {
                    alert(Alert.AlertType.WARNING, messages["login_pending"])
                }
                logger.warn { messages["login_pending"] }
                false
            }

    fun register(): Boolean = if (promiseLogin == null) {
        val msg = ClientMessage.RequestAuthenticationMessage(auth.item, true)
        promiseLogin = msg.toPromise()
        guiActor?.tell(msg, ActorRef.noSender())
        promiseLogin?.future()?.getK() == StatusCodes.OK
    } else {
        Platform.runLater {
            alert(Alert.AlertType.WARNING, messages["login_pending"])
        }
        logger.warn { messages["login_pending"] }
        false
    }

    fun resolveAuth(message: ClientMessage.ResponseAuthenticationMessage) {
        if (promiseLogin != null) {
            message.solvePromise(promiseLogin)
            promiseLogin = null
        } else {
            Platform.runLater {
                alert(Alert.AlertType.WARNING, messages["login_pending"])
            }
            logger.warn { messages["login_pending"] }
        }
    }

    fun getGameGuiController(): GameGuiController = find(GameGuiController::class)

    companion object : KLogging()
}