package it.unibo.ssb.view.frame

import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import it.unibo.ssb.controller.GuiActor
import it.unibo.ssb.controller.SingleActorSystemWrapper.getSystem
import it.unibo.ssb.controller.SingleActorSystemWrapper.resolveActor
import it.unibo.ssb.extension.onCompleteK
import it.unibo.ssb.main.TornadoApp
import it.unibo.ssb.view.controller.GuiController
import it.unibo.ssb.view.fragments.MyInfoFragment
import it.unibo.ssb.view.fragments.SocialFragment
import it.unibo.ssb.view.style.InitialFrameStyle
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.leaderBoardButton
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.loginButton
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.mainVBox
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.newGameButton
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.rulesButton
import it.unibo.ssb.view.style.InitialFrameStyle.Companion.rulesVBox
import javafx.geometry.Pos
import javafx.scene.image.Image
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundImage
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.layout.Priority
import mu.KLogging
import tornadofx.FX.Companion.messages
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.borderpane
import tornadofx.button
import tornadofx.get
import tornadofx.hgrow
import tornadofx.importStylesheet
import tornadofx.removeWhen
import tornadofx.runAsyncWithProgress
import tornadofx.singleAssign
import tornadofx.useMaxWidth
import tornadofx.vbox
import tornadofx.vgrow
import scala.collection.immutable.`List$`.`MODULE$` as List

class InitialFrame : View(title = messages["title"]) {
    private val controller: GuiController by inject()
    private var guiActorRef: ActorRef by singleAssign()
    private var clientActorRef: ActorRef by singleAssign()

    init {
        val system: ActorSystem = getSystem() ?: throw IllegalStateException("Underlying ActorSystem not found")
        resolveActor(app.parameters.named[TornadoApp.ClientActorRefParam] ?: throw IllegalStateException())
                .onCompleteK({ ar ->
                    clientActorRef = ar
                    guiActorRef = system.actorOf(Props.create(GuiActor::class.java, controller, clientActorRef))
                }, { ex ->
                    logger.error(ex) { ex.localizedMessage }
                    TornadoApp.close(1)
                })
        importStylesheet<InitialFrameStyle>()
    }

    override val root = borderpane {
        background = gameBackgroundImage
        center = borderpane {
            center = vbox {
                useMaxWidth = true
                hgrow = Priority.ALWAYS
                vgrow = Priority.ALWAYS
                addClass(mainVBox)
                button(messages["leaderboard"]) {
                    addClass(leaderBoardButton)
                    action {
                        controller.getLeaderBoard()
                    }
                }

                button(messages["new_game"]) {
                    addClass(newGameButton)
                    action {
                        runAsyncWithProgress {
                            controller.getFriends()
                        } ui {
                            replaceWith<ChooseModality>()
                        }
                    }
                }

                button(messages["login_register"]) {
                    addClass(loginButton)
                    action { replaceWith<Authentication>() }
                    removeWhen(controller.player.empty.not())
                }

                button(messages["logout"]) {
                    addClass(loginButton)
                    action { controller.logout() }
                    removeWhen(controller.player.empty)
                }
            }

            right = vbox {
                addClass(rulesVBox)
                alignment = Pos.TOP_CENTER
                useMaxWidth = true
                button(messages["rule"]) {
                    addClass(rulesButton)
                    action {
                        replaceWith<Rules>()
                    }
                }
            }
        }

        left = find<MyInfoFragment>().root
        left.visibleProperty().bind(controller.player.empty.not())

        right = find<SocialFragment>().root
        right.visibleProperty().bind(controller.player.empty.not())
    }

    override fun onDock() {
        super.onDock()
        currentWindow?.sizeToScene()
        currentStage?.sizeToScene()
        currentWindow?.centerOnScreen()
    }

    companion object : KLogging() {
        private const val backgroundPath = "it/unibo/ssb/backgrounds/"
        private const val mainBackground = "Background3.png"
        val gameBackgroundImage = Background(
                BackgroundImage(Image(backgroundPath + mainBackground),
                        BackgroundRepeat.REPEAT,
                        BackgroundRepeat.REPEAT,
                        BackgroundPosition.DEFAULT,
                        BackgroundSize(BackgroundSize.AUTO,
                                BackgroundSize.AUTO,
                                false,
                                false,
                                false, true)))
    }
}
