package it.unibo.ssb.view.fragments

import it.unibo.ssb.view.intToCardUrlMapper
import it.unibo.ssb.view.style.GameStyle
import javafx.beans.property.IntegerProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.geometry.Pos
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import tornadofx.Fragment
import tornadofx.addClass
import tornadofx.get
import tornadofx.gridpane
import tornadofx.label
import tornadofx.row
import tornadofx.vbox
import it.unibo.ssb.model.`SharedGameBoard$`.`MODULE$` as SharedGameBoard

class SecretCardFragment : Fragment() {

    val secretCardSize: IntegerProperty by param(SimpleIntegerProperty(0))

    override val root = vbox {
        alignment = Pos.CENTER
        label("${messages["secret_spells"]}:").addClass(GameStyle.gameFrameLabel)
        gridpane {
            row {
                add(secretCardImageView(secretCardSize, numOfCards))
                add(secretCardImageView(secretCardSize, numOfCards - 1))
            }
            row {
                add(secretCardImageView(secretCardSize, numOfCards - 2))
                add(secretCardImageView(secretCardSize, numOfCards - 3))
            }
        }
    }

    companion object {
        private val numOfCards: Int = SharedGameBoard.SecretCardMaxNumber()

        /**
         * Function that returns an [ImageView] representing the Secret Card.
         *
         * @param numberFromController  the current size of the secret cards deck
         * @param numberToHide          the number that tells me weather I have to make
         *                              this card visible or not, depending on the position
         *                              inside the view
         * @return the [ImageView] with all its properties set
         */
        private fun secretCardImageView(numberFromController: IntegerProperty, numberToHide: Int): ImageView {
            if (numberFromController.value > numOfCards || numberFromController.value < 0) {
                throw IllegalArgumentException()
            }
            val i = ImageView(Image(intToCardUrlMapper(0)))
            i.fitHeight = GameStyle.cardHeight
            i.fitWidth = GameStyle.cardWidth
            i.visibleProperty().bind(numberFromController.greaterThanOrEqualTo(numberToHide))
            return i
        }
    }
}