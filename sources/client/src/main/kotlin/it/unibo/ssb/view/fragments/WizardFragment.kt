package it.unibo.ssb.view.fragments

import com.github.thomasnield.rxkotlinfx.toBinding
import com.github.thomasnield.rxkotlinfx.toObservable
import it.unibo.ssb.extension.isUseful
import it.unibo.ssb.view.intToCardImageMapper
import it.unibo.ssb.view.style.GameStyle
import it.unibo.ssb.view.style.GameStyle.Companion.gameFrameLabel
import it.unibo.ssb.view.style.GameStyle.Companion.newFlowPaneCards
import it.unibo.ssb.view.style.GameStyle.Companion.newFlowPaneWizard
import it.unibo.ssb.view.style.GameStyle.Companion.playerSize
import it.unibo.ssb.view.style.GameStyle.Companion.wizardInTurnFace
import javafx.beans.binding.Binding
import javafx.beans.property.IntegerProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Orientation
import javafx.scene.paint.Color
import jiconfont.icons.FontAwesome
import jiconfont.javafx.IconFontFX
import jiconfont.javafx.IconNode
import tornadofx.Fragment
import tornadofx.addClass
import tornadofx.bindChildren
import tornadofx.borderpane
import tornadofx.circle
import tornadofx.flowpane
import tornadofx.get
import tornadofx.hbox
import tornadofx.imageview
import tornadofx.label
import tornadofx.removeClass
import tornadofx.stackpane
import tornadofx.toBinding
import tornadofx.tooltip
import tornadofx.vbox

class WizardFragment : Fragment() {

    val facePath: String by param("")
    val name: StringProperty by param(SimpleStringProperty(AnonymousWizard))
    val life: IntegerProperty by param(SimpleIntegerProperty(0))
    val cards: ObservableList<Int> by param(FXCollections.observableArrayList((1..5).toList()))
    val score: IntegerProperty by param(SimpleIntegerProperty(0))
    val cardsOrientation: Orientation by param(Orientation.HORIZONTAL)
    val inTurn: Binding<Boolean> by param(SimpleBooleanProperty(true).toBinding())

    private val lifePointsStackPane = stackpane {
        circle {
            centerX = GameStyle.circleSize
            centerY = GameStyle.circleSize
            radius = GameStyle.circleRadius
            fill = Color.WHITE
        }
        label(life.toObservable().map { "${messages["life"]}: $it" }.toBinding()) {
            addClass(GameStyle.lifePointsLabel)
        }
    }

    private val wizardImageView = imageview(facePath) {
        fitHeight = GameStyle.playerSize
        fitWidth = GameStyle.playerSize

        tooltip {
            graphic = vbox {
                addClass(GameStyle.tooltipStyle)
                addClass(GameStyle.gameFrameLabel)

                label(name)
                hbox {
                    label("${messages["score"]}: ")
                    label(score.asString())
                }
            }
        }
    }

    init {
        IconFontFX.register(FontAwesome.getIconFont())
    }

    override val root = borderpane {
        top = flowpane {
            addClass(newFlowPaneWizard)
            add(if (facePath.isUseful()) wizardImageView else IconNode(FontAwesome.USER_SECRET))
            add(label(name) {
                addClass(gameFrameLabel)
                prefWidth = playerSize * 2
                isWrapText = true
            })
            add(lifePointsStackPane)
            orientation = cardsOrientation
            prefWrapLength = playerSize * 5

        }
        center = flowpane {
            addClass(newFlowPaneCards)
            bindChildren(cards) { intToCardImageMapper(it) }
            orientation = cardsOrientation
            prefWrapLength = playerSize * 5
        }
    }

    init {
        if (inTurn.value) root.top.addClass(wizardInTurnFace)
        inTurn.addListener { _, _, b ->
            if (b) root.top.addClass(wizardInTurnFace)
            else root.top.removeClass(wizardInTurnFace)
        }
    }

    companion object {
        const val AnonymousWizard: String = "Anonymous Wizard"
    }
}
