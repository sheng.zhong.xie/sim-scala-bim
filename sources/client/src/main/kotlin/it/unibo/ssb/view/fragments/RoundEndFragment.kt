package it.unibo.ssb.view.fragments

import it.unibo.ssb.view.controller.GameGuiController
import it.unibo.ssb.view.style.RoundEndStyle
import it.unibo.ssb.view.style.RoundEndStyle.Companion.listOfWinnersLabel
import it.unibo.ssb.view.style.RoundEndStyle.Companion.messageLabel
import it.unibo.ssb.view.style.RoundEndStyle.Companion.okButton
import it.unibo.ssb.view.style.RoundEndStyle.Companion.pointsLabel
import it.unibo.ssb.view.style.RoundEndStyle.Companion.rankingElement
import it.unibo.ssb.view.style.RoundEndStyle.Companion.rootPane
import it.unibo.ssb.view.style.RoundEndStyle.Companion.topPane
import javafx.scene.text.TextAlignment
import tornadofx.Fragment
import tornadofx.action
import tornadofx.addClass
import tornadofx.borderpane
import tornadofx.button
import tornadofx.get
import tornadofx.hbox
import tornadofx.importStylesheet
import tornadofx.label
import tornadofx.listview
import tornadofx.observable
import tornadofx.vbox

class RoundEndFragment : Fragment() {

    val gameOver: Boolean by param()
    val wizards: List<String> by param()
    val round: Int by param()
    val ranking: List<Pair<String, Int>> by param()

    private val controller: GameGuiController by inject()

    init {
        importStylesheet<RoundEndStyle>()
    }

    override val root = borderpane {
        addClass(rootPane)
        top = vbox {
            addClass(topPane)

            label(if (gameOver) messages["game_over"] else "${messages["round"]} $round ${messages["over"]}"
                    .toUpperCase())
                    .addClass(messageLabel)

            label(if (wizards.size > 1) "${messages["winners"]}: "
            else "${messages["winner"]}: " + wizards.joinToString())
                    .addClass(listOfWinnersLabel)

            label(if (gameOver) messages["final_ranking"] else messages["current_ranking"]
                    .toUpperCase())
                    .addClass(listOfWinnersLabel)

            listview(ranking.observable()) {
                prefHeight = 100.0
                cellFormat {
                    graphic = borderpane {
                        left = hbox {
                            addClass(rankingElement)
                            label("${ranking.indexOf(it) + 1}°")
                        }
                        center = hbox {
                            addClass(rankingElement)
                            label(it.first)
                        }

                        right = hbox {
                            addClass(rankingElement)
                            addClass(pointsLabel)
                            label("${it.second} ${messages["points"]}") {
                                textAlignment = TextAlignment.RIGHT
                            }
                        }
                    }
                }
            }

            button(messages["ok"].toUpperCase()) {
                addClass(okButton)
                action {
                    close()
                    if (gameOver) controller.wizardQuitsGame()
                    else controller.startNewRound(round)
                }
            }
        }
    }
}