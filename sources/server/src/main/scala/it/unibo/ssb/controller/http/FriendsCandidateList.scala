package it.unibo.ssb.controller.http

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.lang.scala.json.Json
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.model.remote.{ApiPwHash, ApiScore, ApiUsername, FriendsCandidateRoute}

/**
  *
  * this class handles the API call to get the list of player. This list is composed of  all players without:
  *
  * -the player in the param
  * -the friends of the player
  * -other players that ask for friendship to the player
  * -other players that recived friend request from the player
  *
  * Only accept the GET  method with the following parameters:
  *
  * -username [[String]] of the player who wants to accept the friendship
  * -password [[String]] of the player who wants to accept the friend request encrypted with the SHA256 algorithm
  *
  * Return:
  * -401 if the username and password do not match any player in the system
  * -400 if one of the two parameters is missing
  * -200 if the request is valid, return also a [[io.vertx.core.json.JsonArray]] contained the list of players
  */
case class FriendsCandidateList() extends HttpApi(HttpMethod.GET, FriendsCandidateRoute) {

  override def handle(rc: RoutingContext): Unit = {

    val u = rc.queryParams().get(ApiUsername)
    val p = rc.queryParams().get(ApiPwHash)
    val jsonArray = Json.emptyArr()
    rc
      .response()
      .setStatusCode(((u, p) match {
        case (Some(user), Some(pw)) if user.nonEmpty && pw.nonEmpty =>
          if (cache.isCredentialOK(user, pw)) {
            val players: List[(String, Int)] = cache.listOfPossibleFriends(u.get)
            players.foreach(f => jsonArray.add(Json.obj((ApiUsername, f._1), (ApiScore, f._2))))
            StatusCodes.OK
          } else StatusCodes.Unauthorized
        case _ => StatusCodes.BadRequest
      }).intValue)
      .end(jsonArray.encodePrettily())
  }
}
