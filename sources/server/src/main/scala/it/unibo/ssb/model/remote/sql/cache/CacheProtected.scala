package it.unibo.ssb.model.remote.sql.cache

import java.text.Normalizer
import java.text.Normalizer.Form

@deprecated("New JDBC driver already filters the queries", "2018/12/02")
trait CacheProtected extends CacheSqlReader
@deprecated("New JDBC driver already filters the queries", "2018/12/02")
object CacheProtected {

  var cacheProtected: Option[CacheProtected] = None

  def apply(): CacheProtected = {
    if (cacheProtected.isEmpty) {
      cacheProtected = Some(new CacheProtectedImpl())
    }
    cacheProtected.get
  }

  private def checkQuery(query: String): String = {
    val normalized = Normalizer.normalize(query, Form.NFD)
    normalized.replaceAll("[^A-Za-z0-9]", "")
  }

  class CacheProtectedImpl() extends CacheProtected {

    val cache: CacheSqlReader = CacheSqlReader()

    override def playerExist(namePlayer: String): Boolean =
      cache.playerExist(checkQuery(namePlayer))

    override def playerNotExist(namePlayer: String): Boolean =
      cache.playerNotExist(checkQuery(namePlayer))

    override def addPlayer(namePlayer: String, pwHash: String): Unit =
      cache.addPlayer(checkQuery(namePlayer), checkQuery(pwHash))

    override def removePlayer(namePlayer: String): Unit =
      cache.removePlayer(checkQuery(namePlayer))

    override def updateScore(namePlayer: String, score: Int): Unit =
      cache.updateScore(checkQuery(namePlayer), score)

    override def getScore(namePlayer: String): Int =
      cache.getScore(checkQuery(namePlayer))

    override def isCredentialOK(namePlayer: String, pwHash: String): Boolean =
      cache.isCredentialOK(checkQuery(namePlayer), checkQuery(pwHash))

    override def friendshipsNotAccepted(namePlayer: String): List[String] =
      cache.friendshipsNotAccepted(checkQuery(namePlayer))

    override def friendRequests(namePlayer: String): List[String] =
      cache.friendRequests(checkQuery(namePlayer))

    override def acceptFriendShip(namePlayer: String, friend: String): Unit =
      cache.acceptFriendShip(checkQuery(namePlayer), checkQuery(friend))

    override def friends(namePlayer: String): List[String] =
      cache.friends(checkQuery(namePlayer))

    override def askForFriendship(namePlayer: String, nameFriend: String): Unit =
      cache.askForFriendship(checkQuery(namePlayer), checkQuery(nameFriend))

    override def getRankingPosition(namePlayer: String): Int =
      cache.getRankingPosition(checkQuery(namePlayer))

    /**
      *
      * @return the list of all players in the db
      */
    override def playerList(): List[String] = cache.playerList()

    override def setLastLogin(namePlayer: String): Unit = cache.setLastLogin(checkQuery(namePlayer))

    override def hallOFFame: List[(String, Int)] = cache.hallOFFame

    override def listOfPossibleFriends(namePlayer: String): List[(String, Int)] =
      cache.listOfPossibleFriends(checkQuery(namePlayer))

  }

}
