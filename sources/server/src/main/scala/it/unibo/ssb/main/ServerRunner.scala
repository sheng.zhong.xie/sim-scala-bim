package it.unibo.ssb.main

import akka.actor.{ActorRef, ActorSystem}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.Vertx
import it.unibo.ssb.controller.RestVerticle
import it.unibo.ssb.controller.discovery.DiscoveryActor
import it.unibo.ssb.controller.lobby.MasterLobbyActor
import it.unibo.ssb.controller.matchMaking.MatchMakingActor
import it.unibo.ssb.main.ServerConfig.{AkkaBindHostname, AkkaBindPort, AkkaLogicalHostname, AkkaLogicalPort}
import it.unibo.ssb.model.remote
import it.unibo.ssb.model.remote.sql.executor.SqlManager
import it.unibo.ssb.model.remote.{ActorOfDiscoveryIndex, ActorOfMasterLobbyIndex, ActorOfMatchMakingIndex}

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext

object ServerRunner extends App with LazyLogging {

  private val parser = ServerParser("ssb-server")
  private val vertx = Vertx.vertx()
  private implicit val ec: ExecutionContext = VertxExecutionContext.apply(vertx.getOrCreateContext())
  private val actors: ListBuffer[Option[ActorRef]] = ListBuffer[Option[ActorRef]](None, None, None)
  private var conf: ServerConfig = _

  initializeSql()
  parser.parse(args, ServerConfig()) match {

    case Some(config) =>
      conf = config
      val akkaConf = ConfigFactory
        .parseString(s"$AkkaLogicalHostname=${config.hostname}")
        .withFallback(ConfigFactory.parseString(s"$AkkaLogicalPort=${config.akkaPort}"))
        .withFallback(ConfigFactory.parseString(s"$AkkaBindHostname=${config.boundHostname}"))
        .withFallback(ConfigFactory.parseString(s"$AkkaBindPort=${config.boundPort}"))
        .withFallback(ServerConfig.akkaConfig)
      val system: ActorSystem = ActorSystem.create(config.systemName, akkaConf)
      deployActors(system)
      vertx.deployVerticleFuture(new RestVerticle(config.boundHostname, config.port)).onComplete(completed =>
        completed.toEither match {
          case Right(_) =>
            logger.info(s"REST API server loaded on host ${config.hostname} port ${config.port}")
          case Left(throwable) =>
            logger.error("Can't load REST API server: " + throwable.getLocalizedMessage, throwable)
            System.exit(1)
        })

    case None => System.exit(1)
  }

  final def discovery: Option[ActorRef] = actors(ActorOfDiscoveryIndex)

  final def masterLobby: Option[ActorRef] = actors(ActorOfMasterLobbyIndex)

  final def matchMaking: Option[ActorRef] = actors(ActorOfMatchMakingIndex)

  private def deployActors(system: ActorSystem): Unit = {
    actors.update(remote.ActorOfDiscoveryIndex, Some(system.actorOf(DiscoveryActor.props())))
    actors.update(remote.ActorOfMasterLobbyIndex, Some(system.actorOf(MasterLobbyActor.props())))
    actors.update(remote.ActorOfMatchMakingIndex, Some(system.actorOf(MatchMakingActor.props())))
  }

  private def initializeSql(): Unit = {
    val url: String = "jdbc:mysql://localhost:3306/progetto_scala"
    val username: String = "ProgettoScalaUser"
    val password: String = "!QAZxsw2"
    SqlManager.initialize(url, username, password)
  }

  final def configuration(): ServerConfig = conf
}
