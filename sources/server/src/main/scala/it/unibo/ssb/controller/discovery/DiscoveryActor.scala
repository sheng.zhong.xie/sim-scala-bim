package it.unibo.ssb.controller.discovery

import akka.actor.{Actor, ActorRef, Props}
import it.unibo.ssb.controller.messages.DiscoveryMessage._
import it.unibo.ssb.main.ServerRunner

import scala.collection.mutable.ListBuffer

class DiscoveryActor extends Actor {
  val playerActorRef: ListBuffer[(String, ActorRef)] = ListBuffer()

  override def receive: Receive = {
    case ClientPresentationDiscovery(auth, client) =>
      saveActorRef(auth, client)
      sendACKTo(client)
    case WhoIsMasterLobby() =>
      tellActorOfMasterLobby(sender())


    case WhoIsThisActor(actor) =>
      whoIs(actor, sender())

    case WhoIsThisPlayer(player) => whoIs(player, sender())

    case InviteThisClient(lobby, owner, client) =>
      sendInviteToClient(lobby, owner, client)
  }

  /**
    * saves the playerRef of the player in a list.
    * if there was already an actorRef for that player, it is deleted from the list and returned
    *
    * @param player   to add in list
    * @param actorRef of the player
    *
    * @return an [[scala.Option]] containing the old [[akka.actor.ActorRef]], if present
    */
  private def saveActorRef(player: String, actorRef: ActorRef): Option[ActorRef] = {
    var result: Option[ActorRef] = None
    playerActorRef.foreach(pair => if (pair._1 == player) {
      result = Some(pair._2)
      playerActorRef -= pair
    })
    playerActorRef += Tuple2(player, actorRef)
    result
  }

  private def sendACKTo(actorRef: ActorRef): Unit = {
    actorRef ! ACK()
  }

  /**
    * This method send to clientToInvite an invitation message for a game.
    * It checks if client to invite and owner are in the system
    *
    * @param lobby          The lobby to join
    * @param owner          The owner of the lobby
    * @param clientToInvite the client to invite
    */
  private def sendInviteToClient(lobby: ActorRef, owner: (ActorRef, String), clientToInvite: String): Unit = {
    if (checkClientIsLogged(clientToInvite)) {
      if (checkClientIsLogged(owner._2)) {
        val client = playerActorRef.filter(_._1 == clientToInvite).head._2
        val ownerName = playerActorRef.filter(_._2 == owner._1).head._1
        client ! GameInvitation(lobby, ownerName)
        // sendACKTo(owner._1) PERCHE'??????????????
      } else {
        owner._1 ! Error("YOU ARE NOT IN THE SYSTEM! YOU MUST LOG BEFORE")
      }
    } else {
      owner._1 ! Error("Your friend is currently not logged in the system")
    }
  }

  /**
    * Method to discover who is a known players or actorRef
    *
    * @param unknown - Can be a player name or an ActorRef
    * @param sender  - The sender of the message
    */
  private def whoIs(unknown: Object, sender: ActorRef): Unit = unknown match {
    case x: ActorRef =>
      if (checkClientIsLogged(x)) {
        sender ! ActorIs(playerActorRef.filter(_._2 == x).head._1)
      } else {
        sender ! Error("Clients not logged")
      }
    case x: String =>
      if (checkClientIsLogged(x)) {
        sender ! PlayerIs(playerActorRef.filter(_._1 == x).head._2)
      } else {
        sender ! Error("Player not logged")
      }
    case _ => sender ! Error("Object unknown")
  }

  private def checkClientIsLogged(player: String): Boolean = {
    playerActorRef.map(_._1).contains(player)
  }

  private def checkClientIsLogged(client: ActorRef): Boolean = {
    playerActorRef.map(_._2).contains(client)
  }

  private def tellActorOfMasterLobby(sender: ActorRef): Unit = {
    if (playerActorRef.map(_._2).contains(sender)) {
      if (ServerRunner.masterLobby.nonEmpty) {
        sender ! MasterLobbyIs(ServerRunner.masterLobby.get)
      }
      else {
        sender ! Error("Source not available")
      }
    } else {
      sender ! Error("You are not in the system. You must log first")
    }
  }
}

object DiscoveryActor {
  def props(): Props = Props(new DiscoveryActor)
}

