package it.unibo.ssb.controller

import java.util.{Timer, TimerTask}

import akka.actor.{ActorRef, Props, Stash}
import it.unibo.ssb.controller.game.{GameController, GameControllerInitializer}
import it.unibo.ssb.controller.messages.DiscoveryMessage.Error
import it.unibo.ssb.controller.messages.GameMessage._
import it.unibo.ssb.model.{Card, GameBoard, LeaderBoard, Wizard}

import scala.collection.mutable.ListBuffer

/**
  * actor that has the job to interact with the GameGUIActor via message.
  *
  * @param ranked  the attribute of the controller that represents weather the game is ranked or not.
  * @param wizards the name of the wizards.
  */
//noinspection ScalaStyle
class GameControllerActor(ranked: Boolean, wizards: List[String])
  extends ControlActor with Stash with GameControllerInitializer {

  private val gameController = Some(GameController(wizards, ranked))
  private val clientActors = ListBuffer.empty[(ActorRef, String)]
  private var asyncTimer = new Timer()
  private var roundMsgCounter = 0
  private val leaderBoard: Option[LeaderBoard] = if (ranked) Some(LeaderBoard()) else None


  override def receive: Receive = {
    case InitializeGameMessage =>
      if (gameController.get.wizardsPlayer.nonEmpty) {
        initializeGame()
      }

    case WizardsCastSpellMessage(wizard, spellToCast, _) =>
      wizardsCastSpell(wizard, spellToCast)

    case NextWizardMomentMessage() =>
      nextWizardMoment()

    case GetGameBoardMessage(_) =>
      getGameBoard

    case WizardTurnMessage(_) =>
      wizardTurn

    case TimerTimeOut() =>
      handleDisconnection(gameController.get.wizardTurn)

    case ClientPresentation(ref, name) =>
      clientActors += ((ref, name))
      if (clientActors.size == wizards.size) {
        initializeGame()
      }

    case StartRound(_) => roundHandling()
  }

  override def initializeGame(): Unit = {
    gameController.get.initializeGame()
    startGame()
  }

  override def startGame(): Unit = {
    gameController.get.startGame()
    clientActors.toStream.foreach(it => it._1 ! StartGameMessage())
    wizardTurn
  }

  override def wizardTurn: Wizard = {
    val wizardInTurn = gameController.get.wizardTurn
    clientActors.toStream.foreach(it => it._1 ! WizardTurnMessage(Option(wizardInTurn)))
    startTimer()
    wizardInTurn
  }

  override def wizardsCastSpell(wizard: Wizard, spellToCast: Int): Boolean = {
    // stop timer
    stopTimer()
    var canCast = false

    if(gameController.get.getGameBoard.livingWizard.count(_.name == wizard.name) == 0){
      clientActors.foreach(_._1 ! Error("Not valid wizard") )
    }else {
      // I take the coorect wizard from game board
      val wizardTmp = gameController.get.getGameBoard.livingWizard.filter(_.name == wizard.name).head


      // i have to discriminate between normal spell and Owl (4)
      if (spellToCast == 4) {
        canCast = castOwl(wizardTmp)
      } else {
        // try that spell on the model
        canCast = gameController.get.wizardsCastSpell(wizardTmp, spellToCast)
      }
    }

    // communicate to all clients what append
    clientActors.foreach(it => it._1 ! WizardsCastSpellMessage(wizard, spellToCast, Option(canCast)))

    //check if some has won
    val isEnd = handleWinners(gameController.get.getWinners)
    if (canCast && !isEnd) {
      wizardTurn
      canCast
    } else {
      if (!isEnd) {
        startTimer()
        nextWizardMoment()
      }
      canCast
    }
  }

  override def nextWizardMoment(): Unit = {
    stopTimer()
    gameController.get.nextWizardMoment()
    clientActors.foreach(p => p._1 ! NextWizardMomentMessage())
    wizardTurn
  }


  override def getGameBoard: GameBoard = {
    val gameboard: GameBoard = gameController.get.getGameBoard
    clientActors.toStream.foreach(it => it._1 ! GetGameBoardMessage(Option(gameboard)))
    gameboard
  }

  override def setPlayer(numberOfHumanPlayers: List[String]): Unit = {
    gameController.get.setPlayer(numberOfHumanPlayers)
    clientActors.toStream.foreach(it => it._1 ! SetPlayerMessage(numberOfHumanPlayers))
  }

  override def wizardsPlayer: Seq[Wizard] = {
    val players = gameController.get.wizardsPlayer
    clientActors.toStream.foreach(it => it._1 ! WizardsPlayerMessage(players))
    players
  }

  /**
    * Method that takes care about casting spell Owl
    * This spell different from other spells must show to the caster the drawn card
    * This method cast the spell, check if can cast, latter case send to actor clients
    * of caster a special message SeretCard(card) where card is the card drawn.
    *
    * @param wizard caster
    *
    * @return true if he can cast the spell otherwise false
    */
  private def castOwl(wizard: Wizard): Boolean = {
    var isCasted = false
    val cards = gameController.get.getGameBoard.wizardGameBoard(wizard).cardsList.toList
    val canCast = gameController.get.wizardsCastSpell(wizard, GameControllerActor.Four)
    if (canCast) {
      // I must find the card add by spell
      val card: Card = gameController.get.getGameBoard.wizardGameBoard(wizard).cardsList.:+(Card(GameControllerActor.Four))
        .diff(cards).head
      clientActors.filter(_._2 == wizard.name).foreach(_._1 ! SecretCard(card.id))
      isCasted = true
    }
    isCasted
  }

  /**
    * This is the code to execute if timer is reached
    * It sends to GameControllerActor a message TimerTimeOut()
    * to aware it that timer is reached
    *
    * @return
    */
  private def task = new TimerTask {
    override def run(): Unit = self ! TimerTimeOut()
  }

  /**
    * start the timer used to establish if player has left
    */
  private def startTimer(): Unit = {
    asyncTimer.schedule(task, (MaximumTimeStepToResponse + TimeTolerance) * 1000)
  }

  /**
    * Stop the timer
    */
  private def stopTimer(): Unit = {
    asyncTimer.cancel()
    asyncTimer = new Timer()
  }

  /**
    * Method called to handle when a wizard disconnect
    * <ul>
    * <li> Stop timer
    * <li> Send to each clients that one is disconnected
    * <li> Check if the living wizards are 1 he wins
    *
    * @param wizardDisconnected the wizard that disconnect
    */
  private def handleDisconnection(wizardDisconnected: Wizard): Unit = {
    // i communicate to each clients that he has quit
    clientActors.foreach(_._1 ! PlayerDisconnection(wizardDisconnected))
    // If there is just one player left he wins the turn
    if (gameController.get.getGameBoard.livingWizard.size == 2) {
      // i remove wizard disconnected
      gameController.get.wizardDisconnected(wizardDisconnected)
      handleWinners(Option(gameController.get.getGameBoard.livingWizard.toList))
    } else {
      clientActors.remove(clientActors.map(_._2).indexOf(wizardDisconnected.name))
      // i tell to next wizard it's his turn
      nextWizardMoment()
      // i remove wizard disconnected
      gameController.get.wizardDisconnected(wizardDisconnected)
    }
  }

  /**
    * Method to handle the round winning
    * @param winners winners
    * @return true if some one has win
    */
  private def handleWinners(winners: Option[List[Wizard]]): Boolean = {
    if (winners.nonEmpty) {
      clientActors.foreach(_._1 ! WizardHasWonTheRound(winners.get, gameController.get.currentRound,
        gameController.get.getRanking))
    }
    winners.nonEmpty
  }

  /**
    * Method used to count the message  NextRound arrived to the actor.
    * When they are equals to the number of client next round starts
    */
  private def roundHandling(): Unit = {

    roundMsgCounter += 1
    if (roundMsgCounter == clientActors.size) {
      roundMsgCounter = 0
      nextRound()
    }
  }

  /**
    * Method used to restart a new round
    */
  private def nextRound(): Unit = {
    gameController.get.nextRound()
    if (gameController.get.gameWinners.nonEmpty) {
      gameEndHandling(gameController.get.gameWinners)
    } else {
      wizardTurn
    }
  }

  /**
    * Method used to handle the end of the game and the winner
    * @param ranking the list of ranking
    */
  private def gameEndHandling(ranking: List[(Wizard, Boolean, Int)]): Unit = {
    clientActors.foreach(_._1 ! WizardHasWonTheGame(ranking.filter(_._2).map(_._1), ranking.map(w => (w._1, w._3))))
    if (ranked) {
      leaderBoard.get.update(ranking.filter(_._2).map(_._1.name), ranking.map(_._1.name))
    }
  }
}

object GameControllerActor {

  final val Four = 4

  def props(ranked: Boolean, clientActors: List[String]): Props = {
    Props(new GameControllerActor(ranked, clientActors))
  }
}