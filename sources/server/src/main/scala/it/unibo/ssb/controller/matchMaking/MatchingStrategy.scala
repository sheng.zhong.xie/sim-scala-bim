package it.unibo.ssb.controller.matchMaking

import akka.actor.ActorRef

sealed trait MatchingStrategy extends ((List[GamersContainer], List[ActorRef]) => Option[GamersContainer]) {
  /**
    *
    * this function analyzes all the containers that are passed and returns the most compatible one.
    * Compatibility is calculated taking into account:
    * 1- how often the container has been discarded
    * 2- if the addition of the players would fill the container
    *
    * @param queue  of candidate containers
    * @param gamers who must be placed in a container
    *
    * @return the most appropriate container in an [[Option]], [[None]] will be returned if there are no compatible container
    */
  def apply(queue: List[GamersContainer], gamers: List[ActorRef]): Option[GamersContainer]
}

object MatchingStrategy {
  private final val scoreForGame: Int = 3

  def apply(queue: List[GamersContainer], gamers: List[ActorRef]): Option[GamersContainer] = {
    val compatibleContainer = queue.filter(container => container.freeSpace >= gamers.size)
    if (compatibleContainer.isEmpty) None
    else {
      var bestContainer: GamersContainer = compatibleContainer.head
      compatibleContainer.foreach(container =>
        if (containerScore(container, gamers.size) > containerScore(bestContainer, gamers.size)) bestContainer = container)
      Some(bestContainer)
    }
  }

  private def containerScore(gamersContainer: GamersContainer, partySize: Int): Int = {
    if (gamersContainer.freeSpace == partySize) gamersContainer.getIgnoredTimes + scoreForGame
    else gamersContainer.getIgnoredTimes
  }
}
