package it.unibo.ssb.model.remote.sql.cache

import java.util.concurrent.locks.{ReadWriteLock, ReentrantReadWriteLock}

import it.unibo.ssb.model.remote.sql.executor.{FriendSql, PlayerSql, SqlManager}

import scala.collection.mutable.ListBuffer

/**
  *
  * this class keeps in memory the data of the last players required,
  * this allows to reduce the calls to the database and increase the performances
  */
trait CacheSqlReader extends PlayerSql with FriendSql {

  /**
    *
    * @return the list of all players in the db
    */
  def playerList(): List[String]
}

object CacheSqlReader {
  final val DefaultSize = 200
  final val userNameSqlField = "userName"
  final val pwHashSqlField = "passwordHash"
  final val scoreSqlField = "score"
  final val queryLoadUser = "SELECT * FROM progetto_scala.users where lastLogin is not null order by lastLogin ;"
  final val selectFromTableQuery = "SELECT * FROM progetto_scala.users "

  var cacheSqlReader: Option[CacheSqlReader] = None

  /**
    *
    *
    * @return
    */
  def apply(): CacheSqlReader = {
    if (cacheSqlReader.isEmpty) cacheSqlReader = Some(new CacheSqlReaderImpl(DefaultSize))
    cacheSqlReader.get
  }


  class CacheSqlReaderImpl(sizePlayerCache: Int = 0) extends CacheSqlReader {
    private val players: ListBuffer[PlayerCache] = ListBuffer()
    private val lock: ReadWriteLock = new ReentrantReadWriteLock()
    setUserCache()

    override def friendshipsNotAccepted(namePlayer: String): List[String] = FriendSql.friendshipsNotAccepted(namePlayer)

    override def friendRequests(namePlayer: String): List[String] = {
      val player = insertAndGetCachePlayer(namePlayer)
      player.lock.readLock().lock()
      val friends = player.friendRequest.toList
      player.lock.readLock().unlock()
      friends
    }

    override def acceptFriendShip(namePlayer: String, friendName: String): Unit = {
      val player = insertAndGetCachePlayer(namePlayer)
      player.lock.writeLock().lock()
      player.friends += friendName
      player.friendRequest -= friendName
      player.lock.writeLock().unlock()

      val friend = insertAndGetCachePlayer(friendName)
      friend.lock.writeLock().lock()
      friend.friends += namePlayer
      friend.lock.writeLock().unlock()
      FriendSql.acceptFriendShip(namePlayer, friendName)
    }

    override def friends(namePlayer: String): List[String] = {
      val player = insertAndGetCachePlayer(namePlayer)
      player.lock.readLock().lock()
      val friends = player.friends.toList
      player.lock.readLock().unlock()
      friends
    }

    override def askForFriendship(namePlayer: String, nameFriend: String): Unit = {
      insertAndGetCachePlayer(namePlayer)
      val friend = insertAndGetCachePlayer(nameFriend)
      friend.lock.writeLock().lock()
      friend.friendRequest += namePlayer
      friend.lock.writeLock().unlock()
      FriendSql.askForFriendship(namePlayer, nameFriend)
    }

    private def insertAndGetCachePlayer(namePlayer: String): PlayerCache = {
      var player: Option[PlayerCache] = None
      this.lock.readLock().lock()
      if (players.contains(PlayerCache(namePlayer))) {
        player = Some(players.find(playerTmp => {
          playerTmp.playerName == namePlayer
        }).get)
        this.lock.readLock().unlock()
        this.lock.writeLock().lock()
        players -= player.get
        this.lock.writeLock().unlock()
      }
      else {
        this.lock.readLock().unlock()

        player = Some(loadUser(namePlayer))
      }
      addPlayerToCache(player.get)
      player.get
    }

    private def addPlayerToCache(playerCache: PlayerCache): Unit = {
      this.lock.readLock().lock()
      if (this.players.contains(playerCache)) {
        this.lock.readLock().unlock()

      } else {
        this.lock.readLock().unlock()
        this.lock.writeLock().lock()
        this.players += playerCache
        this.lock.writeLock().unlock()
        this.lock.readLock().lock()
        if (players.size > this.sizePlayerCache) {
          val playerLessUsed = players.head
          this.lock.readLock().unlock()
          playerLessUsed.lock.writeLock().lock()
          this.lock.writeLock().lock()
          players.remove(0)
          this.lock.writeLock().unlock()
          playerLessUsed.lock.writeLock().unlock()
        } else {
          this.lock.readLock().unlock()
        }
      }
    }

    private def loadUser(userName: String): PlayerCache = {
      val result = SqlManager.selectQuery(selectFromTableQuery + " where " +
        userNameSqlField + "='" + userName + "' ;")
      result._1.get.next()
      val username = result._1.get.getString(userNameSqlField)
      val pwHash = result._1.get.getString(pwHashSqlField)
      val score = result._1.get.getInt(scoreSqlField)
      //result._2.close()
      PlayerCache(username, pwHash, score)
    }

    override def addPlayer(namePlayer: String, pwHash: String): Unit = {
      addPlayerToCache(PlayerCache(namePlayer, pwHash))
      PlayerSql.addPlayer(namePlayer, pwHash)
    }

    override def removePlayer(namePlayer: String): Unit = {
      this.lock.readLock().lock()
      if (players.contains(PlayerCache(namePlayer))) {
        val player = playerInCache(namePlayer)
        this.lock.readLock().unlock()
        player.lock.writeLock().lock()
        this.lock.writeLock().lock()
        players -= player
        this.lock.writeLock().unlock()
        player.lock.writeLock().unlock()
      } else {
        this.lock.readLock().unlock()
      }
      PlayerSql.removePlayer(namePlayer)
    }

    def playerInCache(playerName: String): PlayerCache = {
      players.filter(p => p.playerName == playerName).last
    }

    override def updateScore(namePlayer: String, score: Int): Unit = {
      val player = insertAndGetCachePlayer(namePlayer)
      player.lock.writeLock().lock()
      player.setScore (score)
      player.lock.writeLock().unlock()
      PlayerSql.updateScore(namePlayer, score)
    }

    override def getScore(namePlayer: String): Int = {
      val player = insertAndGetCachePlayer(namePlayer)
      player.lock.readLock().lock()
      val score = player.getScore
      player.lock.readLock().unlock()
      score
    }

    override def isCredentialOK(namePlayer: String, pwHash: String): Boolean = {
      if (this.playerNotExist(namePlayer)) false
      else {
        this.setLastLogin(namePlayer)
        val player = insertAndGetCachePlayer(namePlayer)
        player.lock.readLock().lock()
        val pwHashTmp = player.pwHash
        player.lock.readLock().unlock()
        pwHashTmp == pwHash
      }
    }

    override def playerNotExist(namePlayer: String): Boolean = !playerExist(namePlayer)

    override def playerExist(namePlayer: String): Boolean = {
      if (players.exists(player => player.playerName == namePlayer)) true
      else PlayerSql.playerExist(namePlayer)
    }

    override def setLastLogin(namePlayer: String): Unit = PlayerSql.setLastLogin(namePlayer)

    def isPlayerInCache(playerName: String): Boolean = {
      players.contains(PlayerCache(playerName))
    }

    override def getRankingPosition(namePlayer: String): Int = {
      PlayerSql.getRankingPosition(namePlayer)
    }

    def playerList(): List[String] = {
      var playersList: List[String] = List()
      val result = SqlManager.selectQuery(selectFromTableQuery + " order by " + userNameSqlField + " ;")
      while (result._1.isDefined && result._1.get.next() && players.size < sizePlayerCache) {
        playersList = playersList ++ List(result._1.get.getString(userNameSqlField))
      }
      playersList
    }

    override def hallOFFame: List[(String, Int)] = PlayerSql.hallOFFame

    override def listOfPossibleFriends(namePlayer: String): List[(String, Int)] = FriendSql.listOfPossibleFriends(namePlayer)

    private def setUserCache(): Unit

    = {
      val result = SqlManager.selectQuery(queryLoadUser)
      while (result._1.isDefined && result._1.get.next() && players.size < sizePlayerCache) {
        val username = result._1.get.getString(userNameSqlField)
        val pwHash = result._1.get.getString(pwHashSqlField)
        val score = result._1.get.getInt(scoreSqlField)
        players += PlayerCache(username, pwHash, score)
      }
      //result._2.close()
    }
  }


}
