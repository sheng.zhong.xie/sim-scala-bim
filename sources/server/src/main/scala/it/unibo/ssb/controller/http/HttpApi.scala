package it.unibo.ssb.controller.http

import io.vertx.core.Handler
import io.vertx.core.http.HttpMethod
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.model.remote.sql.cache.CacheSqlReader

abstract class HttpApi(var method: HttpMethod, var route: String) extends Handler[RoutingContext] {
  protected lazy val cache = CacheSqlReader()
}

