package it.unibo.ssb.controller.matchMaking

/**
  * this Exception is used if the sender is unauthorized
  * @param message of the exception
  */
class UnhautorizedSender (message:String) extends Exception (message)
