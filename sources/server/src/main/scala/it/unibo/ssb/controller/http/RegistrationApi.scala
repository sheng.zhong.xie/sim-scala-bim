package it.unibo.ssb.controller.http

import akka.http.scaladsl.model.StatusCodes
import io.vertx.core.http.HttpMethod
import io.vertx.scala.ext.web.RoutingContext
import it.unibo.ssb.model.remote.{ApiPwHash, ApiUsername, RegistrationRoute}

case class RegistrationApi() extends HttpApi(HttpMethod.PUT, RegistrationRoute) {

  override def handle(rc: RoutingContext): Unit = {
    val u = rc.queryParams().get(ApiUsername)
    val p = rc.queryParams().get(ApiPwHash)

    val sc = (u, p) match {
      case (Some(user), Some(pw)) if user.nonEmpty && pw.nonEmpty =>
        if (cache.playerExist(user)) {
          StatusCodes.NotAcceptable
        }
        else {
          cache.addPlayer(user, pw)
          StatusCodes.OK
        }

      case _ => StatusCodes.BadRequest
    }
    rc.response().setStatusCode(sc.intValue).end(sc.defaultMessage)
  }
}
