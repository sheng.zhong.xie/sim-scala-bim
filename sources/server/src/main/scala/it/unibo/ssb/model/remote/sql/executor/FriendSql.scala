package it.unibo.ssb.model.remote.sql.executor

trait FriendSql {

  /**
    * it allow to know the list of friend of a player that have to accept the friend request
    *
    * @param namePlayer that have send the friend request
    *
    * @return a List[String] that contain the name of the friend that have to accept the request
    */
  def friendshipsNotAccepted(namePlayer: String): List[String]

  /**
    * it allow to kwow the list of player that ask for friendship
    *
    * @param namePlayer of the player that want to know the list of player thet ask for friendship
    *
    * @return a list of player that ask for friendship
    */
  def friendRequests(namePlayer: String): List[String]

  /**
    * it allow to accept friendship
    *
    * @param namePlayer of the player who accept
    * @param friend     of the friendshp
    */
  def acceptFriendShip(namePlayer: String, friend: String): Unit

  /**
    * it allow to kwow the list of friend of a player that have accepted the friend request
    *
    * @param namePlayer to search
    *
    * @return a List[String] that contain the name of the friend that have accepted the friendship
    */
  def friends(namePlayer: String): List[String]

  /**
    * it allow to ask for a friendship
    *
    * @param namePlayer of the player that ask
    * @param nameFriend to ask for a friendship
    */
  def askForFriendship(namePlayer: String, nameFriend: String): Unit

  /**
    *
    * this method returns the list of users that can be added to friends.
    * *
    * This list consists of all users except:
    * *
    *- the user passed as a parameter
    * -users with whom you are already friends
    * -users to whom you have already sent a friend request
    *- users who have sent a friend request to the user
    *
    * @param namePlayer of the user
    *
    * @return the list of possible friends
    */

  def listOfPossibleFriends(namePlayer: String): List[(String, Int)]
}

object FriendSql extends FriendSql {
  final val FRIEND_REQUEST_TABLE = "progetto_scala.firendsrequest"
  final val FRIEND_TABLE = "progetto_scala.friends"

  override def friendshipsNotAccepted(namePlayer: String): List[String] = {
    //noinspection ScalaStyle
    // for sql query
    val result = SqlManager.selectQuery("SELECT friend FROM " + FRIEND_REQUEST_TABLE + " " +
      "WHERE namePlayer='" + namePlayer + "';")
    var friend: List[String] = List()
    if (result._1.isEmpty) {
      // TODO: ECCEZIONE
    }
    while (result._1.get.next()) {
      friend = friend ++: List(result._1.get.getString("friend"))
    }
    // result._2.close()
    friend
  }


  override def friendRequests(namePlayer: String): List[String] = {
    val result = SqlManager.selectQuery("SELECT namePlayer FROM " + FRIEND_REQUEST_TABLE + " " +
      "WHERE friend='" + namePlayer + "';")
    var friend: List[String] = List()
    if (result._1.isEmpty) {
      // TODO: ECCEZIONE
    }
    while (result._1.get.next()) {
      friend = friend ++: List(result._1.get.getString("namePlayer"))
    }
    //result._2.close()
    friend
  }


  override def acceptFriendShip(namePlayer: String, friend: String): Unit = {
    SqlManager.executeQuery("DELETE FROM " + FRIEND_REQUEST_TABLE + " " +
      "WHERE friend='" + namePlayer + "' AND namePlayer='" + friend + "';")
    SqlManager.executeQuery("INSERT INTO " + FRIEND_TABLE + " (nameFriend1,nameFriend2) VALUES ('" + namePlayer + "','" + friend + "');")
  }


  override def friends(namePlayer: String): List[String] = {
    val result = SqlManager.selectQuery("SELECT * FROM " + FRIEND_TABLE + " " +
      "WHERE nameFriend1='" + namePlayer + "' OR nameFriend2='" + namePlayer + "';")
    var friend: List[String] = List()
    if (result._1.isEmpty) {
      // TODO: ECCEZIONE
    }
    while (result._1.get.next()) {
      val player1: String = result._1.get.getString("nameFriend1")
      val player2: String = result._1.get.getString("nameFriend2")
      if (namePlayer.equals(player1)) friend = friend ++: List(player2)
      else friend = friend ++: List(player1)
    }
    //result._2.close()
    friend
  }


  override def askForFriendship(namePlayer: String, nameFriend: String): Unit = {
    SqlManager.executeQuery("INSERT INTO " + FRIEND_REQUEST_TABLE + " (namePlayer,friend) VALUE " +
      "('" + namePlayer + "','" + nameFriend + "');")
  }


  override def listOfPossibleFriends(namePlayer: String): List[(String, Int)] = {
    var players: List[(String, Int)] = List()
    //noinspection ScalaStyle
    //for sql query
    val result = SqlManager.selectQuery(
      s" SELECT userName,score FROM progetto_scala.users where userName <> '$namePlayer' " +
        s" and userName NOT IN" +
        s" (SELECT namePlayer FROM progetto_scala.firendsrequest where friend = '$namePlayer') " +
        s" and userName NOT IN " +
        s" (SELECT friend FROM progetto_scala.firendsrequest where namePlayer = '$namePlayer') " +
        s" and userName NOT IN " +
        s" (SELECT nameFriend1 FROM progetto_scala.friends where nameFriend2 ='$namePlayer') " +
        s" and userName NOT IN " +
        s" (SELECT nameFriend2 FROM progetto_scala.friends where nameFriend1 ='$namePlayer') " +
        s" order by userName;"
    )
    if (result._2.isDefined) {
      while (result._1.get.next()) {
        val userName: String = result._1.get.getString("userName")
        val score: Int = result._1.get.getInt("score")
        players = players ++: List(Tuple2(userName, score))
      }
      result._2.get.close()
      players
    } else throw new Exception("Connection Lost")
  }
}
