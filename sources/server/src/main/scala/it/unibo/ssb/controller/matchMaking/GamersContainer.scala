package it.unibo.ssb.controller.matchMaking

import akka.actor.ActorRef

trait GamersContainer {
  /**
    *
    * @return the size of the [[GamersContainer]]
    */
  def size: Int

  /**
    *
    * @return the maximum number of gamers that can still be added to the container
    */

  def freeSpace: Int

  /**
    * remove a player from the container
    * @param name of the player
    */
  def removePlayer(name:String): Unit

  /**
    *
    * @return
    * the number of times this container was on the waiting list but was not selected
    */
  def getIgnoredTimes: Int

  /**
    * increment the ignoredTimes
    *
    */
  def incIgnoredTimes(num: Int): Unit
  /**
    *
    * @param gamers players to add to the container
    */

  def addGamers(gamers: List[(ActorRef, String)]): Unit

  /**
    *
    * @return true if the container is full,else false
    */

  def isFull: Boolean

  /**
    *
    * @return
    * returns the players who have been placed in the container
    */

  def gamersList: List[(ActorRef, String)]
}

object GamersContainer {
  def apply(size: Int): GamersContainer = GamersContainerImpl(size)
}
