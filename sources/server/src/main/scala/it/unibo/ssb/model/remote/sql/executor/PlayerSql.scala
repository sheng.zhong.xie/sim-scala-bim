package it.unibo.ssb.model.remote.sql.executor

trait PlayerSql {

  /**
    *
    * @param namePlayer of the player to search
    *
    * @return true if a player exist
    */
  def playerExist(namePlayer: String): Boolean

  /**
    *
    * @param namePlayer of the player to search
    *
    * @return true if the player not exist
    */
  def playerNotExist(namePlayer: String): Boolean

  /**
    * add a player to the db
    *
    * @param namePlayer to add
    * @param pwHash     hash of the password of the player
    */
  def addPlayer(namePlayer: String, pwHash: String): Unit

  /**
    * remove a player from the db
    *
    * @param namePlayer of the player to remove
    */
  def removePlayer(namePlayer: String): Unit

  /**
    * update the score of a player
    *
    * @param namePlayer of the score
    * @param score      , the new score
    */
  def updateScore(namePlayer: String, score: Int): Unit

  /**
    * return the score of a player
    *
    * @param namePlayer of the score
    *
    * @return the score
    */
  def getScore(namePlayer: String): Int

  /**
    * check if the credential is ok
    *
    * @param namePlayer to check
    * @param pwHash     of the player
    *
    * @return true if the credential are validate, else false
    */
  def isCredentialOK(namePlayer: String, pwHash: String): Boolean

  /**
    *
    * @param namePlayer of the player
    *
    * @return position in the ranking of the player
    */
  def getRankingPosition(namePlayer: String): Int


  def setLastLogin(namePlayer: String): Unit

  def hallOFFame: List[(String, Int)]

}

object PlayerSql extends PlayerSql {
  final val USERS_TABLE = "progetto_scala.users"
  final val CONNECTION_ERROR="Connection Lost"

  override def addPlayer(namePlayer: String, pwHash: String): Unit = {

    SqlManager.executeQuery("INSERT INTO " + USERS_TABLE + " (userName,passwordHash,lastLogin) VALUE " +
      "('" + namePlayer + "','" + pwHash + "',now());")

  }

  override def removePlayer(namePlayer: String): Unit = {
    /**
      * to avoid sql injection concatenated queries are not allowed
      */
    //noinspection ScalaStyle
    SqlManager.executeQuery("DELETE  FROM " + USERS_TABLE +
      " WHERE userName='" + namePlayer + "' ;")
    SqlManager.executeQuery(" DELETE FROM " + FriendSql.FRIEND_REQUEST_TABLE +
      " WHERE namePlayer='" + namePlayer + "' OR friend='" + namePlayer + "' ;")
    SqlManager.executeQuery(" DELETE FROM " + FriendSql.FRIEND_TABLE +
      " WHERE nameFriend1='" + namePlayer + "' OR nameFriend2='" + namePlayer + "' ;")
  }

  override def playerNotExist(namePlayer: String): Boolean = !playerExist(namePlayer)

  override def playerExist(namePlayer: String): Boolean = {
    //noinspection ScalaStyle
    val connection = SqlManager.selectQuery("SELECT * FROM " + USERS_TABLE + " WHERE userName='" + namePlayer + "';")

    if (connection._2.isDefined) {
      val playerExist = connection._1.exists(_.next())
      connection._2.get.close()
      playerExist
    } else throw new Exception(CONNECTION_ERROR)
  }

  override def updateScore(namePlayer: String, score: Int): Unit = {
    SqlManager.executeQuery("UPDATE " + USERS_TABLE + " SET score=" + score + " WHERE userName='" + namePlayer + "';")
  }

  override def getScore(namePlayer: String): Int = {
    val result = SqlManager.selectQuery("SELECT score FROM " + USERS_TABLE + " WHERE userName='" + namePlayer + "';")
    if (result._2.isDefined) {
      val s = result._1.map(r => if (r.next()) r.getInt("score") else 0).getOrElse(0)
      result._2.get.close()
      s
    } else throw new Exception(CONNECTION_ERROR)
  }

  override def isCredentialOK(namePlayer: String, pwHash: String): Boolean = {
    val connection = SqlManager.selectQuery("SELECT * FROM " + USERS_TABLE + " WHERE userName='" + namePlayer + "'" +
      " AND passwordHash='" + pwHash + "';")
    if (connection._2.isDefined) {
      val playerExist = connection._1.exists(_.next())
      connection._2.get.close()
      playerExist
    } else throw new Exception(CONNECTION_ERROR)
  }

  override def setLastLogin(namePlayer: String): Unit = {
    SqlManager.executeQuery("UPDATE " + USERS_TABLE + " SET lastLogin= now() WHERE userName='" + namePlayer + "';")
  }

  override def getRankingPosition(namePlayer: String): Int = {
    val result = SqlManager.selectQuery("select count(userName) as position from progetto_scala.users where score > " +
      "(SELECT score FROM progetto_scala.users where userName='" + namePlayer + "' and score)")
    if (result._2.isDefined) {
      val s = result._1.map(r => if (r.next()) r.getInt("position") else 0).getOrElse(0)
      result._2.get.close()
      s
    } else throw new Exception(CONNECTION_ERROR)
  }

  override def hallOFFame: List[(String, Int)] = {
    var players: List[(String, Int)] = List()
    val result = SqlManager.selectQuery("Select userName,score from  progetto_scala.users order by score desc")
    if (result._2.isDefined) {
      while (result._1.get.next()) {
        val userName: String = result._1.get.getString("userName")
        val score: Int = result._1.get.getInt("score")
        players = players ++: List(Tuple2(userName, score))
      }
      result._2.get.close()
      players
    } else throw new Exception(CONNECTION_ERROR)
  }
}
