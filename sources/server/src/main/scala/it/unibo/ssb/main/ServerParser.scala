package it.unibo.ssb.main

import scopt.OptionParser

/**
  * This class implements a parser for command line options at server launch.
  *
  * @param name the name of the executable containing the runner class
  */
case class ServerParser(name: String) extends OptionParser[ServerConfig](name) {

  head(name, Option(getClass.getPackage.getImplementationVersion).getOrElse("DEV") + " version")

  opt[String]("host").abbr("rh").action((h, c) =>
    c.copy(hostname = h)).text("Specify the hostname to be used by the server")

  opt[Int]("port").abbr("rp").action((p, c) =>
    c.copy(port = p)).text("Specify the port to be used by the server for HTTP REST server")

  opt[String]("remote-akka-host").abbr("ah").action((h, c) =>
    c.copy(akkaHostname = h)).text("Specify the hostname of the remote akka actor")

  opt[Int]("remote-akka-port").abbr("ap").action((p, c) =>
    c.copy(akkaPort = p)).text("Specify the port of the remote akka actor")
  help("help").abbr("h").text("prints this usage text")

  override def showUsageOnError: Boolean = true

  /**
    * List all accepted CLI parameters.
    *
    * @return the sequence containing each param (both named and abbreviated) as strings
    */
  final def acceptedCommands: Seq[String] = commands
    .map(o => (o.name, o.shortOpt))
    .flatMap(pair => Seq(pair._1, pair._2.getOrElse("")))
    .filter(_.nonEmpty)
}
