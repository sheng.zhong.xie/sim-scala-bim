package it.unibo.ssb.model


import it.unibo.ssb.model.remote.sql.executor.{FriendSql, PlayerSql}
import org.scalatest.{BeforeAndAfterEach, FunSuite}

import scala.util.Random

class FriendSqlTest extends FunSuite with BeforeAndAfterEach {

  private final val RANDOM_RANGE: Int = 10000
  private final val DEFAULT_SLEEP: Int = 500

  var DefaultUsername: String = Random.nextInt(RANDOM_RANGE).toString
  val DefaultHash: String = Random.nextInt(RANDOM_RANGE).toString
  var DefaultUsername2: String = Random.nextInt(RANDOM_RANGE).toString

  override def beforeEach(): Unit = {

    Thread.sleep(DEFAULT_SLEEP)
    while (PlayerSql.playerExist(DefaultUsername)) DefaultUsername = Random.nextInt(RANDOM_RANGE).toString
    PlayerSql.addPlayer(DefaultUsername, DefaultHash)
    while (PlayerSql.playerExist(DefaultUsername2)) DefaultUsername2 = Random.nextInt(RANDOM_RANGE).toString
    PlayerSql.addPlayer(DefaultUsername2, DefaultHash)
  }

  test("if player1 ask for friendship with player2 it have to see that") {
    FriendSql.askForFriendship(DefaultUsername, DefaultUsername2)
    assert(FriendSql.friendRequests(DefaultUsername2).contains(DefaultUsername))
  }
  test("if player2 accept the friendship with player1, they have to see each other in the friends list") {
    FriendSql.acceptFriendShip(DefaultUsername2, DefaultUsername)
    assert(FriendSql.friends(DefaultUsername).contains(DefaultUsername2))
    assert(FriendSql.friends(DefaultUsername2).contains(DefaultUsername))
  }

  override def afterEach(): Unit = {
    PlayerSql.removePlayer(DefaultUsername)
    PlayerSql.removePlayer(DefaultUsername2)
    Thread.sleep(DEFAULT_SLEEP)
  }
}
