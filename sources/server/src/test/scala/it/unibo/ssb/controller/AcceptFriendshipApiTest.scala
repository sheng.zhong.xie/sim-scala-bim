package it.unibo.ssb.controller

import java.util.concurrent.TimeUnit

import akka.http.scaladsl.model.StatusCodes
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient
import it.unibo.ssb.model.remote.sql.executor.{FriendSql, PlayerSql}
import it.unibo.ssb.model.remote.{AcceptFriendshipRoute, ApiFriend, ApiPwHash, ApiUsername, DefaultApiPort, DefaultHostname}
import org.scalatest.{AsyncFunSuite, BeforeAndAfterEach}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Random

class AcceptFriendshipApiTest extends AsyncFunSuite with BeforeAndAfterEach {
  private val vertx: Vertx = Vertx.vertx

  private final val RANDOM_RANGE=10000
  private final val DURATION=10

  var DefaultUsername: String = Random.nextInt(RANDOM_RANGE).toString
  val DefaultHash: String = DefaultUsername.toLowerCase
  var DefaultFriend: String = Random.nextInt(RANDOM_RANGE).toString
  val DefaultDuration: Duration = Duration(DURATION, TimeUnit.SECONDS)

  private implicit val vertxExecutionContext: VertxExecutionContext = VertxExecutionContext(
    vertx.getOrCreateContext()
  )

  private var verticle: RestVerticle = _
  var deploymentID: String = _

  override def beforeEach(): Unit = {
    verticle = new RestVerticle()
    deploymentID = Await.result(vertx.deployVerticleFuture(verticle), DefaultDuration)
    assert(deploymentID.nonEmpty)
    while (PlayerSql.playerExist(DefaultUsername)) DefaultUsername = Random.nextInt(RANDOM_RANGE).toString
    PlayerSql.addPlayer(DefaultUsername, DefaultHash)
    while (PlayerSql.playerExist(DefaultFriend)) DefaultFriend = Random.nextInt(RANDOM_RANGE).toString
    PlayerSql.addPlayer(DefaultFriend, DefaultHash)
    FriendSql.askForFriendship(DefaultFriend, DefaultUsername)
  }

  test("Get to AcceptFriendshipRoute route should not be permitted") {
    WebClient
      .create(vertx)
      .get(DefaultApiPort, DefaultHostname, AcceptFriendshipRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .addQueryParam(ApiFriend, DefaultFriend)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("Post to AcceptFriendshipRoute route should not be permitted") {
    WebClient
      .create(vertx)
      .post(DefaultApiPort, DefaultHostname, AcceptFriendshipRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .addQueryParam(ApiFriend, DefaultFriend)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("delete to AcceptFriendshipRoute route should not be permitted") {
    WebClient.create(vertx)
      .delete(DefaultApiPort, DefaultHostname, AcceptFriendshipRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .addQueryParam(ApiFriend, DefaultFriend)
      .sendFuture()
      .map(response => assert(response.statusCode() == StatusCodes.BadRequest.intValue))
  }

  test("Put to AcceptFriendshipRoute route with invalid credentials should return Unauthorized") {
    WebClient
      .create(vertx)
      .put(DefaultApiPort, DefaultHostname, AcceptFriendshipRoute)
      .addQueryParam(ApiUsername, "userName" + DefaultUsername)
      .addQueryParam(ApiPwHash, "hash" + DefaultHash)
      .addQueryParam(ApiFriend, "friend" + DefaultFriend)
      .sendFuture()
      .map(response => response.statusCode())
      .map(status => assert(status == StatusCodes.Unauthorized.intValue))
  }
  test("Put to AcceptFriendshipRoute route with valid credentials should return Ok") {
    WebClient
      .create(vertx)
      .put(DefaultApiPort, DefaultHostname, AcceptFriendshipRoute)
      .addQueryParam(ApiUsername, DefaultUsername)
      .addQueryParam(ApiPwHash, DefaultHash)
      .addQueryParam(ApiFriend, DefaultFriend)
      .sendFuture()
      .map(response => {
        response.statusCode()
      })
      .map(status => assert(status == StatusCodes.OK.intValue && FriendSql.friends(DefaultUsername).contains(DefaultFriend)))
  }

  override def afterEach(): Unit = {
    Await.ready(vertx.undeployFuture(deploymentID), DefaultDuration)
    PlayerSql.removePlayer(DefaultUsername)
    PlayerSql.removePlayer(DefaultFriend)
  }
}
