package it.unibo.ssb.model


import it.unibo.ssb.model.remote.sql.executor.PlayerSql
import it.unibo.ssb.model.remote.sql.cache.CacheSqlReader
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuite}

import scala.util.Random


class CacheSqlReaderTest extends FunSuite with BeforeAndAfterAll with BeforeAndAfterEach {
  val cache = CacheSqlReader()

  private final val RANDOM_RANGE: Int = 10000
  private final val DEFAULT_SLEEP: Int = 500

  var DefaultUsername: String = Random.nextInt(RANDOM_RANGE).toString
  var DefaultUsername2: String = Random.nextInt(RANDOM_RANGE).toString
  var DefaultFriend: String = Random.nextInt(RANDOM_RANGE).toString
  val DefaultHash: String = Random.nextInt(RANDOM_RANGE).toString

  override def beforeAll(): Unit = {
    while (PlayerSql.playerExist(DefaultUsername)) DefaultUsername = Random.nextInt(RANDOM_RANGE).toString
    cache.addPlayer(DefaultUsername, DefaultHash)
    while (PlayerSql.playerExist(DefaultFriend)) DefaultFriend = Random.nextInt(RANDOM_RANGE).toString
    cache.addPlayer(DefaultFriend, DefaultHash)
    while (PlayerSql.playerExist(DefaultUsername2)) DefaultUsername2 = Random.nextInt(RANDOM_RANGE).toString
    cache.addPlayer(DefaultUsername2,DefaultHash)
  }

  override def beforeEach(): Unit = {
    Thread.sleep(DEFAULT_SLEEP)
  }

  test("if player1 ask for friendship with player2 it have to see that") {
    cache.askForFriendship(DefaultUsername, DefaultFriend)
    assert(cache.friendRequests(DefaultFriend).contains(DefaultUsername))
  }
  test("if player2 accept the friendship with player1, they have to see each other in the friends list") {
    cache.acceptFriendShip(DefaultFriend, DefaultUsername)
    assert(cache.friends(DefaultUsername).contains(DefaultFriend))
    assert(cache.friends(DefaultFriend).contains(DefaultUsername))
  }

  test("a player added must have 1000 point") {
    assert(cache.getScore(DefaultUsername2) == 1000)
  }
  test("a player must login with its credential") {
    assert(cache.isCredentialOK(DefaultUsername2, DefaultHash))
  }
  test("a player added must exist") {
    assert(cache.playerExist(DefaultUsername2))
    assert(!cache.playerNotExist(DefaultUsername2))
  }
  test("a player can increment its score") {
    cache.updateScore(DefaultUsername2, 10)
    assert(cache.getScore(DefaultUsername2) == 10)
  }
  test("a player removed must not exist") {
    cache.removePlayer(DefaultUsername2)
    assert(cache.playerNotExist(DefaultUsername2))
  }

  override def afterAll(): Unit = {
    PlayerSql.removePlayer(DefaultUsername)
    PlayerSql.removePlayer(DefaultUsername2)
    PlayerSql.removePlayer(DefaultFriend)
  }

  override def afterEach(): Unit = {
    Thread.sleep(DEFAULT_SLEEP)
  }

}

