package it.unibo

import java.util.concurrent.TimeUnit

import scala.concurrent.duration.Duration

package object ssb {
  private final val Seconds: Int = 10
  final val DefaultDuration: Duration = Duration(Seconds, TimeUnit.SECONDS)
  final val DefaultUsername: String = "Pippo"
  final val DefaultHash: String = DefaultUsername.toLowerCase
}
