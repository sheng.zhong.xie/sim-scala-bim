Il team che ha lavorato al progetto d'esame per il corso di Paradigmi di Programmazione e Sviluppo è composto da 4 persone;
l'esigenza di organizzazione del lavoro tra i membri ha portato alla necessità di scegliere una modalità di lavoro di gruppo che permettesse di portare a termine il progetto cooperando nel modo più produttivo possibile.

È stata subito evidente la necessità di utilizzare una forma di approccio \textit{Agile} per poter lavorare in maniera fluida ottenendo piccoli risultati concreti in poco tempo.
Si è dunque deciso di affidarsi ad una forma semplificata del framework organizzativo Scrum.

Il modello teorico Scrum prevede varie figure;
nella declinazione da noi adottata, non è stata utilizzata la figura dello Scrum Master:
essa è infatti considerata opzionale in team di piccole dimensioni e, in assenza di esperienze pregresse (come nel nostro caso) potenzialmente dannosa per il processo di sviluppo, o comunque non ottimale per la divisione dei carichi tra i membri.

Come approfondiremo nella \Cref{sec:tools}, il team ha adottato le tecnologie messe a disposizione da GitLab per l'organizzazione di un processo di sviluppo Agile attraverso issue board e milestone.

\subsection{Storie}\label{subsec:story}

Ad ogni Sprint, dopo aver fatto un'analisi dei requisiti sul dominio applicativo, sono state scritte le \textbf{Storie}, 
requisiti per l'applicazione espressi dal punto di vista dell'utente, del programmatore o di altre figure che si presentano o si presenteranno nella vita del progetto.
Ogni storia esprime un'esigenza o un desiderio che il programma finale dovrà implementare.
Esse, insieme, descrivono tutto il programma.

Seguendo la documentazione\footnote{\href{https://about.gitlab.com/2018/03/05/gitlab-for-agile-software-development/}{https://about.gitlab.com/2018/03/05/gitlab-for-agile-software-development/}} fornita sul sito di GitLab,
si è mappato ogni Sprint Scrum su una Milestone di GitLab ed ogni user story ad una Issue del sito, utilizzando checkbox all'interno del testo della stessa per identificare i singoli item previsti per la realizzazione della funzionalità modellata dalla storia.

Tramite template e seguendo quanto consigliato dalla documentazione ufficiale di Scrum, sono stati definiti 3 tipi di storie:

\begin{description}
    \item[User Story] per quanto riguarda ciascuna delle funzionalità presenti dal punto di vista dell'utente finale;
    \item[Non-User Story] per quanto riguarda ciascuna delle funzionalità necessarie ad un membro del team per implementare quanto pianificato;
    \item[Spike] per quanto riguarda necessità di ricerca e analisi più approfondite su un ambito necessario alla fase di implementazione.
\end{description} 

Ciascuna issue ha ricevuto un peso (weight) in ore necessarie.

Il team ha poi utilizzato lo strumento Boards messo a disposizione da GitLab per organizzarsi:
il sito, infatti, permette di creare bacheche che vengono aggiornate automaticamente sulla base di \textit{issues} e \textit{merge request}, di fatto avendo Scrum/Kanban boards automatizzate e dunque più semplici da gestire.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{fig/scrum.jpeg}
    \caption{Screenshot di una board su GitLab}\label{fig:gitlabBoard}
\end{figure}

Durante la fase implementativa, si è cercato di mantenere per quanto possibile un approccio \textit{Test-Driven}, tentando di descrivere il funzionamento del codice partendo dai test.

\subsection{Modalità di divisione dei task}\label{subsec:task}

Ogni task è un insieme di storie relativo possibilmente allo stesso argomento.
I vari programmatori si dividono quindi i task e lavorano in maniera indipendente.
Il nostro team a cercato di affidare ai vari componenti vari aspetti del
progetto, quindi tutte le storie associate.
Nello specifico:

\begin{itemize}
\item 
    Lorenzo Valgimigli: Controller lato Server, attori lato server, definizione dei messaggi lato server, attori lato client, controller lato client.
\item 
    Riccardo Soro: Attori lato server, definizione dei messaggi lato server, implementazione database, setting del server remoto.
\item
    Luca Semprini: Attori lato client, controller lato client, interfaccia grafica.
\item
    Niccolò Maltoni: Attori lato client, interfaccia grafica controller lato client, gestione degli strumenti (Scalastyle, Gradle, TornadoFX, configurazioni).
\end{itemize}

Per maggiori informazioni vedere le sezioni relative ai vari componenti nel \Cref{ch:implementation}.

Per vedere le varie storie definite durante il progetto nella fase di analisi fare riferimento al \Cref{ch:requirements}.

\subsection{Interazioni pianificate}\label{subsec:interations}

Una volta ricevuta l'approvazione della proposta di progetto, abbiamo stabilito che gli Sprint avrebbero avuto \textbf{cadenza bisettimanale};
durante il corso del progetto, il team ha concluso \textbf{4 Sprint}, e ciascuno è stato come detto mappato su una GitLab Milestone di uguale nome.

L'assegnamento delle story e degli item è stato fatto di comune accordo, secondo le richieste e le capacità di ciascuno, cercando di mantenere un corretto bilanciamento e sfruttare le capacità peculiari di ciascuno.
Abbiamo poi previsto la possibilità che alcuni membri del team potessero contattarsi e coordinarsi individualmente.

Tutti i giorni, il team ha effettuato \textbf{Daily Scrum} di durata nell'intorno di una decina di minuti, quando possibile di persona, altrimenti via Whatsapp:
durante questi momenti di discussione, si è cercato di gestire al meglio tutti gli item sui quali si stava lavorando, rilevando eventuali difficoltà.

Inoltre, al termine delle due settimane previste per ciascuno Sprint, il team si incontrava di persona per effettuare \textbf{Sprint Review} e \textbf{Sprint Retrospective}, 
fasi nelle quali in team discuteva dei risultati ottenuti durante lo Sprint, degli eventuali problemi riscontrati;
al termine, il team si occupava di preparare assieme il nuovo \textbf{Sprint Backlog} per il successivo Sprint.
