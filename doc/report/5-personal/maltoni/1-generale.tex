\subsection{Generale}\label{subsec:maltoni:general}
Per quanto riguarda la parte generale, lo studente Maltoni si è occupato della maggior parte delle tecnologie di supporto al processo di sviluppo del software.

La prima fase del processo di creazione del repository che avrebbe ospitato il codice del progetto è partito dalla scelta della piattaforma di hosting del codice.
Le possibili scelte prese in considerazione sono state GitHub (soluzione vista a lezione), BitBucket e GitLab.
Si è scelto di preferire un'alternativa alla soluzione di riferimento vista a lezione a causa della integrazione non nativa, bensì tramite plugin e servizi esterni, di altre strumentazioni che si sapeva essere necessarie, ossia una piattaforma di Continuous Integration e una piattaforma di project management.
Atlassian BitBucket mette a disposizione Atlassian Pipeline e Trello come soluzioni nativamente integrate, mentre GitLab offre GitLab CI e GitLab Boards.
Le maggiori automazioni fornite da GitLab che lo hanno fatto preferire al concorrente di Atlassian sono le seguenti:

\begin{itemize}
    \item Maggiore configurabilità della piattaforma CI tramite container Docker;
    \item Automazione completa della generazione di User Story nella board tramite GitLab Issues e referenza di commit, branch e milestone all'interno di esse;
    \item Maggiore fluidità della UI web fornita.
\end{itemize}

Per abilitare alcune funzionalità avanzate, come il peso in ore delle issue, è stata richiesta una licenza Gold da parte del team, che è stata fornita solo dopo alcune settimane di sviluppo,
quando il progetto è stato riconosciuto da GitLab come valido per la comunità open-source, essendo pubblicato sotto licenza GNU GPLv3.
Nel periodo in cui la licenza non era ancora stata riconosciuta, sotto consiglio dell'assistenza ufficiale è stata richiesta una licenza di prova per un mese.

Per quanto riguarda la configurazione della CI, si è realizzato due immagini Docker sulle quali eseguire le build:

\begin{description}
    \item[niccomlt/gradle-javafx\footnotemark]\footnotetext{https://gitlab.com/NiccoMlt/gradle-javafx}
        è la base per quasi tutti i job della pipeline;
        è stato necessario realizzarla poiché le immagini ufficiali di Gradle e di OpenJDK non dispongono di OpenJFX né dell'utente root per poterlo installare ad ogni esecuzione.
    \item[niccomlt/latex-jre\footnotemark]\footnotetext{https://gitlab.com/NiccoMlt/latex-jre}
        è la base per il job dedicato alla compilazione del report;
        è stato necessario realizzarla poiché lo strumento di compilazione utilizzato, \textit{arara}, necessita di un JRE per eseguire.  
\end{description}

La pipeline eseguita ad ogni commit prevede i seguenti stage e job, rappresentati anche in \Cref{fig:pipelineDetail}:

\begin{itemize}
    \item Lo stage \textbf{build} è composto principalmente di un job \textit{build}, che si occupa di eseguire \texttt{gradle assemble}\label{code:assemble} per compilare tutto il codice del progetto.
        
    Lo stage contiene comunque anche un secondo job \textit{clean}, opzionale e da eseguire manualmente, che pulisce una eventuale cache rimasta nella pipeline.
    \item Lo stage \textbf{test} contiene il job \textit{scalatest} che si occupa di eseguire i test presenti nel repository
    \item Lo stage \textbf{check} esegue un controllo più approfondito sul codice, verificando anche la qualità attraverso il plugin Detekt per Kotlin e ScalaStyle per Scala.
    \item Lo stage \textbf{deploy} contiene invece quattro differenti job per:
    \begin{itemize}
        \item esecuzione del calcolo della coverage tramite SCoverage e successivo upload su Codacy
        \item generazione della documentazione per i vari linguaggi (Javadoc, Scaladoc e KDoc tramite Dokka)
        \item compilazione e deploy di jar eseguibili per client e server
        \item compilazione del codice \LaTeX{} per questo report.
    \end{itemize}
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{fig/pipelineDetail.png}
    \caption{Rappresentazione di una pipeline suddivisa tra i vari stage e job}\label{fig:pipelineDetail}
\end{figure}

Come è possibile capire da quanto eseguito dalla pipeline, il progetto si avvale del build tool Gradle per gestione delle dipendenze, compilazione, test e deploy.

Durante il primo Sprint, la momentanea assenza di una esigenza di separazione ha reso possibile l'utilizzo di un solo modulo Gradle per l'intera codebase, mentre già dal secondo Sprint è stato necessario suddividere in diversi moduli il codice, in modo da mantenere separato il codice per appartenenza logica.

\begin{figure}[H]
    \centering
    \includegraphics[scale=1]{fig/modules.png}
    \caption{Rappresentazione dei vari moduli all'interno di Intellij IDEA}\label{fig:modules}
\end{figure}

Con gli Sprint successivi, il codice stato strutturato in sottomoduli Gradle come specificato nella \Cref{sec:organizations};
da quanto è possibile vedere in \Cref{fig:modules}, all'atto della release finale i moduli sono quattro:

\begin{itemize}
    \item \textbf{ssb-core} è un modulo libreria che contiene tutto il codice comune sia al client che al server, come ad esempio il modello di base del gioco;
    \item \textbf{ssb-client} è un modulo applicazione che dipende dal modulo \textbf{ssb-core} e contiene la parte di codice relativa al client;
    \item \textbf{ssb-server} è un modulo applicazione che dipende dal modulo \textbf{ssb-core} e contiene la parte di codice relativa al server;
    \item il modulo \textbf{ssb-integration} dipende da tutti gli altri moduli e contiene il codice relativo all'integration testing tra client e server.
\end{itemize}

A livello di root sono definiti i plugin, le configurazioni e i task comuni a tutti i moduli.

La decisione di separare il codice in moduli compilati indipendentemente deriva dalla volontà di organizzare il repository in modo gerarchico, definendo chiaramente il contesto nel quale ogni singola classe, libreria o plugin andava a inserirsi e massimizzando dunque l'incapsulamento.

I vantaggi non sono limitati solo alla maggiore chiarezza della struttura, bensì anche di risparmio in termini di tempo di compilazione:
caricare il plugin \textit{kotlin} solo per il modulo \texttt{client}, ad esempio, permette di guadagnare qualche minuto per compilare modifiche esterne al modulo;
avere i plugin necessari ovunque (come \textit{scala}, ad esempio) caricati per il modulo radice (e dunque una volta sola) ha vantaggi simili.

Questa suddivisione ha avuto però complicazioni per quanto riguarda l'analisi del codice:
la generazione dei report, ad esempio, ha richiesto l'implementazione di un task di aggregazione per poter gestire i file generati per ogni modulo da SCoverage o Scalastyle.
