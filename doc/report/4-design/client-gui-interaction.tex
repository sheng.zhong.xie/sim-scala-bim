La gestione della comunicazione fra le componenti client dell'applicativo è stata delegata a quattro attori principali:

\begin{itemize}
    \item \texttt{ClientActor}: implementato in Scala. A cui viene delegata la gestione di messaggi che riguardano
    il comportamento generale dell'applicazione (parte social, inviti alle partite, autenticazione, hall of fame, ecc.);
    questi messaggi possono essere:
    
    \begin{itemize}
        \item Ricevuti dal \texttt{GuiActor}.
        \item Inviati al Server.
        \item Inviati al \texttt{GuiActor}.
    \end{itemize}
    
    \item \texttt{GameActor}: implementato in Scala. A cui viene delegata la gestione di messaggi relativi al Gioco.
    Quando un giocatore deciderà di iniziare la partita, verrà ricevuto dal \texttt{GuiActor} un messaggio
    contenente l'\texttt{ActorRef} del \texttt{GameGuiActor}, che si occuperà dello scambio di messaggi relativi alla partita
    proprio con il suddetto \texttt{GameActor}. 
    
    \item \texttt{GuiActor}: implementato in Kotlin. A cui viene delegata la gestione di messaggi che riguardano
    il comportamento generale dell'applicazione (parte social, inviti alle partite, autenticazione, hall of fame, ecc.);
    questi messaggi possono essere:
    
    \begin{itemize}
        \item Ricevuti dal \texttt{ClientActor}.
        \item Inviati al \texttt{ClientActor}.
    \end{itemize}
    
    \item \texttt{GameGuiActor}: implementato in Kotlin. A cui viene delegata la gestione di messaggi relativi al Gioco.
\end{itemize}

C'è da aggiungere che, essendo gli attori relativi alla GUI implementati in Kotlin,
e dovendo essi comunicare con dei Controller implementati anch'essi utilizzando Kotlin, si occuperanno anche della conversione,
ove necessario, del contenuto dei messaggi in strutture dati più facilmente ``maneggiabili'' via linguaggio Kotlin: ad esempio,
grazie alle funzioni di estensione (che di fatto rappresentano il pattern Scala ``\textbf{Pimp my library}'') che convertono
strutture dati come Collezioni di Scala (\texttt{scala.collection}) in Collezioni di Kotlin (\texttt{kotlin.collection}) e viceversa,
in modo da facilitare l'interoperabilità tra i due linguaggi.

Si è deciso dunque di suddividere le macro-funzionalità dell'applicazione (funzionalità di gioco, e altre) in questo modo.

In sostanza, la comunicazione viene gestita da messaggi inviati dagli attori che si occupano della GUI
(\texttt{GuiActor}, che si occupa di parlare con il \texttt{GuiController} e \texttt{GameGuiActor},
che si occupa di parlare con il \texttt{GameGuiController}), che alla ricezione attivano metodi del \texttt{GuiController},
che modificano proprietà grafiche, che aggiornano automaticamente la View.

\subsubsection{\texttt{ClientActor}}

Questo attore distingue i messaggi in base alla loro classe di appartenenza:

\begin{itemize}
    \item Per quanto riguarda la ricezione di messaggi di tipo \texttt{GameMessage}, il comportamento è semplice:
    se si tratta di un messaggio \texttt{IsTimeToPlay} ricevuto dal Server, questo verrà inoltrato al \texttt{GuiActor},
    che si occuperà di notificare il \texttt{GameGuiActor}, al quale un riferimento è contenuto all'interno del messaggio stesso.
    Se si tratta, invece, di un messaggio \texttt{GuiReadyToPlay} ricevuto dagli attori GUI, il \texttt{ClientActor} si occuperà
    di creare un \texttt{GameActor} che procederà successivamente a ricevere dal \texttt{GameGuiActor} tutti i messaggi relativi
    alla partita stessa e inoltrarli al Server e viceversa.
    \item Per quanto riguarda, invece i messaggi di tipo \texttt{AuthenticatedSocialMessage} ricevuti dal \texttt{GuiActor},
    il comportamento del \texttt{ClientActor} sarà quello di eseguire delle chiamate REST (rese trasparenti grazie al metodo
    \texttt{doRest()}), che consentiranno di rispondere al \texttt{GuiActor} con messaggi di
    risposta opportuni. Fornendo un esempio concreto, il messaggio \texttt{GetHallOfFame} attiverà la chiamata REST per ottenere
    la hall of fame, e risponderà con un messaggio \texttt{HallOfFame} contenente la classifica globale,
    sotto forma di lista di coppie Stringa (nome giocatore), Intero (punteggio), che verrà convertita grazie alle funzioni
    di estensione menzionate in precedenza.
    \item In caso di ricezione di messaggi di tipo \texttt{ClientMessage}, l'attore setterà i riferimenti interni agli attori sul server
    (se si tratta di \texttt{GuiToManagerPresentation}), oppure gestirà la volontà da parte di qualcuno di iniziare una partita,
    ricevendo un messaggio di richiesta o accettazione di invito.
    \item
    L'attore si occupa di gestire anche la comunicazione con l'attore Discovery sul server, dal qual può ricevere messaggi di tipo \texttt{DiscoveryMessage};
    In particolare, può ricevere inviti ad una nuova partita (tramite un messaggio di tipo \texttt{GameInvitation}) oppure messeggi di gestione della comunicazione client-server
    (come \texttt{MasterLobbyIs} contenente il riferimento al \texttt{MasterLobbyActor} o i vari \texttt{ACK}).
    \item
    In ultimo, l'attore gestisce la ricezione di diversi \texttt{LobbyMessage};
    quelli provenienti da \texttt{LobbyActor} sono:
    \begin{itemize}
        \item \texttt{Hello} notifica la corretta partecipazione del client a una lobby;
        \item \texttt{HasJoined} notifica l'aggiunta di un altro giocatore alla lobby nella quale il client sta aspettando;
        \item \texttt{WaitingState} notifica il passaggio dall'attesa in lobby all'attesa in match-making;
        \item \texttt{LobbyDestroyed} notifica la richiesta di cancellazione della lobby da parte del creatore;
    \end{itemize}
    quelli invece mandati dall'attore che controlla la GUI sono:
    \begin{itemize}
        \item \texttt{ForceStart} notifica la richiesta di avvio forzato da parte del creatore della lobby;
        \item \texttt{DestroyLobby} notifica la richiesta di cancellazione della lobby da parte del creatore;
    \end{itemize}
\end{itemize}

\subsubsection{\texttt{GuiActor}}

Questo attore lavora in maniera intuitiva e tutto ciò che fa è ricevere ed inviare messaggi al \texttt{ClientActor}.
\begin{itemize}
    \item Quando \textbf{riceve} un messaggio: tipicamente utilizzerà l'informazione presente all'interno del messaggio stesso
    per aggiornare una qualche proprietà grafica dell'interfaccia grafica, chiamando dei metodi 
    sulle classi di \texttt{Controller}, relative appunto alla GUI\@.
    \item Quando \textbf{invia} un messaggio: tipicamente significa che qualche evento proveniente dalla GUI, 
    come ad esempio la pressione di un pulsante, deve essere notificato al Server (o comunque verso l'esterno).
\end{itemize}

\subsubsection{\texttt{GameActor}}

L'attore dedicato alla gestione delle dinamiche di gioco viene creato dal \texttt{ClientActor} alla ricezione del messaggio
\texttt{GuiReadyToPlay}. Questo attore funge fondamentalmente da ``ponte'' nella comunicazione client-server durante la partita,
il che garantisce indipendenza tra gestore dell'aggiornamento grafico lato client e gestore della partita vera e propria lato server.
Questo attore riceve messaggi, come detto, inerenti alla partita dal \texttt{GameGuiActor} o dal Controller presente sul server,
lasciando al \texttt{GameGuiActor} la trasparenza voluta.

\subsubsection{\texttt{GameGuiActor}}

L'attore preposto all'aggiornamento delle componenti grafiche durante la partita non ha coscienza sostanzialmente della
comunicazione client-server, ma si occupa esclusivamente di inviare messaggi al \texttt{GameActor} a fronte di eventi provenienti
dalla GUI (che saranno di fatto: lancio di un incantesimo, salto del turno ed abbandono della partita) e riceverne dallo stesso
a fronte di computazioni e rielaborazioni della board di gioco eseguite dal controller presente sul Server.

Di seguito si riporta una rappresentazione tramite diagramma di sequenza del comportamento degli attori
relativi alla partita, con attenzione particolare riservata al lato Client.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{fig/ClientPresentation.png}
    \caption{Rappresentazione dell'inizio di una partita tramite scambio di messaggi.}\label{fig:gameStartClient}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{fig/Game-Client-Server-1.png}
    \caption{Rappresentazione dello scambio di messaggi durante una partita: Inizio di una partita}\label{fig:interaction-start}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{fig/Game-Client-Server-2.png}
    \caption{Rappresentazione dello scambio di messaggi durante una partita: scambio di messaggi durante il round}\label{fig:interaction-spell}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.6]{fig/Game-Client-Server-3.png}
    \caption{Rappresentazione dello scambio di messaggi durante una partita: Fine di un round}\label{fig:interaction-end}
\end{figure}
